﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using LibsDb;
using LibsDb.LibsEntities;

namespace LibsMVC.Controllers
{
    public class LibsController : Controller
    {
        private readonly LibsDbContext _context;

        public LibsController(LibsDbContext context)
        {
            _context = context;
        }

        // GET: Libs
        public async Task<IActionResult> Index()
        {
            return View(await _context.Libs.ToListAsync());
        }

        // GET: Libs/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var lib = await _context.Libs.Include(l => l.Holders)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (lib == null)
            {
                return NotFound();
            }

            return View(lib);
        }

        // GET: Libs/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Libs/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,NameShort,NameFull,Sigla,Url")] Lib lib)
        {
            if (ModelState.IsValid)
            {
                _context.Add(lib);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(lib);
        }

        // GET: Libs/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var lib = await _context.Libs.FindAsync(id);
            if (lib == null)
            {
                return NotFound();
            }
            return View(lib);
        }

        // POST: Libs/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,NameShort,NameFull,Sigla,Url")] Lib lib)
        {
            if (id != lib.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(lib);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!LibExists(lib.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(lib);
        }

        // GET: Libs/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var lib = await _context.Libs
                .FirstOrDefaultAsync(m => m.ID == id);
            if (lib == null)
            {
                return NotFound();
            }

            return View(lib);
        }

        // POST: Libs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var lib = await _context.Libs.FindAsync(id);
            _context.Libs.Remove(lib);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool LibExists(int id)
        {
            return _context.Libs.Any(e => e.ID == id);
        }
    }
}

﻿using eckumoc_netcore_codegen;

using Microsoft.EntityFrameworkCore.Scaffolding.Metadata;

using Newtonsoft.Json.Linq;

using SpbPublicLibsCore.Converter.Generators;
using SpbPublicLibsCore.Domain.Odbc.DataSource;
using SpbPublicLibsCore.Domain.Odbc.Metadata;
using SpbPublicLibsCore.Testing;

using System;

namespace SpbPublicLibsUnit.ConverterUnit
{  
    public class TestGenerators : TestingUnit
    {
        public TestGenerators() : base()
        {
            this.Push(new TestJsonModelGenerator());
            this.Push(new TestEntityControllerGenerator());
            this.Push(new TestEntityModelGenerator());
            this.Push(new TestEntityRepositoryGenerator());
            
            this.Push(new TestServiceControllerGenerator());
            this.Push(new TestTypeScriptGenerator());
        }

        protected override void onTest()
        {
            canGenerateTypeScriptDataModel();            
        }
 
        private void canGenerateTypeScriptDataModel()
        {
            MySqlOdbcDataSource datasource = new MySqlOdbcDataSource();
            EntityModelGenerator gen = new EntityModelGenerator();
            foreach (var table in datasource.GetDatabaseMetadata().Tables)
            {
                //Console.WriteLine("\n");
                TableMetaData tableMetadata = datasource.GetDatabaseMetadata().Tables[table.Key];
                string typeScript = gen.CreateTypeScriptModel(tableMetadata);
                string cSharp = gen.CreateSharpModel(tableMetadata);
                this.messages.Add($"can create c# model of entity: {table.Key}");
                this.messages.Add($"can create typeScript model of entity: {table.Key}");
                //Console.WriteLine(typeScript);
                //Console.WriteLine(cSharp);
            }
        }
    }
}

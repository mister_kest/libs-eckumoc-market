﻿using ApplicationDb;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace ApplicationCore.CRUD
{
    /// <summary>
    /// Обеспечивает выполнение операций чтения, записи, удаления и обновления экземпляров сущностей.
    /// </summary>
    public class CRUDProtocol
    {
        public readonly Dictionary<string, EntityRepository<object>> repositories =
            new Dictionary<string, EntityRepository<object>>();

        public readonly Dictionary<Type, EntityRepository<object>> byTypes =
            new Dictionary<Type, EntityRepository<object>>();

        private readonly ILogger<CRUDProtocol> _logger;
        private readonly ApplicationDbContext _context;

        public string Entity = "Apps";

        public CRUDProtocol( ILogger<CRUDProtocol> logger, ApplicationDbContext context )
        {
            _logger = logger;
            _context = context;
            Init( _context );
        }


      



        /// <summary>
        /// Динамическая инициаллизация обьектов управления сущностями
        /// </summary>
        /// <param name="context"> контекст данных </param>
        public void Init(DbContext context)
        {
            foreach (dynamic pair in this.GetEntities(context))
            {
                string entity = pair.name;
                EntityRepository<object> repository =
                    new EntityRepository<object>(context, pair.value, entity);
                repositories[entity] = repository;
                
            }
        }

 

        /// <summary>
        /// Нехороший способ извеления наименований сущностей
        /// </summary>
        /// <param name="subject"> контекст данных </param>
        /// <returns> множество наименований сущностей </returns>
        private HashSet<object> GetEntities(DbContext subject)
        {
            Type type = subject.GetType();
            HashSet<object> entities = new HashSet<object>();
            foreach (MethodInfo info in type.GetMethods())
            {
                if (info.Name.StartsWith("get_") == true && info.ReturnType.Name.StartsWith("DbSet"))
                {                    
                    if(info.Name.IndexOf("MigrationHistory")==-1)
                        entities.Add( new { name = info.Name.Substring(4), value= info.Invoke(subject, new object[0]) });
                }
            }
            return entities;
        }
    }
}

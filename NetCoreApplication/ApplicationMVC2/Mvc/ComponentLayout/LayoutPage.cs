﻿using Mvc.ComponentControls;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Mvc.ControlsLayout
{
    public class LayoutPage : MvcComponent
    {
        public string Title = "this is a test";

        public PageHeader PageHeader { get; }
        public PageFooter PageFooter { get; }
        public PageBody PageContent { get; }

        public LayoutPage() : base()
        {
            Add(this.PageHeader = new PageHeader());
            Add(this.PageContent = new PageBody());
            Add(this.PageFooter = new PageFooter());
        }
    }

      
}

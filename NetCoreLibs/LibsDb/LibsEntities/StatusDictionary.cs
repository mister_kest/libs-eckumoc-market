﻿using ApplicationDb.Types;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace LibsDb.LibsEntities
{

    [DisplayName("Статус определяет доступность операция над литературой")]
    public class StatusDictionary: DictionaryTable
    {
        [Key]
        public int ID { get; set; }

        public string Description { get; set; }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace LibsDb
{
    public static class LibsExtensions
    {            
        public static IServiceCollection AddSpbPublicLibsDbContext( this IServiceCollection services, 
                                                                        string connectionString)
        {
            if( connectionString == null)
            {
                connectionString = LibsDbContext.DefaultConnectionString;
            }

            Console.WriteLine(
                $"SpbPublicLibsDbContextExtensions.AddSpbPublicLibsDbContext:\n"+
                $"\t {connectionString}");
            services.AddDbContext<LibsDbContext>(options=> {
                options.UseSqlServer(connectionString);
            });
            return services;
        }

    }
}

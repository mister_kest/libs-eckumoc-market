﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Mvc.Models
{
    public class LoginViewModel
    {

        [BindProperty]
        public string ErrorMessage { get; set; }

        [BindProperty]
        [Display(Name = "Электронный адрес")]
        [DataType(
            DataType.EmailAddress,
            ErrorMessage = "Электронный адрес задан некорректно"
        )]
        [Required(ErrorMessage = "Не указан электронный адрес")]
        public string Email { get; set; }

        [BindProperty]
        public string EmailValidationState { get; set; }


        [BindProperty]
        [Display(Name = "Пароль для входа")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Не задан пароль для входа")]
        [MinLength(8, ErrorMessage = "Длина пароля должна быть не менее 8 символов")]
        public string Password { get; set; }

        [BindProperty]
        public string PasswordValidationState { get; set; }
    }
}

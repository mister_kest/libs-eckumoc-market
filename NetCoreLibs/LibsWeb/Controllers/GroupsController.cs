using System.Linq;
using ApplicationDb;
 using LibsDb; 
using LibsDb.LibsEntities;
using ApplicationDb.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;


[ApiController]
[Route("api/Groups/[action]")]
public class GroupsController: ControllerBase
{
	private readonly LibsDbContext _db; 


	public GroupsController( LibsDbContext db ){
		_db=db;
	}

	public void Create( Group record )	{
		_db.Groups.Add(record);
		_db.SaveChanges();
	}

	public void Remove( int id ){
		_db.Groups.Remove(this.Find(id));
		_db.SaveChanges();
	}

	public Group Find( int id )	{
		 return _db.Groups.Find(id);
	}

	public System.Collections.Generic.List<Group> List(   )	{
		 return _db.Groups.ToList();
	}

}
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PublicWeatherComponent } from './public-weather.component';



@NgModule({
  declarations: [PublicWeatherComponent],
  exports: [PublicWeatherComponent],
  imports: [
    CommonModule
  ]
})
export class PublicWeatherModule { }

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MvcMarketPlace.Data;
using MvcMarketPlace.Data.Entities;

namespace EcKuMoC.Areas.Market.Controllers
{
    [Area("Market")]
    public class SaleFactsController : Controller
    {
        private readonly MarketDbContext _context;

        public SaleFactsController(MarketDbContext context)
        {
            _context = context;
        }

        // GET: Market
        public async Task<IActionResult> Index()
        {
            var MarketDbContext = _context.SaleFacts.Include(s => s.Customer).Include(s => s.Subdivision).Include(s => s.Transfer);
            return View(await MarketDbContext.ToListAsync());
        }

        // GET: Market/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var sale = await _context.SaleFacts
                .Include(s => s.Customer)
                .Include(s => s.Subdivision)
                .Include(s => s.Transfer)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (sale == null)
            {
                return NotFound();
            }

            return View(sale);
        }

        // GET: Market/Create
        public IActionResult Create()
        {
            ViewData["CustomerID"] = new SelectList(_context.Customers, "ID", "ID");
            ViewData["SubdivisionID"] = new SelectList(_context.SepateSubdivisions, "ID", "ID");
            ViewData["TransferId"] = new SelectList(_context.Set<MoneyTransfer>(), "ID", "ID");
            return View();
        }

        // POST: Market/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Date,Time,CustomerID,SubdivisionID,Common,TransferId")] SaleFacts sale)
        {
            if (ModelState.IsValid)
            {
                _context.Add(sale);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CustomerID"] = new SelectList(_context.Customers, "ID", "ID", sale.CustomerID);
            ViewData["SubdivisionID"] = new SelectList(_context.SepateSubdivisions, "ID", "ID", sale.SubdivisionID);
            ViewData["TransferId"] = new SelectList(_context.Set<MoneyTransfer>(), "ID", "ID", sale.TransferId);
            return View(sale);
        }

        // GET: Market/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var sale = await _context.SaleFacts.FindAsync(id);
            if (sale == null)
            {
                return NotFound();
            }
            ViewData["CustomerID"] = new SelectList(_context.Customers, "ID", "ID", sale.CustomerID);
            ViewData["SubdivisionID"] = new SelectList(_context.SepateSubdivisions, "ID", "ID", sale.SubdivisionID);
            ViewData["TransferId"] = new SelectList(_context.Set<MoneyTransfer>(), "ID", "ID", sale.TransferId);
            return View(sale);
        }

        // POST: Market/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Date,Time,CustomerID,SubdivisionID,Common,TransferId")] SaleFacts sale)
        {
            if (id != sale.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(sale);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SaleExists(sale.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CustomerID"] = new SelectList(_context.Customers, "ID", "ID", sale.CustomerID);
            ViewData["SubdivisionID"] = new SelectList(_context.SepateSubdivisions, "ID", "ID", sale.SubdivisionID);
            ViewData["TransferId"] = new SelectList(_context.Set<MoneyTransfer>(), "ID", "ID", sale.TransferId);
            return View(sale);
        }

        // GET: Market/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var sale = await _context.SaleFacts            
                .Include(s => s.Subdivision)
                .Include(s => s.Transfer)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (sale == null)
            {
                return NotFound();
            }

            return View(sale);
        }

        // POST: Market/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var sale = await _context.SaleFacts.FindAsync(id);
            _context.Remove(sale);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SaleExists(int id)
        {
            return _context.SaleFacts.Any(e => e.ID == id);
        }
    }
}

﻿
using EcKuMoC.Data;
using EcKuMoC.Models.Home;

using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

using Mvc.API;
using Mvc.Models;
using MvcMarketPlace.Data;
using MvcMarketPlace.Data.Entities;

using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace SpbPublicLibsWebAPI.Controllers
{
    public class HomeController : Controller
    {

        private readonly MarketDbContext _db;
        private readonly TopNews _news;
        private readonly ILogger<HomeController> _logger;
    

        public HomeController(
            MarketDbContext context,
            TopNews news,
            ILogger<HomeController> logger )
        {
            _db = context;
            _news = news;
            _logger = logger;
            _logger.LogInformation("Create");
        }



        public IActionResult SeeMostPopularMovies()
        {
            return RedirectToAction("Movies","/Index?movies=SeeMostPopularMovies");
        }



        public IActionResult SeeNewestMovies()
        {
            return RedirectToAction("Movies", "/Index?movies=SeeNewestMovies");
            
        }

        public IActionResult SeeTopRatedMovies()
        {
            return RedirectToAction("Movies", "/Index?movies=SeeTopRatedMovies");
        }

        public IActionResult News()
        {
            return View(new ProductsViewModel()
            {
                categories = CreateProductCategoriesTree(),
                products = new List<Product>(),
                news = _news.GetTopNews()
            });
        }

        public TreeNode CreateProductCategoriesTree()
        {
            List<TreeNode> nodes = new List<TreeNode>();
            foreach (ProductCategory category in _db.ProductCategories)
            {
                nodes.Add(new TreeNode(null, category));
            }
            foreach (TreeNode node in nodes)
            {
                var parent = node.GetItem().ParentCategory;
                if (parent != null)
                {
                    node.SetParent((from n in nodes where n.GetItem().ID == parent.ID select n).FirstOrDefault());
                }
            }
            return (from n in nodes where n.GetItem().ParentCategory == null select n).FirstOrDefault();
        }

        public IActionResult Products()
        {            
            return View();
        }

        public IActionResult Markets()
        {
            return View();
        }

        public IActionResult Statistics()
        {
            return View();
        }




        




        public async Task<IActionResult> Index()
        {
            var marketPlaceDbContext = _db.Products.Include(p => p.Category);
            return View(await marketPlaceDbContext.ToListAsync());
        }

      
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await _db.Products
                .Include(p => p.Category)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (product == null)
            {
                return NotFound();
            }

            return View(product);
        }



        

 

    }
}

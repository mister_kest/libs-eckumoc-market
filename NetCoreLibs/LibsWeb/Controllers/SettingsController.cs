using System.Linq;
using ApplicationDb;
 using LibsDb; 
using LibsDb.LibsEntities;
using ApplicationDb.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;


[ApiController]
[Route("api/Settings/[action]")]
public class SettingsController: ControllerBase
{
	private readonly LibsDbContext _db; 


	public SettingsController( LibsDbContext db ){
		_db=db;
	}

	public void Create(Settings record )	{
		_db.Settings.Add(record);
		_db.SaveChanges();
	}

	public void Remove( int id ){
		_db.Settings.Remove(this.Find(id));
		_db.SaveChanges();
	}

	public Settings Find( int id )	{
		 return _db.Settings.Find(id);
	}

	public System.Collections.Generic.List<Settings> List(   )	{
		 return _db.Settings.ToList();
	}

}
﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace ApplicationCore.Utils
{

    /// <summary>
    /// Реализует методы работы с типами
    /// </summary>
    public class Typing
    {

        /// <summary>
        /// Метод получения описателя вызова статических методов 
        /// </summary>
        /// <param name="type"> тип </param>
        /// <returns> описание статических методов </returns>
        public static Dictionary<string, object> GetStaticMethods(Type type)
        {
            Dictionary<string, object> actionMetadata = new Dictionary<string, object>();
            foreach (MethodInfo info in type.GetMethods())
            {
                if (info.IsPublic && info.IsStatic)
                {
                    Dictionary<string, object> args = new Dictionary<string, object>();
                    foreach (ParameterInfo pinfo in info.GetParameters())
                    {
                        args[pinfo.Name] = new
                        {
                            type = pinfo.ParameterType.Name,
                            optional = pinfo.IsOptional,
                            name = pinfo.Name
                        };
                    }
                }
            }
            return actionMetadata;
        }
    }
}

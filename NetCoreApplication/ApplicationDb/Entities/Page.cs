﻿using ApplicationDb.Types;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ApplicationDb.Entities
{
    public partial class Page: DimensionTable
    {
        public int ID { get; set; }

        [DisplayName("Аватар")]
        public int AvatarID { get; set; }
        public virtual Resource Avatar { get; set; }

        [DisplayName("Наименование")]
        [Required(ErrorMessage ="Необходимо указать наименование приложения")]
        public string Name { get; set; }

        [DisplayName("Описание")]
        [Required(ErrorMessage = "Необходимо указать описание приложения")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [DisplayName("Контент")]
        [Required(ErrorMessage = "Необходимо указать описание приложения")]
        [DataType(DataType.MultilineText)]
        public string Content { get; set; }
        
        [DisplayName("Роль пользователей")]
        public int RoleID { get; set; }
        public virtual Role Role { get; set; }
        
        [DisplayName("Приложение")]        
        public int AppID { get; set; }
        public virtual App App { get; set; }
    }
}

﻿using ApplicationDb.Types;

using System.ComponentModel;

namespace LibsDb.LibsEntities
{
    [DisplayName("Категория")]
    public class Category: HierDictionaryTable<Category>
    {
        public int ID { get; set; }
 
    }
}

﻿using ApplicationDb.Entities;
using System;
using System.Collections.Concurrent;

namespace CoreApp.AppAPI
{
    public interface APIAuthorization
    {
        void Signout();
        void Signin(string Email, string Password);        
        void Signup(string Email, string Password, string Confirmation,
                    string SurName, string FirstName, string LastName, DateTime Birthday, string Tel);
        User Verify();
        ConcurrentDictionary<string,object> Session();
        bool Has(string Email);
        bool IsSignin();
        bool InRole(string roleName);
    }
}

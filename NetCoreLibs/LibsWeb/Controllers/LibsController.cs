using System.Linq;
using ApplicationDb;
 using LibsDb; 
using LibsDb.LibsEntities;
using ApplicationDb.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;


[ApiController]
[Route("api/Libs/[action]")]
public class LibsController: ControllerBase
{
	private readonly LibsDbContext _db; 


	public LibsController( LibsDbContext db ){
		_db=db;
	}

	public void Create( Lib record )	{
		_db.Libs.Add(record);
		_db.SaveChanges();
	}

	public void Remove( int id ){
		_db.Libs.Remove(this.Find(id));
		_db.SaveChanges();
	}

	public Lib Find( int id )	{
		 return _db.Libs.Find(id);
	}

	public System.Collections.Generic.List<Lib> List(   )	{
		 return _db.Libs.ToList();
	}

}
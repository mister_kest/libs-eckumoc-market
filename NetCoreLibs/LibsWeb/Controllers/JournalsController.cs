using System.Linq;
using ApplicationDb;
 using LibsDb; 
using LibsDb.LibsEntities;
using ApplicationDb.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;


[ApiController]
[Route("api/Journals/[action]")]
public class JournalsController: ControllerBase
{
	private readonly LibsDbContext _db; 


	public JournalsController( LibsDbContext db ){
		_db=db;
	}

	public void Create( Journal record )	{
		_db.Journals.Add(record);
		_db.SaveChanges();
	}

	public void Remove( int id ){
		_db.Journals.Remove(this.Find(id));
		_db.SaveChanges();
	}

	public Journal Find( int id )	{
		 return _db.Journals.Find(id);
	}

	public System.Collections.Generic.List<Journal> List(   )	{
		 return _db.Journals.ToList();
	}

}
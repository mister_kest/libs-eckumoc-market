using System.Linq;
using ApplicationDb;
 using LibsDb; 
using LibsDb.LibsEntities;
using ApplicationDb.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;


[ApiController]
[Route("api/Resources/[action]")]
public class ResourcesController: ControllerBase
{
	private readonly LibsDbContext _db; 


	public ResourcesController( LibsDbContext db ){
		_db=db;
	}

	public void Create( Resource record )	{
		_db.Resources.Add(record);
		_db.SaveChanges();
	}

	public void Remove( int id ){
		_db.Resources.Remove(this.Find(id));
		_db.SaveChanges();
	}

	public Resource Find( int id )	{
		 return _db.Resources.Find(id);
	}

	public System.Collections.Generic.List<Resource> List(   )	{
		 return _db.Resources.ToList();
	}

}
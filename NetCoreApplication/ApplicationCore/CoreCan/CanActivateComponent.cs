﻿using CoreApp.AppAPI;

using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

 

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.CoreDomain
{
    public class CanActivateComponent
    {
        
        private readonly ILogger<CanActivateComponent> _logger;
        private readonly CanActivateOptions _options;
        private readonly RequestDelegate _next;

        public CanActivateComponent(ILogger<CanActivateComponent> logger, RequestDelegate next, CanActivateOptions options) {
            _logger = logger;
            _options = options;
            _next = next;
        }

        public async Task Invoke( HttpContext http, APIAuthorization authorization )
        {            
            string path = http.Request.Path.ToString();

            // перенаправление неавторизованных пользователей на страницу регистрации
            foreach(string route in _options.SigninValdationRoutes)
            {
                if( path.StartsWith(route))
                {
                    _logger.LogInformation($"denied request to: {path}");
                    http.Response.Redirect(_options.LoginPagePath);
                    return;
                }
            }


            // фильтрация запросов пользователей не принадлежащих к заданным ролям
            foreach (var pair in _options.RoleValidationRoutes)
            {
                string roleName = pair.Key;
                foreach(string rolePath in pair.Value)
                {
                    if( path.StartsWith(rolePath))
                    {
                        authorization.InRole(roleName);
                        _logger.LogInformation($"denied request to: {path}");
                    }
                }
            }


            _logger.LogInformation($"commit request to: {path}");
            await _next.Invoke(http);
        }
    }
}

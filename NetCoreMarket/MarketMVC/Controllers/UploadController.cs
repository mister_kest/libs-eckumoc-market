using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using CoreApp;

using miac.ServerApp;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

using MvcMarketPlace.Data;

using Newtonsoft.Json.Linq;


namespace Controllers
{


    [Route("api/[controller]/[action]")]
    [ApiController]
    public class UploadController : Controller
    {
        
      
        private readonly MarketDbContext _db;
        private ResourceManager resourceManager;

        public UploadController(  MarketDbContext db) : base()
        {
          
            _db = db;
            this.resourceManager = new ResourceManager();
        }

        
  

        // GET: api/Upload/ConvertExcelToJson
   
        public async Task<object> ConvertExcelToJson( string returnUrl=null )
        {
            Dictionary<string, object> response = new Dictionary<string, object>();
            long? length = this.HttpContext.Request.ContentLength;
            if ( length != null )
            {
                try
                {
                    byte[] data = new byte[( long ) length];
                    await this.HttpContext.Request.Body.ReadAsync( data, 0, ( int ) length );
                    string mime = Request.ContentType;
                    string text = Encoding.UTF8.GetString(data);
                    Console.WriteLine(text);                                     
                    
                    response["response"] = text;
                    response["status"] = "success";
                }
                catch(Exception e )
                {
                    response["status"] = "failed";
                    Console.WriteLine( e );
                }
            }
            return response;          
        }

       
 


        [HttpGet()]
        public void Download(string id)
        {
            System.Diagnostics.Debug.WriteLine(id);
            Response.ContentType = "image/png";
            byte[] data = ResourceManager.Download(id);
            Response.Body.Write(data,0,data.Length);            
        }

        //https://localhost:44382/api/Upload/AddResource
        // uri: api/Upload/Upload?name=icon1
        [HttpGet("[action]")]
        [HttpPost("[action]")]
        [HttpPatch("[action]")]
        public object AddResource( )
        {
            //miac.ServerApp.Model.SessionScope session = miac.ServerApp.Common.LoginManager.GetSession(Request, timeup);
            Dictionary<string, object> response = new Dictionary<string, object>();
            try
            {
                
                long? length = this.HttpContext.Request.ContentLength;
                if ( length != null )
                {
                    byte[] data = new byte[( long ) length];
                    this.HttpContext.Request.Body.Read( data, 0, ( int ) length );
                    string mime = Request.ContentType;
                    ResourceManager.Upload("test1", "application/pdf", data);

                   
                }
                response["status"] = "success";
            }
            catch(Exception ex )
            {
                response["text"] = ex.Message;
                response["status"] = "failed";
            }
            return response;
        }
 
    }
}

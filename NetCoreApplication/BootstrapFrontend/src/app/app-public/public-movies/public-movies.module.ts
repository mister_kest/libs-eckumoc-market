import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PublicMoviesComponent } from './public-movies.component';

@NgModule({
  declarations: [PublicMoviesComponent],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule
  ]
})
export class PublicMoviesModule { }

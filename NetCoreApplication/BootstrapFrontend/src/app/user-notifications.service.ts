import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserNotificationsService {

  interval: any;

  constructor( private http: HttpClient ) { }

  unsubscribe() {
    clearInterval( this.interval );
  }

  recieveNotifications() {
    return this.http.get('/api/NotificationsApi/recieve');
  }

  handleNotificationsCallback(): any {
    const ctrl = this;
    return function( notifications ){
      ctrl.handleNotifications( notifications );
    }
  }

  handleNotifications(notifications: any[]) {
    const ctrl = this;
    notifications.forEach((notification)=>{
      ctrl.pushNotification(notification);
    });
  }

  pushNotification(notification: any) {
    alert(notification);
  }

  subscribe() {
    const ctrl = this;
    this.interval = setInterval(()=>{
      ctrl.recieveNotifications().subscribe(ctrl.handleNotificationsCallback());
    },1000);
  }
}

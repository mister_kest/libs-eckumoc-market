import { SharedModule } from '../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PublicNewsComponent } from './public-news.component';

@NgModule({
  declarations: [PublicNewsComponent],
  imports: [
    CommonModule,
    SharedModule
  ]
})
export class PublicNewsModule { }

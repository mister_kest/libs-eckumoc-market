import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AppLanguageService } from '../app-language.service';
import { AppService } from '../app.service';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent {
  isExpanded = false;

  collapse() {
    this.isExpanded = false;
  }

  toggle() {
    this.isExpanded = !this.isExpanded;
  }



  constructor(
    public router: Router,
    public app: AppService
    ){

  }



  getRoutes(){
    return this.router.config.filter(r=>r.path!=''&&r.path!='**'&&r.path.indexOf('/')==-1);
  }
}

﻿using Mvc.ComponentControls;
using SpbPublicLibsЬMVC.Mvc.ComponentControls;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Mvc.ControlsLayout
{
    public class PageHeader : MvcComponent
    {
        public string Title = "this is a test";
        public Dictionary<string, ControlButton> menu = new Dictionary<string, ControlButton>();

        public PageHeader( ): base()
        {
            /*menu["about"] = this.createButton("about", ()=> {
                test();
            });*/
            Add(new ControlNavigation());
        }

        private void test()
        {
            for( int i=0; i<10000; i++)
            {
                Debug.WriteLine(i);
            }
        }

        ControlButton createButton( string label, Action click )
        {
            return new ControlButton(label, click );
        }
    }
}

﻿using SpbPublicLibsCore.Converter;
using SpbPublicLibsCore.Domain.Odbc;
using SpbPublicLibsCore.Domain.Odbc.DataSource;
using SpbPublicLibsCore.Testing;

namespace SpbPublicLibsUnit.ConverterUnit
{
    public class TestConverter : TestingUnit
    {
        public TestConverter()
        {
            this.Push(new TestGenerators());
        }

        protected override void onTest()
        {
            canCreateDatabaseContext();
        }

        private void canCreateDatabaseContext()
        {
            ConverterAngular angular = new ConverterAngular();
            string connectionString = "Server=DESKTOP-66CFM7U\\SQLSERVER;Database=spb-public-libs;integrated security=True;";
            string odbcConnectionString = RestfullOdbcExtensions.FromAdoToOdbcConnectionStringForSqlServer(connectionString);
            SqlServerOdbcDataSource dataSource = new SqlServerOdbcDataSource(odbcConnectionString);

            angular.CreateDataContext(
                dataSource, "MysqlDatabaseContext", "MysqlDatabaseContext", "");
        }
    }
}

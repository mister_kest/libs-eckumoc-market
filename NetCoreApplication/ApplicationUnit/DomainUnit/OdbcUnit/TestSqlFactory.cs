﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SpbPublicLibsCore.APIS.TheMovieDatabaseAPI;
using SpbPublicLibsCore.Converter.Generators;
using SpbPublicLibsCore.Domain.Odbc.Controllers;
using SpbPublicLibsCore.Domain.Odbc.Metadata;
using SpbPublicLibsCore.Testing;
using System;
using System.Collections.Generic;
using System.Text;

namespace SpbPublicLibsUnit.DomainUnit.OdbcUnit
{
    class TestSqlFactory: TestingUnit
    {
      
        protected override void onTest()
        {
            TheMovieDatabaseService mdb = new TheMovieDatabaseService();
            JsonModelGenerator generator = new JsonModelGenerator();
            JObject json =
                JObject.FromObject(  mdb.getMoviesTopRated().Result );
            TableMetaData table = generator.GenerateCapitalModelFromJson("PublicMovie", json);
            SqlFactory factory = new SqlFactory();
            Console.WriteLine(factory.CreateTable(table));
            messages.Add("can generate sql to create table");
        }
    
    }
}

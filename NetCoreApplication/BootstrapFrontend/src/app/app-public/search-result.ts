export class SearchResult
{
  page: number;
  total_results: number;
  total_pages: number;
  results: MovieResult[] = [];
}

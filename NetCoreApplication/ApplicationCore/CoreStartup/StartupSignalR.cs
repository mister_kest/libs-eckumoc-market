﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;

using ApplicationCore;

namespace Startup
{
    internal class StartupSignalR : IStartupElement
    {

        public void ConfigureServices(IServiceCollection services)
        {
            /*services.AddSignalR().AddHubOptions<AppHub>(options=>
            {
                options.EnableDetailedErrors = true;
            });*/
        }


        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {            
            app.UseEndpoints(routes => {             
                //routes.MapHub<AppHub>("/Message");
            });
        }
    }
}
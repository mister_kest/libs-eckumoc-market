﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MvcMarketPlace.Data;
using MvcMarketPlace.Data.Entities;

namespace EcKuMoC.Areas.Market.Controllers
{
    [Area("Market")]
    public class ProductCountInfoesController : Controller
    {
        private readonly MarketDbContext _context;

        public ProductCountInfoesController(MarketDbContext context)
        {
            _context = context;
        }

        // GET: Market/ProductCountInfoes
        public async Task<IActionResult> Index()
        {
            var MarketDbContext = _context.ProductCountInfos.Include(p => p.Product).Include(p => p.Warehouse);
            return View(await MarketDbContext.ToListAsync());
        }

        // GET: Market/ProductCountInfoes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var productCountInfo = await _context.ProductCountInfos
                .Include(p => p.Product)
                .Include(p => p.Warehouse)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (productCountInfo == null)
            {
                return NotFound();
            }

            return View(productCountInfo);
        }

        // GET: Market/ProductCountInfoes/Create
        public IActionResult Create()
        {
            ViewData["ProductID"] = new SelectList(_context.Products, "ID", "ID");
            ViewData["WarehouseId"] = new SelectList(_context.Warehouses, "ID", "ID");
            return View();
        }

        // POST: Market/ProductCountInfoes/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,ProductID,WarehouseId,Count")] ProductCountInfo productCountInfo)
        {
            if (ModelState.IsValid)
            {
                _context.Add(productCountInfo);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProductID"] = new SelectList(_context.Products, "ID", "ID", productCountInfo.ProductID);
            ViewData["WarehouseId"] = new SelectList(_context.Warehouses, "ID", "ID", productCountInfo.WarehouseId);
            return View(productCountInfo);
        }

        // GET: Market/ProductCountInfoes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var productCountInfo = await _context.ProductCountInfos.FindAsync(id);
            if (productCountInfo == null)
            {
                return NotFound();
            }
            ViewData["ProductID"] = new SelectList(_context.Products, "ID", "ID", productCountInfo.ProductID);
            ViewData["WarehouseId"] = new SelectList(_context.Warehouses, "ID", "ID", productCountInfo.WarehouseId);
            return View(productCountInfo);
        }

        // POST: Market/ProductCountInfoes/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,ProductID,WarehouseId,Count")] ProductCountInfo productCountInfo)
        {
            if (id != productCountInfo.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(productCountInfo);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProductCountInfoExists(productCountInfo.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProductID"] = new SelectList(_context.Products, "ID", "ID", productCountInfo.ProductID);
            ViewData["WarehouseId"] = new SelectList(_context.Warehouses, "ID", "ID", productCountInfo.WarehouseId);
            return View(productCountInfo);
        }

        // GET: Market/ProductCountInfoes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var productCountInfo = await _context.ProductCountInfos
                .Include(p => p.Product)
                .Include(p => p.Warehouse)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (productCountInfo == null)
            {
                return NotFound();
            }

            return View(productCountInfo);
        }

        // POST: Market/ProductCountInfoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var productCountInfo = await _context.ProductCountInfos.FindAsync(id);
            _context.ProductCountInfos.Remove(productCountInfo);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ProductCountInfoExists(int id)
        {
            return _context.ProductCountInfos.Any(e => e.ID == id);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eckumoc.Data.Connect
{
    public class OdbcConnectionException: Exception
    {
        public OdbcConnectionException(string connectionString, Exception ex) : base($"Con not connect by odbc to: {connectionString}", ex)
        {

        }
        public OdbcConnectionException( string connectionString ):base($"Con not connect by odbc to: {connectionString}")
        {
            
        }
    }
}

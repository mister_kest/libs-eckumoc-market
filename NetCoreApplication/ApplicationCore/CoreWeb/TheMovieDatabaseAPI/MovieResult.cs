﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ApplicationCore.APIS.TheMovieDatabaseAPI
{
    public class MovieResult
    {
        public string popularity { get; set; }
        public string vote_count { get; set; }
        public string video { get; set; }
        public string poster_path { get; set; }
        public string id { get; set; }
        public string adult { get; set; }
        public string backdrop_path { get; set; }
        public string original_language { get; set; }
        public string original_title { get; set; }
        public string title { get; set; }
        public string vote_average { get; set; }
        public string overview { get; set; }

        [DisplayFormat(DataFormatString = "{0:MM.dd.yyyy}")]
        public DateTime? release_date { get; set; }         
        
        public string Year
        {
            get
            {
                return ((DateTime)this.release_date).Year.ToString();
            }
        }
    }
}
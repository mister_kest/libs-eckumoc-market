﻿using System;
using System.Diagnostics;

using ApplicationDb;
using ApplicationDb.Entities;
using LibsDb.LibsEntities;

using Microsoft.EntityFrameworkCore;

namespace LibsDb
{
    public partial class LibsDbContext : DbContext
    {
        public new static string DefaultConnectionString =
            "Server=CCPL-1728;" +
            $"Database={typeof(LibsDbContext).Name};" +
            "Trusted_Connection=True;" +
            //"User id=eckumoc@gmail.com;" +
            //"Password=Gye*34FRtw;" +
            "MultipleActiveResultSets=True";


        public virtual DbSet<Lib> Libs { get; set; }
        public virtual DbSet<Holder> Holders { get; set; }
        public virtual DbSet<Liter> Liters { get; set; }
        public virtual DbSet<Book> Books { get; set; }
        public virtual DbSet<Journal> Journals { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<DeliveryOrder> DeliveryOrders { get; set; }
        public virtual DbSet<DeliveryFact> DeliveryFacts { get; set; }
        public virtual DbSet<RecoveryFact> RecoveryFacts { get; set; }
        


        public virtual DbSet<Account> Accounts { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Person> Persons { get; set; }
        public virtual DbSet<Settings> Settings { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<Group> Groups { get; set; }
        public virtual DbSet<UserGroups> UserGroups { get; set; }

        public virtual DbSet<App> Apps { get; set; }
        public virtual DbSet<Page> Pages { get; set; }
        public virtual DbSet<Resource> Resources { get; set; }
        public virtual DbSet<Calendar> Calendars { get; set; }
        public virtual DbSet<News> News { get; set; }
        public virtual DbSet<Service> Services { get; set; }



        // факты 
        public virtual DbSet<LoginFact> LoginFacts { get; set; }

        // статистика
        public virtual DbSet<LoginDaily> LoginDaily { get; set; }
        public virtual DbSet<LoginWeekly> LoginWeekly { get; set; }


        public LibsDbContext( ) : base() { }

        public LibsDbContext(
                DbContextOptions<LibsDbContext> options)
        {

        }


        /// <summary>
        /// Настройка конфигурации контекста данных
        /// </summary>
        /// <param name="optionsBuilder"></param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            Console.WriteLine($"OnConfiguring(DbContextOptionsBuilder optionsBuilder)");
            Debug.WriteLine($"OnConfiguring(DbContextOptionsBuilder optionsBuilder)");
            if (!optionsBuilder.IsConfigured)
            {
                Debug.WriteLine($"\t connectionString={DefaultConnectionString}");
                optionsBuilder.UseSqlServer(DefaultConnectionString);
            }
        }
    }
}

﻿
using Microsoft.AspNetCore.Http.Connections;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using ApplicationCore.Messaging;
using ApplicationCore.Utils;

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text.Json;
using System.Threading.Tasks;

namespace ApplicationCore
{
    public class HubClient
    {
        private ConcurrentDictionary<string, Func<ResponseMessage, int>> pool;
        private Random R = new Random();
        private int SERIAL_LENGTH = 32;

        private HubConnection connection = null;
        private string url = null;
        private string token = null;
       

        public HubClient( string url= "wss://localhost:5001/Message")
        {
            this.pool = new ConcurrentDictionary<string, Func<ResponseMessage, int>>();
            this.url = url;
            this.Connect();        
        }

       
        /// <summary>
        /// Передача запроса на удаленный сервис
        /// </summary>
        /// <param name="message"> сообщение запроса </param>
        /// <param name="callback"> процедура обработки ответного сообщения </param>
        public async Task Request(RequestParams requestParams, Func<ResponseMessage, int> callback)
        {
            string key = GenerateSerialKey();
            RequestMessage request = new RequestMessage() {
              request = requestParams,
              created = Timing.GetTime(),
              mid = this.GenerateSerialKey()               
            };
            if (token != null)
            {
                request.token = token;
            }
            pool[request.mid] = callback;
            await connection.InvokeAsync("Request", JObject.FromObject(request).ToString());
        }


        /// <summary>
        /// Установка соединения с сервисом SignalR
        /// </summary>
        public void Connect()
        {
            Console.WriteLine( "connecting to: "+this.url );                           
            connection = new HubConnectionBuilder()
                .ConfigureLogging((logging) =>
                {
                    logging.AddConsole();
                })
                .AddJsonProtocol(options => {
                    options.PayloadSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
                })
                .WithUrl(this.url, (options) =>
                {
                    options.SkipNegotiation = true;
                    options.Transports = HttpTransportType.WebSockets;
                })
                .Build();
            connection.Closed += async (error) =>
            {
                await Task.Delay(new Random().Next(0, 5) * 1000);
                await connection.StartAsync();
            };
            connection.On<string>("Response", (string responseText) =>
            {
                Console.WriteLine(responseText);
                ResponseMessage response = JsonConvert.DeserializeObject<ResponseMessage>(responseText);
                if (response.request==null || response.request.mid == null)
                {
                    return;
                }
                else
                {
                    Func<ResponseMessage, int> responseHandler = null;
                    pool.TryRemove(response.request.mid, out responseHandler);
                    if (responseHandler != null)
                    {
                        responseHandler(response);
                    }
                }                    
            });
            connection.StartAsync();                             
        }


        /// <summary>
        /// Генерация ключа доступа к обратному сообщению
        /// </summary>
        /// <returns> ключ </returns>
        private string GenerateSerialKey()
        {
            string key = "";
            do
            {
                key = "";
                for (int i = 0; i < SERIAL_LENGTH; i++)
                {
                    key += (Math.Floor(R.NextDouble() * 10)).ToString();
                }
            } while (pool.ContainsKey(key));

            return key;
        }

    }
}

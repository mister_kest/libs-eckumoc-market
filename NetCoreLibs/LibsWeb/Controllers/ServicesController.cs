using System.Linq;
using ApplicationDb;
 using LibsDb; 
using LibsDb.LibsEntities;
using ApplicationDb.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;


[ApiController]
[Route("api/Services/[action]")]
public class ServicesController: ControllerBase
{
	private readonly LibsDbContext _db; 


	public ServicesController( LibsDbContext db ){
		_db=db;
	}

	public void Create( Service record )	{
		_db.Services.Add(record);
		_db.SaveChanges();
	}

	public void Remove( int id ){
		_db.Services.Remove(this.Find(id));
		_db.SaveChanges();
	}

	public Service Find( int id )	{
		 return _db.Services.Find(id);
	}

	public System.Collections.Generic.List<Service> List(   )	{
		 return _db.Services.ToList();
	}

}
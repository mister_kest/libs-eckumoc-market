﻿using System;

namespace ApplicationCore.Domain.Odbc.DataSource
{
    public class ExcelOleDataSource:OleDataSource
    {
        public ExcelOleDataSource(string filename):base("Excel", filename)
        {
            if( !System.IO.File.Exists(filename))
            {
                throw new Exception("file:"+filename+" not exist");
            }
        }
    }
}

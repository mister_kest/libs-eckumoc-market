﻿using SpbPublicLibsCore.Domain.Odbc.DataSource;
using SpbPublicLibsCore.Domain.Odbc.Metadata;
using SpbPublicLibsCore.Testing;
using System;

namespace SpbPublicLibsCore.Converter.Generators
{ 
    public class TestEntityRepositoryGenerator : TestingUnit
    {
        protected override void onTest()
        {             
            MySqlOdbcDataSource dataSource = new MySqlOdbcDataSource();
            EntityRepositoryGenerator generator = new EntityRepositoryGenerator();
            foreach (var ptable in dataSource.GetDatabaseMetadata().Tables)
            {
                TableMetaData metadata = dataSource.GetDatabaseMetadata().Tables[ptable.Key];
                //Console.WriteLine(generator.CreateEntityRepository(metadata, "SpbPublicLibs"));                
            }
            messages.Add("can generate entity repositories");
        }
    }
}

import { SignInValidatedCanActivateService } from './signin-validated.can-activate.service';

import { PublicMoviesModule } from './app-public/public-movies/public-movies.module';

import { LoginComponent } from './login.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { NavMenuComponent } from './app-nav/nav-menu.component';
import { PublicNewsModule } from './app-public/public-news/public-news.module';
import { PublicNewsComponent } from './app-public/public-news/public-news.component';
import { PublicMoviesComponent } from './app-public/public-movies/public-movies.component';
import { SharedModule } from './shared/shared.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PublicWeatherModule } from './app-public/public-weather/public-weather.module';
import { AppPublicModule } from './app-public/app-public.module';
import { AppUserModule } from './app-user/app-user.module';
import { RegistrationComponent } from './registration.component';


@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    LoginComponent,
    RegistrationComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,

    AppPublicModule,
    AppUserModule,
    SharedModule,

    RouterModule.forRoot([
      { path: '', redirectTo: 'public', pathMatch: 'full' },
      { path: 'public',  children: AppPublicModule.routes },
      { path: 'user',    children: AppUserModule.routes,
        canActivate: [SignInValidatedCanActivateService]},
      { path: 'login',  component: LoginComponent },
      { path: 'registration',  component: RegistrationComponent },
    ]),
    BrowserAnimationsModule
  ],
  providers: [
    // { provide: HTTP_INTERCEPTORS, useClass: AuthorizeInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

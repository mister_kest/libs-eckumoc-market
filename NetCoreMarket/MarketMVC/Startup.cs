 using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ApplicationCore.APIS;
using CoreApp;
using EcKuMoC.Data;
using SpbPublicLibsWeb.MVC;
using SpbPublicLibsWebAPI.Controllers;
using Mvc.Controllers;
using MvcMarketPlace.Data;
using ApplicationDb;

namespace MarketMVC
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }



        public void ConfigureServices(IServiceCollection services)

        {
          
          
            services.AddMvcApplication();
            services.AddMvcApplicationPart(typeof(AccountController).Assembly);
            services.AddTransient<TopNews>();

            services.AddCoreApp<MarketDbContext>(Configuration, MarketDbContext.DefaultConnectionString);
            services.AddDbContext<ApplicationDbContext>(settings =>
            {
                settings.UseSqlServer(ApplicationDbContext.DefaultConnectionString);
            });
            services.AddDbContext<MarketDbContext>(settings =>
            {
                settings.UseSqlServer(ApplicationDbContext.DefaultConnectionString);
            });
            services.AddWebApiServices();
            services.AddMarketPlace();



            services.AddSession();
            services.AddControllersWithViews();
            services.AddRazorPages();
            /*services
                .AddDefaultIdentity<IdentityUser>(options => options.SignIn.RequireConfirmedAccount = true)
                //.AddRoles<MarketplaceRole>()
                .AddEntityFrameworkStores<MarketDbContext>();*/
        }


        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseFileServer();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSession();
            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
                endpoints.MapControllerRoute(
                     name: "default",
                     pattern: "{area=}/{controller=Home}/{action=Index}/{id?}");
                endpoints.MapControllerRoute(
                     name: "default",
                     pattern: "{controller=Home}/{action=Index}/{id?}");

            });
        }
    }
}

 
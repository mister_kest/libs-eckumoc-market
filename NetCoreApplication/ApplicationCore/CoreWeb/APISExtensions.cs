﻿using Microsoft.Extensions.DependencyInjection;
using ApplicationCore.APIS.OpenWeatherAPI;
using ApplicationCore.APIS.TheMovieDatabaseAPI;
using System;

namespace ApplicationCore.APIS
{
    public static class APISExtensions
    {
        public static IServiceCollection AddWebApiServices( this IServiceCollection services )
        {
            Console.WriteLine($"APISExtensions.AddWebApiServices");
            services.AddHttpClient();
            services.AddHttpContextAccessor();
            services.AddTransient<OnecallForecastAPI>();
            services.AddTransient<WeatherForecastAPI>();
            services.AddTransient<OpenWeatherService>();
            services.AddTransient<TheMovieDatabaseService>();
            return services;
        }
    }
}

﻿using Newtonsoft.Json.Linq;

using SpbPublicLibsCore.Domain.Odbc;
using SpbPublicLibsCore.Domain.Odbc.Controllers;
using SpbPublicLibsCore.Domain.Odbc.DataSource;
using SpbPublicLibsCore.Testing;

using System;
using System.Collections.Generic;
using System.Text;

namespace SpbPublicLibsUnit.DomainUnit.OdbcUnit
{
    public class TestSqlServerConnection: TestingUnit
    {
        protected override void onTest()
        {
            canConnectOdbcByAdoConnectionString();
        }

        private void canConnectOdbcByAdoConnectionString()
        {            
            string connectionString = "Server=DESKTOP-66CFM7U\\SQLSERVER;Database=spb-public-libs;integrated security=True;";
            string odbcConnectionString = RestfullOdbcExtensions.FromAdoToOdbcConnectionStringForSqlServer(connectionString);
            SqlServerOdbcDataSource dataSource = new SqlServerOdbcDataSource(odbcConnectionString);
            //Console.WriteLine(JObject.FromObject(dataSource.GetDatabaseMetadata()));
            messages.Add("can connect odbc by ado connection string: "+odbcConnectionString);

        }
    }
}

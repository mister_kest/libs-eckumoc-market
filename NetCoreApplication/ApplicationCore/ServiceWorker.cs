﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace eckumoc.ServerApp.Hosted
{
    public class ServiceWorker : BackgroundService
    {
        public static ILogger<ServiceWorker> _logger;
        public static BlockingCollection<Action<object>> _queue;    


        public ServiceWorker(ILogger<ServiceWorker> logger)
        {
            _queue = new BlockingCollection<Action<object>>();
            _logger = logger;            
            _logger.LogInformation("Create");
        }


        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {                
                await this.DoWork( _queue.Take() );                
            }
        }


        private async Task DoWork( Action<object> todo )
        {            
            await Task.Run(() => {
                todo(this);
            });
        }        
    }
}

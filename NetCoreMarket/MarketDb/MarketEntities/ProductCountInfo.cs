﻿using System.ComponentModel;

namespace MvcMarketPlace.Data.Entities
{
    public class ProductCountInfo
    {

        public int ID { get; set; }
        public int ProductID { get; set; }

        [DisplayName("Продукт")]
        public Product Product { get; set; }
        
        public int WarehouseId { get; set; }

        [DisplayName("Склад")]
        public Warehouse Warehouse { get; set; }

        [DisplayName("Кол-во")]
        public int Count { get; set; }
    }
}
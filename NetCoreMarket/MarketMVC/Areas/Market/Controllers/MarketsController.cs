﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using CoreApp.AppAPI;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MvcMarketPlace.Data;
using MvcMarketPlace.Data.Entities;

namespace EcKuMoC.Areas.Market.Controllers
{
    [Area("Market")]
    public class MarketsController : Controller
    {
        private readonly MarketDbContext _context;
        private readonly APIAuthorization _authorization;

        public MarketsController(MarketDbContext context, APIAuthorization authorization)
        {
            _context = context;
            _authorization = authorization;
        }
 

        // GET: Market/Markets/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var market = await _context.Markets
                .Include(m => m.Owner)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (market == null)
            {
                return NotFound();
            }

            return View(market);
        }

        // GET: Market/Markets/Create
        public IActionResult Create()
        {
          
            return View();
        }

        // POST: Market/Markets/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,OwnerID,Name,Description,Logo")] MvcMarketPlace.Data.Entities.Market market)
        {                        
            if (ModelState.IsValid)
            {
                _context.Add(market);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            
            return View(market);
        }

        // GET: Market/Markets/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var market = await _context.Markets.FindAsync(id);
            if (market == null)
            {
                return NotFound();
            }
            
            return View(market);
        }

        // POST: Market/Markets/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,OwnerID,Name,Description,Logo")] MvcMarketPlace.Data.Entities.Market market)
        {
            if (id != market.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(market);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MarketExists(market.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            
            return View(market);
        }

        // GET: Market/Markets/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var market = await _context.Markets
                .Include(m => m.Owner)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (market == null)
            {
                return NotFound();
            }

            return View(market);
        }

        // POST: Market/Markets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var market = await _context.Markets.FindAsync(id);
            _context.Markets.Remove(market);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool MarketExists(int id)
        {
            return _context.Markets.Any(e => e.ID == id);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Internal;

using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mvc
{
    public class MvcComponent
    {
        public string id;
        public MvcComponent parent;
        public Dictionary<string,MvcComponent> children = new Dictionary<string, MvcComponent>();

        public string location;
        public string index;

        public bool Selected = false;
        public bool Visible = true;

        public MvcComponent( )
        {
            this.id = this.GetType().Name;                   
        }


        public string path()
        {
            if( this.parent != null)
            {
                return this.parent.path() + "-" + this.parent.children.Values.IndexOf(this);
            }
            else
            {
                return "1";
            }
        }

        public MvcComponent Find( string id )
        {
            string[] ids = id.Substring(id.IndexOf("-")+1).Split("-");      
            MvcComponent p = this;
            foreach (string next in ids)
            {
                p = p.children.Values.ToArray()[int.Parse(next)-1];
            }                        
            return p;
        }


        public void Init()
        {
            Console.WriteLine(this + " init");
        }


        public void Destroy()
        {
            Console.WriteLine(this + " init");
        }


        public string Request(string action, object args)
        {
            return $"$request({JObject.FromObject(new { url = this.location, action= action, args = args, id= this.index }) });";
        }





        public void Add(MvcComponent tochild)
        {
            if (tochild.location == null)
            {
                tochild.location = location;
            }
            this.children[tochild.id] = tochild;
            tochild.index = this.index + "-" + this.children.Count;
            tochild.parent = this;
        }


        /// <summary>
        /// Наименование представление Partial
        /// </summary>
        /// <returns></returns>
        public string GetView()
        {
            return "_"+this.GetType().Name;
        }
    }
}

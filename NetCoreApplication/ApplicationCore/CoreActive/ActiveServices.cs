﻿using ApplicationDb.Entities;

using CoreApp.AppAPI;
using Microsoft.Extensions.Logging;

namespace CoreApp.AppActive
{
    /// <summary>
    /// Сервис уровня приложения, содержит службы сеансов
    /// </summary>
    public class ActiveServices : ActiveCollection<Service>, APIServices
    {        
        public ActiveServices( ILogger<ActiveServices> logger, ActiveOptions options ): base( logger, options){}
    }
}

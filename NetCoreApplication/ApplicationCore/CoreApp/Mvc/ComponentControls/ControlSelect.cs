﻿using System;
using System.Collections.Generic;

namespace Mvc.ComponentControls
{ 
    public class ControlSelect : Mvc.MvcComponent
    {        
        public string Label;
        public List<string> Options;

        public ControlSelect( string label, List<string> options ) :base(  )
        {
            Options = options;
            Label = label;
        }


        public void Select( string option )
        {
            Label = option;
        }
    }
}


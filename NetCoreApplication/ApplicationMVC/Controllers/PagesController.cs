using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ApplicationDb;
using ApplicationDb.Entities;
using System.ComponentModel;
using SpbPublicLibsWeb.MVC.DescriptionAttributes;

namespace ApplicationMVC
{

    public class Test: Attribute
    {

    }

    [Test]
    [MvcControllerDescription("","","")]
    public class PagesController : Controller
    {
        private readonly ApplicationDbContext _context;

        
        public PagesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Pages
        public async Task<IActionResult> Index(int? id)
        {
            var applicationDbContext = _context.Pages.Where(p=>p.AppID==id).Include(p => p.App).Include(p => p.Avatar).Include(p => p.Role);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: Pages/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var page = await _context.Pages
                .Include(p => p.App)
                .Include(p => p.Avatar)
                .Include(p => p.Role)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (page == null)
            {
                return NotFound();
            }

            return View(page);
        }

        // GET: Pages/Create
        public IActionResult Create()
        {
            ViewData["AppID"] = new SelectList(_context.Apps, "ID", "ID");
            ViewData["AvatarID"] = new SelectList(_context.Resources, "ID", "Mime");
            ViewData["RoleID"] = new SelectList(_context.Roles, "ID", "Description");
            return View();
        }

        // POST: Pages/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,AvatarID,Name,Description,Content,RoleID,AppID")] Page page)
        {
            if (ModelState.IsValid)
            {
                _context.Add(page);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["AppID"] = new SelectList(_context.Apps, "ID", "ID", page.AppID);
            ViewData["AvatarID"] = new SelectList(_context.Resources, "ID", "Mime", page.AvatarID);
            ViewData["RoleID"] = new SelectList(_context.Roles, "ID", "Description", page.RoleID);
            return View(page);
        }

        // GET: Pages/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var page = await _context.Pages.FindAsync(id);
            if (page == null)
            {
                return NotFound();
            }
            ViewData["AppID"] = new SelectList(_context.Apps, "ID", "ID", page.AppID);
            ViewData["AvatarID"] = new SelectList(_context.Resources, "ID", "Mime", page.AvatarID);
            ViewData["RoleID"] = new SelectList(_context.Roles, "ID", "Description", page.RoleID);
            return View(page);
        }

        // POST: Pages/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,AvatarID,Name,Description,Content,RoleID,AppID")] Page page)
        {
            if (id != page.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(page);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PageExists(page.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["AppID"] = new SelectList(_context.Apps, "ID", "ID", page.AppID);
            ViewData["AvatarID"] = new SelectList(_context.Resources, "ID", "Mime", page.AvatarID);
            ViewData["RoleID"] = new SelectList(_context.Roles, "ID", "Description", page.RoleID);
            return View(page);
        }

        // GET: Pages/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var page = await _context.Pages
                .Include(p => p.App)
                .Include(p => p.Avatar)
                .Include(p => p.Role)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (page == null)
            {
                return NotFound();
            }

            return View(page);
        }

        // POST: Pages/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var page = await _context.Pages.FindAsync(id);
            _context.Pages.Remove(page);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PageExists(int id)
        {
            return _context.Pages.Any(e => e.ID == id);
        }
    }
}

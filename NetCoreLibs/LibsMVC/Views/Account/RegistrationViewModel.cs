﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LibsMVC.Views.Account
{
    public class RegistrationViewModel
    {

        [BindProperty]
        public string ErrorMessage { get; set; }


        [BindProperty]
        [Display(Name = "Электронный адрес")]
        [DataType(
            DataType.EmailAddress,
            ErrorMessage = "Электронный адрес задан некорректно"
        )]
        [Required(ErrorMessage = "Не указан электронный адрес")]
        public string Email { get; set; }

        [BindProperty]
        public string EmailValidationState { get; set; }


        [BindProperty]
        [Display(Name = "Пароль для входа")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Не задан пароль для входа")]
        [MinLength(8, ErrorMessage = "Для пароля должна быть не менее 8 символов")]
        public string Password { get; set; }

        [BindProperty]
        public string PasswordValidationState { get; set; }


        [BindProperty]
        [Display(Name = "Подтверждение")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Не задано подтверждение пароля")]
        public string Confirmation { get; set; }

        [BindProperty]
        public string ConfirmationValidationState { get; set; }


        [BindProperty]
        [Display(Name = "Фамилия")]
        [Required(ErrorMessage = "Не указана фамилия пользователя")]
        [RegularExpression(@"^[а-яА-ЯёЁ]+$", ErrorMessage = "Фамилия может содержать только буквы русского алфавита")]
        public string SurName { get; set; }

        [BindProperty]
        public string SurNameValidationState { get; set; }


        [BindProperty]
        [Display(Name = "Имя")]
        [Required(ErrorMessage = "Не указано имя пользователя")]
        [RegularExpression(@"^[а-яА-ЯёЁ]+$", ErrorMessage = "Имя может содержать только буквы русского алфавита")]
        public string FirstName { get; set; }

        [BindProperty]
        public string FirstNameValidationState { get; set; }


        [BindProperty]
        [Display(Name = "Отчество")]
        [Required(ErrorMessage = "Не указано отчество пользователя")]
        [RegularExpression(@"^[а-яА-ЯёЁ]+$", ErrorMessage = "Отчество может содержать только буквы русского алфавита")]
        public string LastName { get; set; }

        [BindProperty]
        public string LastNameValidationState { get; set; }


        [BindProperty]
        [Display(Name = "Дата рождения")]
        [DataType(DataType.Date)]
        [Required(ErrorMessage = "Не указана дата рождения пользователя")]
        public DateTime Birthday { get; set; }

        [BindProperty]
        public string BirthdayValidationState { get; set; }

        [BindProperty]
        [Phone]
        [Display(Name = "Номер телефона")]
        [Required(ErrorMessage = "Не указана номер телефона")]
        public string Tel { get; set; }
        [BindProperty]
        public string TelValidationState { get; set; }

    }
}

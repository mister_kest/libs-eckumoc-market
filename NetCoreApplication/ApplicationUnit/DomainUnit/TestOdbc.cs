﻿using Microsoft.EntityFrameworkCore.SqlServer.Storage.Internal;

using SpbPublicLibsCore.Testing;

using SpbPublicLibsUnit.DomainUnit.OdbcUnit;

namespace SpbPublicLibsUnit
{
    public class TestOdbc : TestingUnit
    {
        public TestOdbc()
        {
            
            this.Push(new TestSqlFactory());
            this.Push(new TestSqlServerConnection());
             
            this.Push(new TestResourceManager());
            this.Push(new TestDatabaseManager());
        }

        protected override void onTest()
        {
 
        }
    }
}
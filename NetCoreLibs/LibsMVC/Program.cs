using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LibsDb;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace LibsMVC
{
    public class Program
    {
        public static void Main(string[] args)
        {

            LibsDbInitiallizer inititiallizer = new LibsDbInitiallizer();
            inititiallizer.DoInitiallize();

            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}

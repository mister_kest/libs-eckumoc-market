﻿using Microsoft.AspNetCore.Mvc.ApplicationModels;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpbPublicLibsWeb.MVC
{
    public class MvcApiExplorerConvention : IApplicationModelConvention
    {
        public void Apply(ApplicationModel application)
        {
            Console.WriteLine("MvcApiExplorerConvention.Apply()");
            application.ApiExplorer.IsVisible = true;
            
        }
    } 
}

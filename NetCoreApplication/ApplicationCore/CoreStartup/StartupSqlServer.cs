﻿using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using ApplicationCore.Domain.Odbc;

namespace Startup
{
    public class StartupSqlServer<TContext> : IStartupElement where TContext : DbContext
    {
        private string _connectionString;
        

        public StartupSqlServer( string name, string connectionString )
        {            
            this._connectionString = connectionString;            
        }

        public void ConfigureServices(IServiceCollection services)
        {
            if (OdbcSingleton.CONFIGURED == false)
            {
                services.AddSingleton<OdbcSingleton>();
                OdbcSingleton.CONFIGURED = true;
            }
            services.AddDbContext<TContext>(settings=>
            {
                settings.UseSqlServer(this._connectionString);
            });
        }


        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseOdbcForSqlServer(this._connectionString);
        }
    } 
}

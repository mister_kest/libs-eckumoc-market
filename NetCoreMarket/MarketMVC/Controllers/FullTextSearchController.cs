﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using ApplicationCore.Domain.Odbc.Controllers;

namespace messager.ServerApp.Common
{

    [Route("api/[controller]")]
    [ApiController] 
    public class FullTextSearchController : Controller
    {

        [HttpGet()]
        public Dictionary<string, int> search( string query )
        {
            
            Dictionary<string, int> results = new Dictionary<string, int>();
            try
            {


            
                query = query.ToLower();
                
                foreach (var pair in GetDatabaseManager().fasade)
                {
                    string name = pair.Key;
               
                    string pk = GetDatabaseManager().GetMetaData().Tables[pair.Key].pk;
                    if(pk == null)
                    {
                        throw new Exception("Primary key udefined for table "+name);
                    }
                    List<string> textColumns = ((TableManagerStatefull)pair.Value).GetMetadata().GetTextColumns();
                    foreach ( JObject record in ((TableManagerStatefull)pair.Value).dataRecords)
                    {
                        try
                        {
                            //Console.WriteLine(record);
                            int id = record[pk].Value<int>();
                            int relevation = 0;
                            foreach (string column in textColumns)
                            {
                                if (record[column] != null)
                                {
                                    string textValue = record[column].Value<string>();
                                    if (String.IsNullOrEmpty(textValue)) continue;
                                    int relativeClause = (from word in textValue.Split() where query.Split(" ").Contains(word) select word).Count();
                                    relevation += relativeClause;
                                }

                            }
                            if (relevation > 0)
                            {
                                results[name + "/" + id] = relevation;
                                var myList = results.ToList();
                                myList.Sort((pair1, pair2) => pair1.Value.CompareTo(pair2.Value));
                            }
                        }catch(Exception ex)
                        {
                            Console.WriteLine(ex);
                            continue;
                        } 

                        
                    }
                
                }
            }catch(Exception ex){
                results[ex.Message] = 500;
                Console.WriteLine(ex);
            }
            return results;
        }

        private DatabaseManager GetDatabaseManager()
        {
            return new DatabaseManager();
        }
    }
}

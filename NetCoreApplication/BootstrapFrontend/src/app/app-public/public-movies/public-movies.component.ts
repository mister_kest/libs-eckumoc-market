import { AppService } from './../../app.service';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AppLanguageService } from 'src/app/app-language.service';

import { SearchResult } from '../search-result';
import { ReaderOrderService } from 'src/app/app-user/user-reader/reader-order/reader-order.service';

@Component({
  selector:     'public-movies',
  templateUrl:  './public-movies.component.html'
})
export class PublicMoviesComponent implements OnInit {

  model: SearchResult = new SearchResult();
  query = 'love';

  constructor(
    private http: HttpClient,
    private checkout: ReaderOrderService,
    private app: AppService,
    private lang: AppLanguageService  ) { }

  ngOnInit() {

    this.http.get('/api/Search/getMoviesTopRated',{
      params: {
      }
    }).subscribe(this.setter('model'));
    const ctrl = this;
    this.app.searchEvent.subscribe(
      (q)=>{

        ctrl.http.get('/api/Search/movieSearch',{
          params: {
            query: ctrl.query=q,
            page:  '1'
          }
        }).subscribe(ctrl.setter('model'));
      }
    );
  }


  setPage( page ){
    console.log(page);
    const ctrl = this;
    ctrl.http.get('/api/Search/movieSearch',{
      params: {
        query: ctrl.query,
        page:  ''+page+''
      }
    }).subscribe(ctrl.setter('model'));
  }

  setter( key ) {
    const ctrl = this;
    return function( value ){
      console.log( value );
      ctrl[key] = value;
    }
  }

  onMovieClick( movie ){
    this.checkout.products.push(
      { name: movie.title, price: 0}
    );
  }
}


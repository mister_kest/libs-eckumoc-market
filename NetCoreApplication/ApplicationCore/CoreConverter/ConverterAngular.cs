﻿using System;
using System.Collections.Generic;

using ApplicationCore.CoreAPI;

using eckumoc_netcore_codegen;

using Microsoft.CodeAnalysis.CSharp.Syntax;

using ApplicationCore.Converter.Generators;
using ApplicationCore.Converter.Models;
using ApplicationCore.Domain.Odbc.Metadata;
using ApplicationCore.Utils;

namespace ApplicationCore.Converter
{
    /// <summary>
    /// Реализация обьекта управления приложением Angular.
    /// </summary>
    public class ConverterAngular
    {

        /// <summary>
        /// Путь к корневой директории.
        /// </summary>
        private string ClientAppDirectory = null;
        private string ServerAppDirectory = null;

        private Dictionary<string, string> files;

        private EntityModelGenerator genModels = new EntityModelGenerator();
        private EntityRepositoryGenerator genRepositories = new EntityRepositoryGenerator();
        private EntityControllerGenerator genControllers = new EntityControllerGenerator();      
        private TypeScriptGenerator genTypeScript = new TypeScriptGenerator();
        private ServiceControllerGenerator genServices = new ServiceControllerGenerator();


        /// <summary>
        /// Конструктор по-умолчанию
        /// </summary>
        public ConverterAngular(): this("SpbPublicLibsWebAPI")
        {
        }


        /// <summary>
        /// Конструктор 
        /// </summary>
        /// <param name="ClientAppDirectory"> абсолютный путь к приложению </param>
        public ConverterAngular(string ClientAppDirectory )
        {
            
            ClientAppDirectory = @"A:\EcKuMoC\NetCoreApplication\MaterialApplication\";
            ServerAppDirectory = @"A:\EcKuMoC\NetCoreLibs\LibsWeb\";

            if (System.IO.Directory.Exists(this.ClientAppDirectory = ClientAppDirectory) == false)
            {
                System.IO.Directory.CreateDirectory(ClientAppDirectory);
            }
        }


        /// <summary>
        /// Генерация контроллеров для источника данных
        /// </summary>
        /// <param name="datasource"> Источник данных </param>
        public void Do(APIDataSource datasource, string dbContextName)
        {
            DatabaseMetadata databasemetadata = datasource.GetDatabaseMetadata();
            databasemetadata.Validate();
            List<string> entityControllers = new List<string>();
            Dictionary<string, string> files =
                new Dictionary<string, string>();
                //genTypeScript.createDataContext(datasource.GetDatabaseMetadata());
            foreach (var ptable in databasemetadata.Tables)
            {
                Console.WriteLine(ptable.Value.name);
                TableMetaData metadata = datasource.GetDatabaseMetadata().Tables[ptable.Key];
                //string apiController = genControllers.CreateEntityController(metadata);
                //files["WebAPI\\"+Naming.ToCapitalStyle(metadata.name) + "Controller.cs"] = apiController;

                // классы сущностей
                //string sharpModel = genModels.CreateSharpModel(metadata);
                //files[$"{ServerAppDirectory}Models\\" + Naming.ToCapitalStyle(metadata.singlecount_name) + ".cs"] = sharpModel;

                // классы сущностей
                string tsModel = genModels.CreateTypeScriptModel(metadata);
                files[$"{ClientAppDirectory}\\src\\app\\models\\"+Naming.ToKebabStyle(metadata.singlecount_name) + ".ts"] = tsModel;

                // webapi контроллеры
                string crudRepository = genRepositories.CreateEntityRepository(metadata, dbContextName);
                files[$"{ServerAppDirectory}Controllers\\"+Naming.ToCapitalStyle(metadata.name) + "Controller.cs"] = crudRepository;
                entityControllers.Add(Naming.ToCapitalStyle(metadata.name) + "Controller");
                string serviceController = this.genServices.CreateServiceController(new Models.ServiceControllerModel()
                {
                    Name =      Naming.ToCapitalStyle(metadata.name) + "Controller",
                    Path =      $"/api/{Naming.ToCapitalStyle(metadata.name)}",
                    Actions =  new Dictionary<string, ControllerActionModel>()
                    {
                        {   "Find",
                            new ControllerActionModel(){
                                Name="Find",
                                Path=$"/api/{Naming.ToCapitalStyle(metadata.name)}/Find",
                                Parameters = new Dictionary<string, ActionParameterModel>()
                                {
                                    { "id",
                                        new ActionParameterModel(){
                                            
                                            Name = "id",
                                            Type = "int",
                                            IsOptional = false
                                    }   }
                                }
                            }
                        },
                        {   "Remove",
                            new ControllerActionModel(){
                                Name="Remove",
                                Path=$"/api/{Naming.ToCapitalStyle(metadata.name)}/Remove",
                                Parameters = new Dictionary<string, ActionParameterModel>()
                                {
                                    { "id",
                                        new ActionParameterModel(){
                                            Name = "id",
                                            Type = "int",
                                            IsOptional = false
                                    }   }
                                }
                            }
                        },
                        {   "Create",
                            new ControllerActionModel(){
                                Name="Create",
                                Path=$"/api/{Naming.ToCapitalStyle(metadata.name)}/Create",
                                Parameters = new Dictionary<string, ActionParameterModel>()
                                {
                                    { "record",
                                        new ActionParameterModel(){
                                            Name = "record",
                                            Type = "object",
                                            IsOptional = false
                                    }   }
                                }
                            }
                        },
                        {   "List",
                            new ControllerActionModel(){
                                Name="List",
                                Path=$"/api/{Naming.ToCapitalStyle(metadata.name)}/List",
                                Parameters = new Dictionary<string, ActionParameterModel>()
                                {                                   
                                }
                            }
                        }
                    }
                }); 
                files[$"{ClientAppDirectory}\\src\\app\\controllers\\" + Naming.ToKebabStyle(metadata.multicount_name).ToLower() + ".service.ts"] = serviceController;
            
            }


            // создаем директорию
            if (System.IO.Directory.Exists($"{ClientAppDirectory}\\src\\app\\controllers") == false)
            {
                System.IO.Directory.CreateDirectory($"{ClientAppDirectory}\\src\\app\\controllers");
            }

            // создаем сервисы взаимодействующие с контроллерам
            Dictionary<string, string> webapi = this.genServices.GetWebApi();
            foreach (var api in webapi)
            {
                files[$"{ ClientAppDirectory}\\src\\app\\controllers\\" + Naming.ToKebabStyle(api.Key).ToLower() + ".service.ts"] = webapi[api.Key];
           
            }

            // создаём контекст данных
            files[$"{ClientAppDirectory}\\src\\app\\controllers\\" + Naming.ToKebabStyle(dbContextName.Replace("DbContext", "")).ToLower() + ".service.ts"]
                = this.CreateWebApiDbContext(dbContextName, entityControllers);


            /*string dbContextCode = $"public partial class {dbContextName}: Microsoft.EntityFrameworkCore.DbContext";
            dbContextCode += "\n{";
            dbContextCode += $"\n\n\tpublic {dbContextName}():base()" + "{}";
            dbContextCode += $"\n\n\tpublic {dbContextName}(Microsoft.EntityFrameworkCore.DbContextOptions<{dbContextName}> options): base(options)" + "{}";
            foreach (var ptable in databasemetadata.Tables)
            {
                dbContextCode += $"\n\n\tpublic virtual Microsoft.EntityFrameworkCore.DbSet<{Naming.ToCapitalStyle(ptable.Value.singlecount_name)}> {Naming.ToCapitalStyle(ptable.Value.multicount_name)}"+" { get; set; }";
            }
            dbContextCode += "\n}";
            files[dbContextName + ".cs"] = dbContextCode;*/

            this.files = files;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbContextName"></param>
        /// <param name="entityControllers"></param>
        /// <returns></returns>
        private string CreateWebApiDbContext(string dbContextName, List<string> entityControllers)
        {
            string typeScript = "import { HttpClient,HttpParams } from '@angular/common/http';\n";
            typeScript += "import { Injectable } from '@angular/core';\n\n";
            

            string imports = "";
            string injection = "";
            foreach (string ctrl in entityControllers)
            {
                imports += "import {" + Naming.ToCapitalStyle(ctrl) + "Service}" + $" from './{Naming.ToKebabStyle(ctrl).ToLower().Replace("-controller","")}.service';\n";
                injection += "public "+Naming.ToCamelStyle(ctrl) + ": " + ctrl + "Service,\n\t\t\t\t";
            }
            if (injection.EndsWith(","))
            {
                injection = injection.Substring(0, injection.Length - 1);
            }
            string code = $"\n\nexport class {dbContextName.Replace("DbContext","")}Service" + "{\n";
            code += $"\t\tconstructor({injection})"+ "{}";
            code += "}";
            code = "@Injectable({ providedIn: 'root' })\n"+ code;
            return typeScript+imports+code;
        }


        /// <summary>
        /// Сохранение кода в файловую систему
        /// </summary>
        public void Save()
        {
            Console.WriteLine(this.ClientAppDirectory);
            
            if (System.IO.Directory.Exists(this.ClientAppDirectory + $"\\Models") == false)
            {
                System.IO.Directory.CreateDirectory(this.ClientAppDirectory + $"\\Models");
            }
            if (System.IO.Directory.Exists(this.ClientAppDirectory + $"\\WebAPI") == false)
            {
                System.IO.Directory.CreateDirectory(this.ClientAppDirectory + $"\\WebAPI");
            }
            if (System.IO.Directory.Exists(this.ClientAppDirectory + $"\\Controllers") == false)
            {
                System.IO.Directory.CreateDirectory(this.ClientAppDirectory + $"\\Controllers");
            }
            if (System.IO.Directory.Exists(this.ClientAppDirectory + $"\\ClientApp\\src\\app\\models") == false)
            {
                System.IO.Directory.CreateDirectory(this.ClientAppDirectory + $"\\ClientApp\\src\\app\\models");
            }
            
            foreach (var pfile in files)
            {
                string filename =  $"{pfile.Key}";
                System.IO.File.WriteAllText(filename, pfile.Value);
                Console.WriteLine(filename + " " + pfile.Value.Length + " bytes");
            };
            Console.WriteLine(this.ClientAppDirectory);
        }


        /// <summary>
        /// Генерация сервисов для доступа к базе данных
        /// </summary>
        /// <param name="odbc"> компонент ODBC </param>
        /// <param name="serviceClassName"> класс контекста данных </param>
        /// <param name="serviceFileName"> имя файла контекста данных </param>
        /// <param name="outputDir"> директория вывода </param>
        public void CreateDataContext(APIDataSource odbc, string serviceClassName, string serviceFileName, string outputDir)
        {
            outputDir = !outputDir.StartsWith(System.IO.Directory.GetCurrentDirectory()) ?
                System.IO.Directory.GetCurrentDirectory() + outputDir : outputDir;
            if ( !System.IO.Directory.Exists(outputDir) )
            {
                throw new ArgumentException("outputDir");
            }

            DatabaseMetadata mdb = odbc.GetDatabaseMetadata();
            string imports = "import { Component,Injectable } from '@angular/core';\n";
            string dbcontextCode = "\n@Injectable({providedIn: 'root'})\nexport class " + serviceClassName + "\n{\n";
            dbcontextCode += "\t constructor( \n";
           
            foreach (TableMetaData table in mdb.Tables.Values)
            {
                imports += "import { " + table.getTableNameCapitalized() + "Service } from './" + table.singlecount_name + ".service';\n";
                dbcontextCode += "\t\t public " + table.getTableNameCamelized() + ": " + table.getTableNameCapitalized() + "Service,\n";


                string wscode = "import { Component,Inject,Injectable } from '@angular/core';\n" +
                                "import { HttpClient, HttpParams } from '@angular/common/http';\n" +
                                "import { " + table.getTableNameCapitalized() + " } from './" + table.singlecount_name + ".model';\n";
                wscode += "@Injectable({providedIn: 'root'})\nexport class " + table.getTableNameCapitalized() + "Service \n{\n" +
                    "\t\t constructor(  private http: HttpClient ){} \n" +

                    "\t\t toHttpParams(obj: any){\n" +
                    "\t\t    const result = new HttpParams();\n" +
                    "\t\t    Object.getOwnPropertyNames(obj).forEach(name => {\n" +
                    "\t\t        result.set(name, JSON.stringify(obj[name]));\n" +
                    "\t\t    });\n" +
                    "\t\t    return result;\n" +
                    "\t\t }\n" +
                "\t\t list( resolve: ( data: " + table.getTableNameCapitalized() + "[])=>any, reject? :(err)=>any ){ this.http.get<" + table.getTableNameCapitalized() + "[]>('api/" + table.getTableNameCapitalized() + "').subscribe(resolve,reject); };\n" +
                    "\t\t find( id: number, resolve: (data: " + table.getTableNameCapitalized() + ")=>any, reject? :(err)=>any ){ this.http.get('api/" + table.getTableNameCapitalized() + "',{params:this.toHttpParams({ id: id })}).subscribe(resolve,reject); };\n" +
                    "\t\t update( id: number, obj: " + table.getTableNameCapitalized() + ",resolve,reject ){ this.http.put('api/" + table.getTableNameCapitalized() + "',{params:this.toHttpParams({id: id, " + table.getTableNameCapitalized() + ": obj })}).subscribe(resolve,reject); };\n" +
                    "\t\t create( obj: " + table.getTableNameCapitalized() + ", resolve? :( status: number )=>any, reject? :(err)=>any ){ this.http.post('api/" + table.getTableNameCapitalized() + "',{params:this.toHttpParams({ " + table.getTableNameCapitalized() + " : obj })}).subscribe(resolve,reject); };\n" +
                    "\t\t remove( id: number, resolve? :(status: number)=>any, reject? :(err)=>any ){ this.http.delete('api/" + table.getTableNameCapitalized() + "',{params: this.toHttpParams({id: id})}).subscribe(resolve,reject); };\n" +
                "}";



                //dbcontextCode += "\n" + table.multicount_name + ": " + table.singlecount_name + "[];\n";
                string tscode = genModels.CreateTypeScriptModel(table);
                System.IO.File.WriteAllText(outputDir + table.singlecount_name + ".service.ts", wscode);
                System.IO.File.WriteAllText(outputDir + table.singlecount_name + ".model.ts", tscode);
                
            }
            dbcontextCode = dbcontextCode.Substring(0, dbcontextCode.Length - 2) + "){}\n" + "}\n";
            System.IO.File.WriteAllText(outputDir + serviceFileName + ".service.ts", imports + dbcontextCode);
        }
    }
}
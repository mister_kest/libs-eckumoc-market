﻿using System;
using SpbPublicLibsDb.DAL;
using SpbPublicLibsDb.DAL.SpbPublicLibs;
using SpbPublicLibsCore.Testing;
using SpbPublicLibsCore.Domain.Registration;

namespace SpbPublicLibsUnit
{
    public class TestRegistration : TestingUnit
    {
        protected override void onTest()
        {
            canCreateUserAccount();
            canCheckEmailRegistration();
        }

        private void canCheckEmailRegistration()
        {
            using (SpbPublicLibsDb.DAL.SpbPublicLibsDbContext db = new SpbPublicLibsDb.DAL.SpbPublicLibsDbContext())
            {
                RegistrationService registration = new RegistrationService(null, db);
                registration.HasEmailRegistered("eckumoc@gmail.com");
                messages.Add("can check email registration");
            }
        }

        private void canCreateUserAccount()
        {
            using(SpbPublicLibsDb.DAL.SpbPublicLibsDbContext db = new SpbPublicLibsDb.DAL.SpbPublicLibsDbContext())
            {
                RegistrationService registration = new RegistrationService(null, db);
                registration.RegistrationComplete("sgdf1423", "eckumoc@gmail.com", DateTime.Parse("26.08.1989"), "Konstantin", "Alexandrovich", "Batov");
                messages.Add("can create user account");
            }
        }
    }
}

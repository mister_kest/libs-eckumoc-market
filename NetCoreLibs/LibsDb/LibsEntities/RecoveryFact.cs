﻿using ApplicationDb.Entities;
using ApplicationDb.Types;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace LibsDb.LibsEntities
{
    /// <summary>
    /// Таблица фактов возврата литературы
    /// </summary>
    [DisplayName("Сведения о возврате литературы")]
    public class RecoveryFact: FactsTable
    {
        [Key]
        public int ID { get; set; }


        [DisplayName("Читатель")]
        public int UserID { get; set; }
        public virtual User User { get; set; }


        [DisplayName("Литература")]
        public int LiteratureID { get; set; }
        public virtual Liter Literature { get; set; }
    }
}

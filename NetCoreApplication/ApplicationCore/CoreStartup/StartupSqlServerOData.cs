﻿using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
 
using System;
//using Microsoft.AspNet.OData.Builder;

namespace Startup
{
    public class StartupSqlServerOData<TContext> : IStartupElement where TContext : DbContext
    {
        private string _connectionString;


        public StartupSqlServerOData(string connectionString)
        {
            this._connectionString = connectionString;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            //services.AddOData();
            services.AddDbContext<TContext>(settings =>
            {
                settings.UseSqlServer(this._connectionString);
            });
        }


        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                //endpoints.Select().Expand().Count().Filter().OrderBy().MaxTop(100).SkipToken();

                //ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
            
                
                //endpoints.MapODataRoute("odata", "odata", builder.GetEdmModel());
            });
        }
    }
}

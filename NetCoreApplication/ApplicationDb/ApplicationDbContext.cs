﻿using ApplicationDb.Entities;

using Microsoft.EntityFrameworkCore;
using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace ApplicationDb
{    
    public partial class ApplicationDbContext : DbContext
    {
        public static string DefaultConnectionString =
            "Server=CCPL-1728;" +
            $"Database=LibsDbContext;" +
            "Trusted_Connection=True;" +
            "MultipleActiveResultSets=True";

        public virtual DbSet<Account> Accounts { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Person> Persons { get; set; }
        public virtual DbSet<Settings> Settings { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<Group> Groups { get; set; }
        public virtual DbSet<UserGroups> UserGroups { get; set; }
 
        public virtual DbSet<App> Apps { get; set; }
        public virtual DbSet<Page> Pages { get; set; }
        public virtual DbSet<Resource> Resources { get; set; }
        public virtual DbSet<Calendar> Calendars { get; set; }
        public virtual DbSet<News> News { get; set; }
        public virtual DbSet<Service> Services { get; set; }


        // факты 
        public virtual DbSet<LoginFact> LoginFacts { get; set; }

        // статистика
        public virtual DbSet<LoginDaily> LoginDaily { get; set; }
        public virtual DbSet<LoginWeekly> LoginWeekly { get; set; }



        public ApplicationDbContext( ) : base() { }

        public ApplicationDbContext(
                DbContextOptions<ApplicationDbContext> options) : base(options) { }


        /// <summary>
        /// Настройка конфигурации контекста данных
        /// </summary>
        /// <param name="optionsBuilder"></param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            Console.WriteLine($"OnConfiguring(DbContextOptionsBuilder optionsBuilder)");
            Debug.WriteLine($"OnConfiguring(DbContextOptionsBuilder optionsBuilder)");
            if (!optionsBuilder.IsConfigured)
            {
                Debug.WriteLine($"\t connectionString={DefaultConnectionString}");
                optionsBuilder.UseSqlServer(DefaultConnectionString);
            }
        }

        public new void SaveChanges()
        {
            base.SaveChanges();
        }

        public Task SaveChangesAsync()
        {
            return base.SaveChangesAsync();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using LibsDb;
using LibsDb.LibsEntities;

namespace LibsMVC.Areas.ReaderFace.Controllers
{
    [Area("ReaderFace")]
    public class DeliveryFactsController : Controller
    {
        private readonly LibsDbContext _context;

        public DeliveryFactsController(LibsDbContext context)
        {
            _context = context;
        }

        // GET: ReaderFace/DeliveryFacts
        public async Task<IActionResult> Index()
        {
            var libsDbContext = _context.DeliveryFacts.Include(d => d.Calendar).Include(d => d.Literature).Include(d => d.Order);
            return View(await libsDbContext.ToListAsync());
        }

        // GET: ReaderFace/DeliveryFacts/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var deliveryFact = await _context.DeliveryFacts
                .Include(d => d.Calendar)
                .Include(d => d.Literature)
                .Include(d => d.Order)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (deliveryFact == null)
            {
                return NotFound();
            }

            return View(deliveryFact);
        }

        // GET: ReaderFace/DeliveryFacts/Create
        public IActionResult Create()
        {
            ViewData["CalendarID"] = new SelectList(_context.Calendars, "ID", "ID");
            ViewData["LiteratureID"] = new SelectList(_context.Liters, "ID", "ID");
            ViewData["OrderID"] = new SelectList(_context.DeliveryOrders, "ID", "ID");
            return View();
        }

        // POST: ReaderFace/DeliveryFacts/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,LiteratureID,OrderID,Created,CalendarID")] DeliveryFact deliveryFact)
        {
            if (ModelState.IsValid)
            {
                _context.Add(deliveryFact);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CalendarID"] = new SelectList(_context.Calendars, "ID", "ID", deliveryFact.CalendarID);
            ViewData["LiteratureID"] = new SelectList(_context.Liters, "ID", "ID", deliveryFact.LiteratureID);
            ViewData["OrderID"] = new SelectList(_context.DeliveryOrders, "ID", "ID", deliveryFact.OrderID);
            return View(deliveryFact);
        }

        // GET: ReaderFace/DeliveryFacts/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var deliveryFact = await _context.DeliveryFacts.FindAsync(id);
            if (deliveryFact == null)
            {
                return NotFound();
            }
            ViewData["CalendarID"] = new SelectList(_context.Calendars, "ID", "ID", deliveryFact.CalendarID);
            ViewData["LiteratureID"] = new SelectList(_context.Liters, "ID", "ID", deliveryFact.LiteratureID);
            ViewData["OrderID"] = new SelectList(_context.DeliveryOrders, "ID", "ID", deliveryFact.OrderID);
            return View(deliveryFact);
        }

        // POST: ReaderFace/DeliveryFacts/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,LiteratureID,OrderID,Created,CalendarID")] DeliveryFact deliveryFact)
        {
            if (id != deliveryFact.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(deliveryFact);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DeliveryFactExists(deliveryFact.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CalendarID"] = new SelectList(_context.Calendars, "ID", "ID", deliveryFact.CalendarID);
            ViewData["LiteratureID"] = new SelectList(_context.Liters, "ID", "ID", deliveryFact.LiteratureID);
            ViewData["OrderID"] = new SelectList(_context.DeliveryOrders, "ID", "ID", deliveryFact.OrderID);
            return View(deliveryFact);
        }

        // GET: ReaderFace/DeliveryFacts/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var deliveryFact = await _context.DeliveryFacts
                .Include(d => d.Calendar)
                .Include(d => d.Literature)
                .Include(d => d.Order)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (deliveryFact == null)
            {
                return NotFound();
            }

            return View(deliveryFact);
        }

        // POST: ReaderFace/DeliveryFacts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var deliveryFact = await _context.DeliveryFacts.FindAsync(id);
            _context.DeliveryFacts.Remove(deliveryFact);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DeliveryFactExists(int id)
        {
            return _context.DeliveryFacts.Any(e => e.ID == id);
        }
    }
}

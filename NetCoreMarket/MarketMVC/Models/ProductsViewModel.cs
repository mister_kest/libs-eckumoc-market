﻿using ApplicationDb.Entities;
using Mvc;
using Mvc.API;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EcKuMoC.Models.Home
{
    public class ProductsViewModel: MvcComponent
    {
        public TreeNode categories { get; set; }
        public IEnumerable<MvcMarketPlace.Data.Entities.Product> products { get; set; }
        public IEnumerable<News> news { get; set; }
    }
}

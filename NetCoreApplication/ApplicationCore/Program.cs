﻿using ApplicationCore.Converter;
using ApplicationCore.CRUD;
using ApplicationCore.Domain.Odbc;
using ApplicationCore.Domain.Odbc.DataSource;

using ApplicationDb;
using ApplicationDb.Entities;

using Microsoft.EntityFrameworkCore.Metadata;

using Newtonsoft.Json.Linq;



using System;
using System.Diagnostics;

namespace ApplicationCore
{
    public class Program
    {


        /// <summary>
        /// Выполняется генерация элементов Rest-архитектуры
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
       
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                EntityRepository<User> repository = new EntityRepository<User>(db, db.Users, "User");
                foreach(INavigation nav in repository.GetNavigationProperties())
                {
                    Debug.WriteLine(nav.Name);
                }
            }

            /*Runtime.SetInterval(() => {
                HubSecureClient client = new HubSecureClient();
                client.Request(new RequestParams()
                {
                    path = "login.Login",
                    pars = JObject.FromObject(new
                    {
                        login = "eckumoc@gmail.com",
                        password = "sgdf1432"
                    })
                }, (resp) =>
                {
                    for (int i = 0; i < 100; i++)
                    {
                        Console.WriteLine("" + i + JObject.FromObject(resp).ToString());
                    }
                    return 1;
                });
            }, 1200);
            Thread.Sleep(Timeout.Infinite);*/
        }
    }
}

﻿ 
using SpbPublicLibsЬMVC.Mvc.API;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mvc.API
{
    public class TreeNode<T>: MvcComponent, ITreeNode<T>
    {
        private readonly T _item;
        private readonly Dictionary<string, ITreeNode<T>> _children;
        private ITreeNode<T> _parent;
        private string _name;
        public bool Expanded = true;
        public bool Editable = false;

        public TreeNode(TreeNode<T> parent, T item)
        {
            _item = item;
            _children = new Dictionary<string, ITreeNode<T>>();
        }

       
        public T GetItem()
        {
            return _item;
        }


        public void SetParent(ITreeNode<T> parent)
        {
            
            if ( _parent != null && _parent.GetChildren().ContainsValue(this))
            {                
                _parent.GetChildren().Remove(this.GetName());
            }
            _parent = parent;
            _parent.GetChildren()[GetName()] = this;
        }

        public void toggleExpanded()
        {

            Expanded = Expanded == true ? false : true;
        }


        public void toggleSelected()
        {
            Selected = Selected == true ? false : true;
        }

        



        public string GetName()
        {
            return _item.GetType().Name;
        }


        public Dictionary<string, ITreeNode<T>> GetChildren()
        {
            return _children;
        }
    }
}

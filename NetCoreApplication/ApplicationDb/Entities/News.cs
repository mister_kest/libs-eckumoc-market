﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ApplicationDb.Entities
{
    public class News 
    {
        public int ID { get; set; }


        [DisplayName("Заголовок")]
        [Required(ErrorMessage = "Необходимо указать заголовок сообщения")]
        public string Title { get; set; }


        [DisplayName("Время")]
        [DataType(DataType.DateTime)]
        public DateTime Time { get; set; }


        [DisplayName("URL-изображения")]
        public string Image { get; set; }


        [DisplayName("Ссылка на ресурс")]
        public string Href { get; set; }


        [DisplayName("Краткое описание")]
        [Required(ErrorMessage = "Необходимо ввести текст описывающий событие")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
    }
}

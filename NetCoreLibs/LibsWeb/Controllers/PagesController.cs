using System.Linq;
using ApplicationDb;
 using LibsDb; 
using LibsDb.LibsEntities;
using ApplicationDb.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;


[ApiController]
[Route("api/Pages/[action]")]
public class PagesController: ControllerBase
{
	private readonly LibsDbContext _db; 


	public PagesController( LibsDbContext db ){
		_db=db;
	}

	public void Create( Page record )	{
		_db.Pages.Add(record);
		_db.SaveChanges();
	}

	public void Remove( int id ){
		_db.Pages.Remove(this.Find(id));
		_db.SaveChanges();
	}

	public Page Find( int id )	{
		 return _db.Pages.Find(id);
	}

	public System.Collections.Generic.List<Page> List(   )	{
		 return _db.Pages.ToList();
	}

}
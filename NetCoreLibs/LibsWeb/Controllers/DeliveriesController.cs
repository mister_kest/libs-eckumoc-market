using System.Linq;
using ApplicationDb;
 using LibsDb; 
using LibsDb.LibsEntities;
using ApplicationDb.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;


[ApiController]
[Route("api/Deliveries/[action]")]
public class DeliveriesController: ControllerBase
{
	private readonly LibsDbContext _db; 


	public DeliveriesController( LibsDbContext db ){
		_db=db;
	}

	public void Create( DeliveryFact record )	{
		_db.DeliveryFacts.Add(record);
		_db.SaveChanges();
	}

	public void Remove( int id ){
		_db.DeliveryFacts.Remove(this.Find(id));
		_db.SaveChanges();
	}

	public DeliveryFact Find( int id )	{
		 return _db.DeliveryFacts.Find(id);
	}

	public System.Collections.Generic.List<DeliveryFact> List(   )	{
		 return _db.DeliveryFacts.ToList();
	}

}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using Newtonsoft.Json.Linq;

using ApplicationCore.Converter;
using ApplicationCore.Converter.Generators;
using ApplicationCore.Converter.Models;
using ApplicationCore.Domain;
using ApplicationCore.Domain.Odbc;
using ApplicationCore.Domain.Odbc.DataSource;
using ApplicationCore.Messaging;
using ApplicationCore.Testing;

using SpbPublicLibsUnit;

namespace SpbPublicLibsWeb.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class ApiController : Controller
    {
        private readonly ILogger<ApiController> _logger;
      //  private readonly IApplication _app;
        //private readonly TestSpbPublicLibs _tests;


        public ApiController(
            ILogger<ApiController> logger//,
            //TestSpbPublicLibs tests, 
            //IApplication app
        )
        {
            _logger = logger;
       //     _app = app;
            //_tests = tests;
            _logger.LogInformation("Create");
        }



        


        [HttpGet]
        public ResponseMessage GetDiagnosticReport()
        {            
            return new ResponseMessage()
            {
               // data = _app.GetApiController()
            };
        }


        [HttpGet]
        public string DoTests()
        {
            return "";// _tests.doTest().ToDocument();
        }



        [HttpGet]
        public string CreateClienApp()
        {
            string connectionString = "Server=DESKTOP-66CFM7U\\SQLSERVER;Database=spb-public-libs;integrated security=True;";
            string odbcConnectionString = RestfullOdbcExtensions.FromAdoToOdbcConnectionStringForSqlServer(connectionString);
            SqlServerOdbcDataSource dataSource = new SqlServerOdbcDataSource(odbcConnectionString);
            Console.WriteLine(JObject.FromObject(dataSource.GetDatabaseMetadata()));

            ConverterAngular ng = new ConverterAngular();
            ng.Do(dataSource, "SpbPublicLibsDbContext");
            ng.Save();
            return "ok";
        }



        [HttpGet]
        public ControllersMapModel GetControllersApi()
        {
            _logger.LogInformation("GetControllersApi");
            ServiceControllerGenerator controllerGeneration = new ServiceControllerGenerator();
            return controllerGeneration.GetControllersMap();
        }


        [HttpGet]
        public ActionResult GetControllersServices()
        {
            _logger.LogInformation("GetControllersServices");
            ServiceControllerGenerator controllerGeneration = new ServiceControllerGenerator();
            ControllersMapModel controllersMapModels = controllerGeneration.GetControllersMap();            
            return View("ServiceController", controllersMapModels[controllersMapModels.Keys.First()]);
        }
    }
}

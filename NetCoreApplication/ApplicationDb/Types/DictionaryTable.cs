﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace ApplicationDb.Types
{
    /// <summary>
    /// Справочник ( дополнительная информация, не обрабатываеся приложением )
    /// </summary>
    public class DictionaryTable
    {
        [DisplayName("Наименование")]
        public string Name { get; set; }
    }
}

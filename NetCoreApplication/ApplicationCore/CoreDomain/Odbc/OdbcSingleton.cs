﻿using Microsoft.Extensions.Logging;
using ApplicationCore.Domain.Odbc.Controllers;
using System.Collections.Concurrent;
using System.Linq;

namespace ApplicationCore.Domain.Odbc
{
    /// <summary>
    /// Сервис уровня приложения, хранит обьекты управления источниками данных ODBC.
    /// </summary>
    public class OdbcSingleton
    {
        /// <summary>
        /// True, если служба зарегистрирована
        /// </summary>
        public static bool CONFIGURED = false;

        private readonly ILogger<OdbcSingleton> _logger;

        /// <summary>
        /// Источники данных ODBC по строкам соединения
        /// </summary>
        private readonly ConcurrentDictionary<string, DatabaseManager> _databases =
            new ConcurrentDictionary<string, DatabaseManager>();


        public OdbcSingleton(ILogger<OdbcSingleton> logger)
        {
            _logger = logger;
            _logger.LogInformation("Create");
        }


        /// <summary>
        /// Метод получения обьекта управления источником данных ODBC.
        /// </summary>
        /// <param name="connectionString"></param>
        /// <returns> обьект управления источником данных ODBC </returns>
        public DatabaseManager Get( string connectionString=null )
        {
            if( connectionString==null)
            {
                if (_databases.Count > 0)
                {
                    return _databases.Values.First();
                }
                else
                {
                    throw new System.Exception("Источник данных ODBC не зарегистрирован");
                }
            }
            if (_databases.ContainsKey(connectionString) == false)
            {
                _databases[connectionString] = new DatabaseManager(connectionString);
            }
            return _databases[connectionString];
        }

    }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReaderOrderModule } from './reader-order/reader-order.module';
import { ReaderOrderComponent } from './reader-order/reader-order.component';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ReaderOrderModule
  ],
  exports:[
    ReaderOrderModule
  ]
})
export class UserReaderModule {
  static routes = [
    {path: '',        redirectTo: 'order', pathMatch: 'full'},
    {path: 'order',   component: ReaderOrderComponent }
  ];
}

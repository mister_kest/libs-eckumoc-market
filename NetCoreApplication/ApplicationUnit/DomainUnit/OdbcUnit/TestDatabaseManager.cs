﻿using SpbPublicLibsCore.Testing;
using SpbPublicLibsCore.Domain.Odbc.Controllers;
using SpbPublicLibsCore.Domain.Odbc.DataSource;
using System.Collections.Generic;

namespace SpbPublicLibsUnit
{
    public class TestDatabaseManager :  TestingUnit
    {
        protected override void onTest()
        {
            canConnectMysqlSqlServerPostgre();
            canReadDatabaseMetadata();
            canCreateReadUpdateDelete();
            canSearch();
            canInsertDataIntoDatabase();
        }


        protected void canInsertDataIntoDatabase()
        {
            DatabaseManager manager = new DatabaseManager(new MySqlOdbcDataSource());
            TableManager accountManager = ((TableManagerStatefull)manager.GetFasade()["accounts"]).tableManager;
            long before = accountManager.Count();
             ;

            Dictionary<string, object> values = new Dictionary<string, object>();
            values["account_username"] = "";
            values["account_email"] = "";
            values["account_hashcode"] = "";

            accountManager.Create(values);
           
            if ((accountManager.Count() - before) != -1)
            {
                throw new System.Exception();
            }
            else
            {
                this.messages.Add("can create new records in databse");
            }
        }

        private void canConnectMysqlSqlServerPostgre()
        {
            DatabaseManager dbm = new DatabaseManager();
            // TODO:
        }

        private void canReadDatabaseMetadata()
        {
            DatabaseManager dbm = new DatabaseManager();
            
            //TODO: 
        }

        private void canSearch()
        {
            DatabaseManager dbm = new DatabaseManager();
            dbm.discovery();
            this.messages.Add("can create full text indexes");
        }

        private void canCreateReadUpdateDelete()
        {
            DatabaseManager dbm = new DatabaseManager();
            dbm.discovery();
            //TODO: 
            this.messages.Add("can create full text indexes");
        }
    }
}

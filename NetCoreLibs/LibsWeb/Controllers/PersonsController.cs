using System.Linq;
using ApplicationDb;
 using LibsDb; 
using LibsDb.LibsEntities;
using ApplicationDb.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;


[ApiController]
[Route("api/Persons/[action]")]
public class PersonsController: ControllerBase
{
	private readonly LibsDbContext _db; 


	public PersonsController( LibsDbContext db ){
		_db=db;
	}

	public void Create( Person record )	{
		_db.Persons.Add(record);
		_db.SaveChanges();
	}

	public void Remove( int id ){
		_db.Persons.Remove(this.Find(id));
		_db.SaveChanges();
	}

	public Person Find( int id )	{
		 return _db.Persons.Find(id);
	}

	public System.Collections.Generic.List<Person> List(   )	{
		 return _db.Persons.ToList();
	}

}
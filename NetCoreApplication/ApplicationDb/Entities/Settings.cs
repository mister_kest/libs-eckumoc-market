﻿ 

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ApplicationDb.Entities
{
    public partial class Settings
    {
 
        [Key]
        public int ID { get; set; }


        [DisplayName("Помощь")]
        public bool Help { get; set; }


        [DisplayName("Темный оттенок")]
        public bool Dark { get; set; }


        [DisplayName("Мобильный режим")]
        public bool Mobile { get; set; }

    }
}

﻿using SpbPublicLibsCore.APIS.TheMovieDatabaseAPI;
using SpbPublicLibsCore.Domain.Reflection;
using SpbPublicLibsCore.Testing;
using SpbPublicLibsCore.Utils;

namespace SpbPublicLibsUnit.APISUnit
{

    public class TestTheMovieDatabaseAPI : TestingUnit
    {
        protected override void onTest()
        {
            TheMovieDatabaseService api = new TheMovieDatabaseService();
            object x;
            foreach (var m in api.GetType().GetMethods())
            {
                x = ("x=(api." + m.Name + "().Result);");
            }

            x = (api.getTvOnAiring().Result);
            x = (api.getTvTodayAiring().Result);
            x = (api.getTvTopRated().Result);
            x = (api.getTvPopular().Result);

            x = (api.getPersonExternalIds(208225).Result);
            x = (api.getPersonImages(1910848).Result);
            x = (api.getPersonTaggedImages(208225).Result);
            x = (api.getPersonChanges().Result);
            x = (api.getPersonPopular().Result);
            x = (api.getPersonLatest().Result);

            x = (api.getMovieChanges().Result);
            x = (api.getCompany(60992).Result);
            x = (api.getCompanyMovies(12299).Result);
            x = (api.getMovieKeywords(439981).Result);
            x = (api.getMovieTranslations(442405).Result);
            x = (api.getMovieSimilarMovies(369925).Result);
            x = (api.getMoviesTopRated().Result);
            x = (api.getMoviesPopular().Result);
            x = (api.getMoviesLatest().Result);
            x = (api.getMovieReviews(439981).Result);
            x = (api.getMovieTrailers(442405).Result);
            x = (api.getMovieReleases(369925).Result);
            x = (api.getPersonChanges().Result);
            x = (api.configuration().Result);
            x = (api.keywordSearch("love", 1).Result);
            x = (api.collectionSearch("love", 1).Result);
            x = (api.personSearch("love", 1).Result);
            x = (api.movieSearch("love", 1).Result);
            x = (api.tvSearch("love", 1).Result);
            x = (api.companySearch("love", 1).Result);

            foreach( var m in new ReflectionService().GetOwnPublicMethods(this.GetType()))
            {
                this.messages.Add("can " + Naming.SplitName(m.Name));
            }

            //api.companySearch("cinema",1 ).Result;  =>companies: 60992,12299
            //string json1 = api.getCompanyMovies(60992).Result;
            //string json2 = api.getCompanyMovies(12299).Result;
            //string json = api.getCompany(60992).Result;
            //x=("\n\n\n"+json);
            //x=("\n\n\n" + json1);
            //x=("\n\n\n" + json2);

            //movies: 439981,442405,369925
            /*x=("\n\n\n" + api.getMovieReviews(439981).Result);
            x=("\n\n\n" + api.getMovieTranslations(442405).Result);
            x=("\n\n\n" + api.getMovieKeywords(369925).Result);
            x=("\n\n\n" + api.getMovieSimilarMovies(369925).Result);*/
            //x=("\n\n\n" + api.getMoviesLatest().Result);
            //x=("\n\n\n" + api.getMoviesPopular().Result);

            //1910848,208225
            //x=("\n\n\n" + api.getPersonPopular().Result);
            //x=("\n\n\n" + api.getPersonTaggedImages(1910848).Result);
            //x=("\n\n\n" + api.getPersonImages(208225).Result);
            //x=("\n\n\n" + api.getTvTopRated().Result);
        }
    }
}

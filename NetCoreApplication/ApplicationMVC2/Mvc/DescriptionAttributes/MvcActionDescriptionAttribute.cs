﻿using Microsoft.AspNetCore.Mvc.ApplicationModels;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpbPublicLibsWeb.MVC.DescriptionAttributes
{
    public class MvcActionDescriptionAttribute : Attribute, IActionModelConvention
    {
        private readonly string _description;

        public MvcActionDescriptionAttribute(string description)
        {
            _description = description;
        }

        public void Apply(ActionModel actionModel)
        {
            actionModel.Properties["description"] = _description;
        }
    }
}

﻿//using MailKit.Net.Pop3;
//using MailKit.Net.Smtp;
//using MimeKit;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace eckumoc.Services
{
    /*
    /// <summary>
    /// Service communicate by email message transport protocol.
    /// </summary>
    internal class EmailService
    {
        private string emailName;
        private string emailAddress;
        private string smtpHost;
        private int smtpPort;
        private string pop3Host;
        private int pop3Port;
        private bool useSsl;
        private string username;
        private string password;

        /// <summary>
        /// Default constructor with default parameters
        /// </summary>
        public EmailService()
        {
            this.emailName = "EcKuMoC:support";
            this.emailAddress = "kba-2018@mail.ru";
            this.smtpHost = "smtp.mail.ru";
            this.smtpPort = 465;
            this.pop3Host = "pop.mail.ru";
            this.pop3Port = 995;
            this.useSsl = true;
            this.username = "kba-2018@mail.ru";
            this.password = "eckumoc1423";
        }

        /// <summary>
        /// Creates new instance of email service
        /// </summary>
        /// <param name="emailName">email name</param>
        /// <param name="emailAddress">email address</param>
        /// <param name="smtpHost">url of smtp host</param>
        /// <param name="smtpPort">smtp port</param>
        /// <param name="pop3Host">url of pop host</param>
        /// <param name="pop3Port">pop port</param>
        /// <param name="useSsl">enables ssl</param>
        /// <param name="username">username</param>
        /// <param name="password">password</param>
        public EmailService(string emailName,
                             string emailAddress,
                             string smtpHost,
                             int smtpPort,
                             string pop3Host,
                             int pop3Port,
                             bool useSsl,
                             string username,
                             string password)
        {
            this.emailName = emailName;
            this.emailAddress = emailAddress;
            this.smtpHost = smtpHost;
            this.smtpPort = smtpPort;
            this.pop3Host = pop3Host;
            this.pop3Port = pop3Port;
            this.useSsl = useSsl;
            this.username = username;
            this.password = password;
        }

        /// <summary>
        /// Send text message by email
        /// </summary>
        /// <param name="email">email address of recipient</param>
        /// <param name="subject">subject of message</param>
        /// <param name="message">message text</param>
        public void SendEmail(string email, string subject, string message)
        {
            var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress(this.emailName, this.emailAddress));
            emailMessage.To.Add(new MailboxAddress("", email));
            emailMessage.Subject = subject;
            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = message
            };
            //emailMessage.Attachments
            using (var client = new SmtpClient())
            {
                client.Connect(this.smtpHost, this.smtpPort, this.useSsl);
                client.Authenticate(this.username, this.password);
                client.Send(emailMessage);
                client.Disconnect(true);
            }
        }


        /// <summary>
        /// Send text message by email
        /// </summary>
        /// <param name="email">email address of recipient</param>
        /// <param name="subject">subject of message</param>
        /// <param name="message">message text</param>
        public async Task SendEmailAync(string email, string subject, string message)
        {
            var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress(this.emailName, this.emailAddress));
            emailMessage.To.Add(new MailboxAddress("", email));
            emailMessage.Subject = subject;
            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = message
            };
            //emailMessage.Attachments
            using (var client = new SmtpClient())
            {
                client.Connect(this.smtpHost, this.smtpPort, this.useSsl);
                client.Authenticate(this.username, this.password);
                await client.SendAsync(emailMessage);
                client.Disconnect(true);
            }
        }


        /// <summary>
        /// Recieve list of messages inbox.
        /// </summary>
        /// <returns>list mime messages</returns>
        public List<MimeMessage> Recieve()
        {
            using (var client = new Pop3Client())
            {
                List<MimeMessage> messages = new List<MimeMessage>();
                client.Connect(this.pop3Host, this.pop3Port, this.useSsl);
                //client.AuthenticationMechanisms.Remove("XOAUTH2");
                client.Authenticate(this.username, this.password);
                for (int i = 0; i < client.Count; i++)
                {
                    MimeMessage message = client.GetMessage(i);
                    messages.Add(message);
                }
                client.Disconnect(true);
                return messages;
            }
        }

        /// <summary>
        /// Recieve list of messages inbox.
        /// </summary>
        /// <returns>list mime messages</returns>
        public async Task<List<MimeMessage>> RecieveAsync()
        {
            using (var client = new Pop3Client())
            {
                List<MimeMessage> messages = new List<MimeMessage>();
                client.Connect(this.pop3Host, this.pop3Port, this.useSsl);
                client.AuthenticationMechanisms.Remove("XOAUTH2");
                client.Authenticate(this.username, this.password);
                for (int i = 0; i < client.Count; i++)
                {
                    MimeMessage message = await client.GetMessageAsync(i);
                    messages.Add(message);
                }
                client.Disconnect(true);
                return messages;
            }
        }
    }*/
}

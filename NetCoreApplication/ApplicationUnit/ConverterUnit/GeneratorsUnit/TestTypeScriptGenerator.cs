﻿using eckumoc_netcore_codegen;

using SpbPublicLibsCore.Domain.Odbc.DataSource;
using SpbPublicLibsCore.Testing;
using System;

namespace SpbPublicLibsCore.Converter.Generators
{
    public class TestTypeScriptGenerator : TestingUnit
    {
        protected override void onTest()
        {
            MySqlOdbcDataSource datasource = new MySqlOdbcDataSource();
            TypeScriptGenerator generator = new TypeScriptGenerator();
            foreach(var pfile in generator.createDataContext(datasource.GetDatabaseMetadata()))
            {
                //Console.WriteLine(pfile.Key);
                //Console.WriteLine(pfile.Value);
            }
            this.messages.Add("can generate data model into type script");
        }
    }
}

﻿using SpbPublicLibsCore.Testing;
using System;

namespace SpbPublicLibsUnit.DomainUnit
{
    public class TestDomainUser : TestingUnit
    {

        public TestDomainUser(
            TestUserMessages testUserMessages,
            TestUserMovies testUserMovies)
        {
            Push(testUserMovies);
            Push(testUserMessages);
        }

        protected override void onTest()
        {
            //messages.Add("");
        }
    }
}

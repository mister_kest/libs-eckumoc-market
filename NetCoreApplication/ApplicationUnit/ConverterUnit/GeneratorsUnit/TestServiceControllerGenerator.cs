﻿using SpbPublicLibsCore.Testing;

using System;

namespace SpbPublicLibsCore.Converter.Generators
{
    public class TestServiceControllerGenerator : TestingUnit
    {
        protected override void onTest()
        {         
            ServiceControllerGenerator generator = new ServiceControllerGenerator();
            foreach (Type ptable in AssemblyReader.GetControllers())
            {                 
                //Console.WriteLine(generator.CreateServiceController(ptable));
            }
            messages.Add("can generate service controllers");
        }
    }
}

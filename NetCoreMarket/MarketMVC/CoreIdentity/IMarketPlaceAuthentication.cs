﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MvcMarketPlace.Core
{
    public interface IMarketPlaceAuthentication
    {
        bool Validate();
        bool Login(string email,string password,bool rememberMe);
        bool Logout();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;

using ApplicationCore.Messaging;
using ApplicationCore.Utils;

namespace ApplicationCore.Domain.Proxy
{
    /// <summary>
    /// Посредник между потребителем и сервисом
    /// </summary>
    public class ProxyComponent: BaseComponent

    {
        private readonly string _url;
        private readonly string _route;
        private readonly ILogger<ProxyComponent> _logger;
    
        private readonly Func<RequestMessage,int> _commit;

        public ProxyComponent(
            RequestDelegate next, 
            ILogger<ProxyComponent> logger, 
            string route, string url,
            Func<RequestMessage, int> commit): base(next) {         
         
            _logger = logger;
            _url = url;
            _commit = commit;
            _route = route;
            _logger.LogInformation("Create");
        }


        /*/// <summary>
        /// Запрос http-абстрагируется 
        /// </summary>
        /// <param name="httpContext"> контекст протокола </param>
        /// <param name="_next"> следующий делегат </param>
        /// <param name="app"> сервис уровня приложения </param>
        /// <param name="auth"> сервис проверки подлинности </param>
        public Task Invoke(
            HttpContext httpContext,             
            IApplication app,
            IAuthentication auth)
        {
            _logger.LogInformation("Invoke");
            RequestMessage request = this.toMessageRequest(httpContext);
            ResponseMessage response = new ResponseMessage();
            response.request = request;
            _commit(request);
            this.OnMessage(request, response);
            if( response.confirmed )
            {
                httpContext.Response.WriteAsync(JObject.FromObject(response).ToString());
                _logger.LogInformation($"Confirmed: {_route}");
                return null;
            }
            else
            {
                return _next.Invoke(httpContext);
            }
                                    
        }*/


         
        /// <summary>
        /// Метод обработки сообщения
        /// </summary>
        /// <param name="request"> запрос </param>
        /// <param name="response"> ответ </param>
        public override void OnMessage(RequestMessage request, ResponseMessage response)
        {
            _logger.LogInformation("OnMessage");
            _logger.LogInformation(JObject.FromObject(request).ToString());
            _commit(request);
            if (!request.request.path.StartsWith(_route))
            {
                return;
            }
            request.request.path = request.request.path.Length == _route.Length ? "" : request.request.path.Length>(_route.Length+1)? request.request.path.Substring(_route.Length+1): "";
            string uri = request.request.path.StartsWith("/")==false?"/" + request.request.path: request.request.path;
            string requestUrl = (_url + $"/{_route}/"+uri).Replace(@"//", "/").Replace(@"//", "/").Replace(@":/", "://");
            requestUrl += this.toHttpParams(request.getParametersMap());
            _logger.LogInformation("url: "+requestUrl );
            response.confirmed = false;

            _logger.LogInformation("Message redirect to: "+ requestUrl);
            HttpClient client = new HttpClient();
            HttpResponseMessage httpResponse = client.GetAsync(requestUrl).Result;
            try
            {
                //httpResponse.EnsureSuccessStatusCode();
                response.confirmed = true;
                response.data = Newtonsoft.Json.JsonConvert.DeserializeObject(
                    httpResponse.Content.ReadAsStringAsync().Result);
            }
            catch( Exception ex )
            {                
                _logger.LogError(ex.Message);
                response.confirmed = false;
            }                                    
        }


        


        


        /// <summary>
        /// Извлечение параметров запроса и контекста
        /// </summary>
        /// <param name="httpContext"> контекст протокола </param>
        /// <returns></returns>
        private JObject getHttpParams(HttpContext httpContext)
        {
            Dictionary<string, object> pars = new Dictionary<string, object>();            
            foreach (var p in httpContext.Request.Query)
            {
                pars[p.Key] = p.Value;
            }
            return JObject.FromObject(pars);
        }


        /// <summary>
        /// Считывание бинарных данных в основном блоке сообщения
        /// </summary>
        /// <param name="httpContext"> контекст протокола </param>
        /// <returns></returns>
        private byte[] ReadRequestBody(HttpContext httpContext)
        {
            long? length = httpContext.Request.ContentLength;
            if (length != null)
            {
                byte[] data = new byte[(long)length];
                httpContext.Request.Body.Read(data, 0, (int)length);
                string mime = httpContext.Request.ContentType;
                return data;
            }
            return new byte[0];
        }

        protected override void OnException(Exception ex)
        {
            throw new NotImplementedException();
        }
    }
}

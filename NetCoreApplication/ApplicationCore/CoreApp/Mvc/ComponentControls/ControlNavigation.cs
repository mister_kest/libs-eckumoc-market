﻿using Mvc;

using System;

namespace SpbPublicLibsЬMVC.Mvc.ComponentControls
{
    public class ControlNavigation : MvcComponent
    {
        private Action<string> Behaviour;

        public ControlNavigation(): base()
        {
            Add( new NavigationMenu("Персональная информация") );
        }

        

        public void Subscribe(Action<string> listener)
        {
            Behaviour = listener;
        }

        public void Changed(string message)
        {
            Behaviour(message);
        }
    }
}
;
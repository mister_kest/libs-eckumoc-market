using System.Linq;
using ApplicationDb;
 using LibsDb; 
using LibsDb.LibsEntities;
using ApplicationDb.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;


[ApiController]
[Route("api/Liters/[action]")]
public class LitersController: ControllerBase
{
	private readonly LibsDbContext _db; 


	public LitersController( LibsDbContext db ){
		_db=db;
	}

	public void Create( Liter record )	{
		_db.Liters.Add(record);
		_db.SaveChanges();
	}

	public void Remove( int id ){
		_db.Liters.Remove(this.Find(id));
		_db.SaveChanges();
	}

	public Liter Find( int id )	{
		 return _db.Liters.Find(id);
	}

	public System.Collections.Generic.List<Liter> List(   )	{
		 return _db.Liters.ToList();
	}

}
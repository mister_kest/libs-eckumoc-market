﻿using AngleSharp.Common;

using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using SpbPublicLibsCore.APIS.TheMovieDatabaseAPI;
using SpbPublicLibsCore.Domain.Reflection;
using SpbPublicLibsCore.Testing;
using SpbPublicLibsCore.Utils;
using SpbPublicLibsDb.DAL;

using System;

namespace SpbPublicLibsUnit
{
    public class TestReflection : TestingUnit
    {
        protected override void onTest( )
        {
            canUseReflectionScope();
            canParseReflection();
            canInvokeReflectiveMethod();            
        }

        private void canUseReflectionScope()
        {
            ReflectionScope scope = new ReflectionScope("", new TheMovieDatabaseService());
            var p = new {
                fields = scope.ListPropertyNames(),
                actions = scope.ListMethodNames(),
            };
            messages.Add("can use reflection scope");
        }

        private void canInvokeReflectiveMethod()
        {
            ReflectionService service = new ReflectionService(null);
            service.GetSkeleton(new TheMovieDatabaseService());
            messages.Add("can parse reflection");
        }


        private void canParseReflection()
        {
            ReflectionService service = new ReflectionService(null);
            Console.WriteLine(JObject.FromObject(service.GetSkeleton(new TheMovieDatabaseService())));          
            messages.Add("can parse reflection");
        }
    }
}

﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace SpbPublicLibsWebAPI.Controllers
{
    public class HomeController : Controller
    {

    
        private readonly TopNews _news;
        private readonly ILogger<HomeController> _logger;
    

        public HomeController(
          
            TopNews news,
            ILogger<HomeController> logger )
        {
           
            _news = news;
            _logger = logger;
            _logger.LogInformation("Create");
        }



        public IActionResult SeeMostPopularMovies()
        {
            return RedirectToAction("Movies","/Index?movies=SeeMostPopularMovies");
        }



        public IActionResult SeeNewestMovies()
        {
            return RedirectToAction("Movies", "/Index?movies=SeeNewestMovies");
            
        }

        public IActionResult SeeTopRatedMovies()
        {
            return RedirectToAction("Movies", "/Index?movies=SeeTopRatedMovies");
        }

       
        public IActionResult Products()
        {            
            return View();
        }

        public IActionResult Markets()
        {
            return View();
        }

        public IActionResult Statistics()
        {
            return View();
        }




        





        

         

    }
}

﻿using Newtonsoft.Json.Linq;

using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.Utils
{
    public class Writing
    {
        public static void ToConsole( object target )
        {
            Console.WriteLine(JObject.FromObject(target).ToString());
        }
    }
}

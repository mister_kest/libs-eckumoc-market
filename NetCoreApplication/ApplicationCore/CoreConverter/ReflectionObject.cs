﻿using ApplicationCore.Converter.Models;

using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace ApplicationCore.Domain.Reflection
{
    public class ReflectionObject
    {
        private readonly object _target = null;
        private readonly Dictionary<string, ControllerActionModel> _actions;

        public ReflectionObject( object target )
        {
            _target = target;
            _actions = new Dictionary<string, ControllerActionModel>();
            _init();
        }

        private void _init()
        {
            _actions.Clear();
            foreach(MethodInfo methodInfo in _target.GetType().GetMethods())
            {
                _actions[methodInfo.Name] = new ControllerActionModel();
            }
        }
         
    }
}

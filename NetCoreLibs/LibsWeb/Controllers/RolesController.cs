using System.Linq;
using ApplicationDb;
 using LibsDb; 
using LibsDb.LibsEntities;
using ApplicationDb.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;


[ApiController]
[Route("api/Roles/[action]")]
public class RolesController: ControllerBase
{
	private readonly LibsDbContext _db; 


	public RolesController( LibsDbContext db ){
		_db=db;
	}

	public void Create( Role record )	{
		_db.Roles.Add(record);
		_db.SaveChanges();
	}

	public void Remove( int id ){
		_db.Roles.Remove(this.Find(id));
		_db.SaveChanges();
	}

	public Role Find( int id )	{
		 return _db.Roles.Find(id);
	}

	public System.Collections.Generic.List<Role> List(   )	{
		 return _db.Roles.ToList();
	}

}
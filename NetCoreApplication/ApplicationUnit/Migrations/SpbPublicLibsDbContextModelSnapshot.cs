﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using SpbPublicLibsDb.DAL;

namespace SpbPublicLibsWebAPI.Migrations
{
    [DbContext(typeof(SpbPublicLibsDbContext))]
    partial class SpbPublicLibsDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.8")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("PublicAuthor", b =>
                {
                    b.Property<string>("PublicAuthorId")
                        .HasColumnName("PublicAuthorId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("AuthorNationality")
                        .HasColumnName("AuthorNationality")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("Created")
                        .HasColumnType("datetime2");

                    b.Property<int>("Popularity")
                        .HasColumnType("int");

                    b.Property<string>("PublicAlias")
                        .HasColumnName("PublicAlias")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("Rating")
                        .HasColumnType("int");

                    b.Property<DateTime>("Updated")
                        .HasColumnType("datetime2");

                    b.Property<int>("Version")
                        .HasColumnType("int");

                    b.HasKey("PublicAuthorId");

                    b.ToTable("PublicAuthors");
                });

            modelBuilder.Entity("PublicBlog", b =>
                {
                    b.Property<string>("PublicBlogId")
                        .HasColumnName("PublicBlogId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<int>("BlogRating")
                        .HasColumnName("BlogRating")
                        .HasColumnType("int");

                    b.Property<string>("BlogUrl")
                        .HasColumnName("BlogUrl")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("Created")
                        .HasColumnType("datetime2");

                    b.Property<int>("PPublicAuthorId")
                        .HasColumnType("int");

                    b.Property<string>("PPublicAuthorPublicAuthorId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<int>("Popularity")
                        .HasColumnType("int");

                    b.Property<int>("PublicAuthorId")
                        .HasColumnName("PublicAuthorId")
                        .HasColumnType("int");

                    b.Property<int>("Rating")
                        .HasColumnType("int");

                    b.Property<DateTime>("Updated")
                        .HasColumnType("datetime2");

                    b.Property<int>("Version")
                        .HasColumnType("int");

                    b.HasKey("PublicBlogId");

                    b.HasIndex("PPublicAuthorPublicAuthorId");

                    b.ToTable("PublicBlogs");
                });

            modelBuilder.Entity("PublicCatalog", b =>
                {
                    b.Property<string>("PublicCatalogId")
                        .HasColumnName("PublicCatalogId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<int>("CatalogDatabaseId")
                        .HasColumnName("CatalogDatabaseId")
                        .HasColumnType("int");

                    b.Property<int>("CatalogServerId")
                        .HasColumnName("CatalogServerId")
                        .HasColumnType("int");

                    b.Property<int>("CatalogdatabasePublicdatabaseid")
                        .HasColumnName("CatalogDatabase_PublicDatabaseId")
                        .HasColumnType("int");

                    b.Property<int>("CatalogserverPublicserverid")
                        .HasColumnName("CatalogServer_PublicServerId")
                        .HasColumnType("int");

                    b.Property<DateTime>("Created")
                        .HasColumnType("datetime2");

                    b.Property<int>("PPublicDatabaseId")
                        .HasColumnType("int");

                    b.Property<string>("PPublicDatabasePublicDatabaseId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<int>("PPublicLibId")
                        .HasColumnType("int");

                    b.Property<string>("PPublicLibPublicLibId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<int>("PPublicServerId")
                        .HasColumnType("int");

                    b.Property<string>("PPublicServerPublicServerId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<int>("Popularity")
                        .HasColumnType("int");

                    b.Property<int>("PublicLibId")
                        .HasColumnName("PublicLibId")
                        .HasColumnType("int");

                    b.Property<int>("Rating")
                        .HasColumnType("int");

                    b.Property<DateTime>("Updated")
                        .HasColumnType("datetime2");

                    b.Property<int>("Version")
                        .HasColumnType("int");

                    b.HasKey("PublicCatalogId");

                    b.HasIndex("PPublicDatabasePublicDatabaseId");

                    b.HasIndex("PPublicLibPublicLibId");

                    b.HasIndex("PPublicServerPublicServerId");

                    b.ToTable("PublicCatalogs");
                });

            modelBuilder.Entity("PublicCustomer", b =>
                {
                    b.Property<string>("PublicCustomerId")
                        .HasColumnName("PublicCustomerId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<DateTime>("Created")
                        .HasColumnType("datetime2");

                    b.Property<int>("Popularity")
                        .HasColumnType("int");

                    b.Property<int>("Rating")
                        .HasColumnType("int");

                    b.Property<DateTime>("Updated")
                        .HasColumnType("datetime2");

                    b.Property<int>("Version")
                        .HasColumnType("int");

                    b.HasKey("PublicCustomerId");

                    b.ToTable("PublicCustomers");
                });

            modelBuilder.Entity("PublicDatabase", b =>
                {
                    b.Property<string>("PublicDatabaseId")
                        .HasColumnName("PublicDatabaseId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<DateTime>("Created")
                        .HasColumnType("datetime2");

                    b.Property<int>("PPublicDatabaseId")
                        .HasColumnType("int");

                    b.Property<string>("PPublicDatabasePublicDatabaseId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<int>("Popularity")
                        .HasColumnType("int");

                    b.Property<int>("Rating")
                        .HasColumnType("int");

                    b.Property<int>("ServerdatabasePublicdatabaseid")
                        .HasColumnName("ServerDatabase_PublicDatabaseId")
                        .HasColumnType("int");

                    b.Property<DateTime>("Updated")
                        .HasColumnType("datetime2");

                    b.Property<int>("Version")
                        .HasColumnType("int");

                    b.HasKey("PublicDatabaseId");

                    b.HasIndex("PPublicDatabasePublicDatabaseId");

                    b.ToTable("PublicDatabases");
                });

            modelBuilder.Entity("PublicJournal", b =>
                {
                    b.Property<string>("PublicJournalId")
                        .HasColumnName("PublicJournalId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<DateTime>("Created")
                        .HasColumnType("datetime2");

                    b.Property<int>("JournalLiteratureId")
                        .HasColumnName("JournalLiteratureId")
                        .HasColumnType("int");

                    b.Property<int>("PPublicLiteratureId")
                        .HasColumnType("int");

                    b.Property<string>("PPublicLiteraturePublicLiteratureId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<int>("Popularity")
                        .HasColumnType("int");

                    b.Property<int>("Rating")
                        .HasColumnType("int");

                    b.Property<DateTime>("Updated")
                        .HasColumnType("datetime2");

                    b.Property<int>("Version")
                        .HasColumnType("int");

                    b.HasKey("PublicJournalId");

                    b.HasIndex("PPublicLiteraturePublicLiteratureId");

                    b.ToTable("PublicJournals");
                });

            modelBuilder.Entity("PublicLib", b =>
                {
                    b.Property<string>("PublicLibId")
                        .HasColumnName("PublicLibId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<DateTime>("Created")
                        .HasColumnType("datetime2");

                    b.Property<string>("Name")
                        .HasColumnName("Name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("NameShort")
                        .HasColumnName("NameShort")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("Popularity")
                        .HasColumnType("int");

                    b.Property<int>("Rating")
                        .HasColumnType("int");

                    b.Property<string>("Sigla")
                        .HasColumnName("Sigla")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("Updated")
                        .HasColumnType("datetime2");

                    b.Property<string>("Url")
                        .HasColumnName("Url")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("Version")
                        .HasColumnType("int");

                    b.HasKey("PublicLibId");

                    b.ToTable("PublicLibs");
                });

            modelBuilder.Entity("PublicLiterature", b =>
                {
                    b.Property<string>("PublicLiteratureId")
                        .HasColumnName("PublicLiteratureId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<DateTime>("Created")
                        .HasColumnType("datetime2");

                    b.Property<int>("Popularity")
                        .HasColumnType("int");

                    b.Property<int>("Rating")
                        .HasColumnType("int");

                    b.Property<DateTime>("Updated")
                        .HasColumnType("datetime2");

                    b.Property<int>("Version")
                        .HasColumnType("int");

                    b.HasKey("PublicLiteratureId");

                    b.ToTable("PublicLiteratures");
                });

            modelBuilder.Entity("PublicPost", b =>
                {
                    b.Property<string>("PublicPostId")
                        .HasColumnName("PublicPostId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<int>("BlogId")
                        .HasColumnName("BlogId")
                        .HasColumnType("int");

                    b.Property<DateTime>("Created")
                        .HasColumnType("datetime2");

                    b.Property<int>("PPublicBlogId")
                        .HasColumnType("int");

                    b.Property<string>("PPublicBlogPublicBlogId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<int>("Popularity")
                        .HasColumnType("int");

                    b.Property<string>("PostContent")
                        .HasColumnName("PostContent")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("PostTitle")
                        .HasColumnName("PostTitle")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("PublicblogPublicblogid")
                        .HasColumnName("PublicBlog_PublicBlogId")
                        .HasColumnType("int");

                    b.Property<int>("Rating")
                        .HasColumnType("int");

                    b.Property<DateTime>("Updated")
                        .HasColumnType("datetime2");

                    b.Property<int>("Version")
                        .HasColumnType("int");

                    b.HasKey("PublicPostId");

                    b.HasIndex("PPublicBlogPublicBlogId");

                    b.ToTable("PublicPosts");
                });

            modelBuilder.Entity("PublicReader", b =>
                {
                    b.Property<string>("PublicReaderId")
                        .HasColumnName("PublicReaderId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<DateTime>("Created")
                        .HasColumnType("datetime2");

                    b.Property<int>("PUserAccountId")
                        .HasColumnType("int");

                    b.Property<string>("PUserAccountUsername")
                        .IsRequired()
                        .HasColumnType("nvarchar(450)");

                    b.Property<int>("Popularity")
                        .HasColumnType("int");

                    b.Property<int>("Rating")
                        .HasColumnType("int");

                    b.Property<int>("ReaderAccountId")
                        .HasColumnName("ReaderAccountId")
                        .HasColumnType("int");

                    b.Property<string>("ReaderaccountUseraccountid")
                        .HasColumnName("ReaderAccount_UserAccountId")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("Updated")
                        .HasColumnType("datetime2");

                    b.Property<int>("Version")
                        .HasColumnType("int");

                    b.HasKey("PublicReaderId");

                    b.HasIndex("PUserAccountUsername");

                    b.ToTable("PublicReaders");
                });

            modelBuilder.Entity("PublicServer", b =>
                {
                    b.Property<string>("PublicServerId")
                        .HasColumnName("PublicServerId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<DateTime>("Created")
                        .HasColumnType("datetime2");

                    b.Property<int>("Popularity")
                        .HasColumnType("int");

                    b.Property<int>("Rating")
                        .HasColumnType("int");

                    b.Property<string>("ServerIp")
                        .HasColumnName("ServerIp")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("ServerName")
                        .HasColumnName("ServerName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("Updated")
                        .HasColumnType("datetime2");

                    b.Property<int>("Version")
                        .HasColumnType("int");

                    b.HasKey("PublicServerId");

                    b.ToTable("PublicServers");
                });

            modelBuilder.Entity("StatisticsCalendar", b =>
                {
                    b.Property<string>("StatisticsCalendarId")
                        .HasColumnName("StatisticsCalendarId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<DateTime>("Created")
                        .HasColumnType("datetime2");

                    b.Property<int>("Day")
                        .HasColumnName("Day")
                        .HasColumnType("int");

                    b.Property<int>("Hour")
                        .HasColumnName("Hour")
                        .HasColumnType("int");

                    b.Property<int>("Minute")
                        .HasColumnName("Minute")
                        .HasColumnType("int");

                    b.Property<int>("Month")
                        .HasColumnName("Month")
                        .HasColumnType("int");

                    b.Property<int>("Popularity")
                        .HasColumnType("int");

                    b.Property<int>("Quarter")
                        .HasColumnName("Quarter")
                        .HasColumnType("int");

                    b.Property<int>("Rating")
                        .HasColumnType("int");

                    b.Property<DateTime>("Updated")
                        .HasColumnType("datetime2");

                    b.Property<int>("Version")
                        .HasColumnType("int");

                    b.Property<int>("Week")
                        .HasColumnName("Week")
                        .HasColumnType("int");

                    b.Property<int>("Year")
                        .HasColumnName("Year")
                        .HasColumnType("int");

                    b.HasKey("StatisticsCalendarId");

                    b.ToTable("StatisticsCalendars");
                });

            modelBuilder.Entity("UserAccount", b =>
                {
                    b.Property<string>("Username")
                        .HasColumnName("Username")
                        .HasColumnType("nvarchar(450)");

                    b.Property<DateTime>("Created")
                        .HasColumnType("datetime2");

                    b.Property<string>("Hashcode")
                        .HasColumnName("Hashcode")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("InSystemNow")
                        .IsRequired()
                        .HasColumnName("InSystemNow")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("LastActive")
                        .HasColumnName("LastActive")
                        .HasColumnType("datetime2");

                    b.Property<int>("LoginCount")
                        .HasColumnName("LoginCount")
                        .HasColumnType("int");

                    b.Property<int>("PUserGroupId")
                        .HasColumnType("int");

                    b.Property<string>("PUserGroupUserGroupId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<int>("PUserPersonId")
                        .HasColumnType("int");

                    b.Property<string>("PUserPersonUserPersonId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<int>("PUserPhotoId")
                        .HasColumnType("int");

                    b.Property<string>("PUserPhotoUserPhotoId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<int>("PUserRoleId")
                        .HasColumnType("int");

                    b.Property<string>("PUserRoleUserRoleId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<int>("PUserSettingId")
                        .HasColumnType("int");

                    b.Property<string>("PUserSettingUserSettingId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("Password")
                        .HasColumnName("Password")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("Popularity")
                        .HasColumnType("int");

                    b.Property<int>("Rating")
                        .HasColumnType("int");

                    b.Property<int>("SettingsUsersettingid")
                        .HasColumnName("settings_UserSettingId")
                        .HasColumnType("int");

                    b.Property<DateTime>("Updated")
                        .HasColumnType("datetime2");

                    b.Property<int>("UserConfigId")
                        .HasColumnName("UserConfigId")
                        .HasColumnType("int");

                    b.Property<int>("UserGroupId")
                        .HasColumnName("UserGroupId")
                        .HasColumnType("int");

                    b.Property<int>("UserPersonId")
                        .HasColumnName("UserPersonId")
                        .HasColumnType("int");

                    b.Property<int>("UserPhotoId")
                        .HasColumnName("UserPhotoId")
                        .HasColumnType("int");

                    b.Property<int>("UserRoleId")
                        .HasColumnName("UserRoleId")
                        .HasColumnType("int");

                    b.Property<int>("Version")
                        .HasColumnType("int");

                    b.HasKey("Username");

                    b.HasIndex("PUserGroupUserGroupId");

                    b.HasIndex("PUserPersonUserPersonId");

                    b.HasIndex("PUserPhotoUserPhotoId");

                    b.HasIndex("PUserRoleUserRoleId");

                    b.HasIndex("PUserSettingUserSettingId");

                    b.ToTable("UserAccounts");
                });

            modelBuilder.Entity("UserGroup", b =>
                {
                    b.Property<string>("UserGroupId")
                        .HasColumnName("UserGroupId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<DateTime>("Created")
                        .HasColumnType("datetime2");

                    b.Property<int>("Popularity")
                        .HasColumnType("int");

                    b.Property<int>("Rating")
                        .HasColumnType("int");

                    b.Property<DateTime>("Updated")
                        .HasColumnType("datetime2");

                    b.Property<string>("UserGroupDescription")
                        .HasColumnName("UserGroupDescription")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("UserGroupName")
                        .HasColumnName("UserGroupName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("Version")
                        .HasColumnType("int");

                    b.HasKey("UserGroupId");

                    b.ToTable("UserGroups");
                });

            modelBuilder.Entity("UserMessage", b =>
                {
                    b.Property<string>("UserMessageId")
                        .HasColumnName("UserMessageId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<DateTime>("Created")
                        .HasColumnType("datetime2");

                    b.Property<string>("MessageSubject")
                        .HasColumnName("MessageSubject")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("MessageText")
                        .HasColumnName("MessageText")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("PUserAccountId")
                        .HasColumnType("int");

                    b.Property<string>("PUserAccountUsername")
                        .HasColumnType("nvarchar(450)");

                    b.Property<int>("Popularity")
                        .HasColumnType("int");

                    b.Property<int>("Rating")
                        .HasColumnType("int");

                    b.Property<string>("Readed")
                        .IsRequired()
                        .HasColumnName("Readed")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Sender")
                        .HasColumnName("Sender")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("Updated")
                        .HasColumnType("datetime2");

                    b.Property<string>("UserAccountId")
                        .HasColumnName("UserAccountId")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("Version")
                        .HasColumnType("int");

                    b.HasKey("UserMessageId");

                    b.HasIndex("PUserAccountUsername");

                    b.ToTable("UserMessages");
                });

            modelBuilder.Entity("UserPerson", b =>
                {
                    b.Property<string>("UserPersonId")
                        .HasColumnName("UserPersonId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<DateTime>("Birthday")
                        .HasColumnName("Birthday")
                        .HasColumnType("datetime2");

                    b.Property<DateTime>("Created")
                        .HasColumnType("datetime2");

                    b.Property<string>("Email")
                        .HasColumnName("Email")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Firstname")
                        .HasColumnName("Firstname")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Lastname")
                        .HasColumnName("Lastname")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("Popularity")
                        .HasColumnType("int");

                    b.Property<int>("Rating")
                        .HasColumnType("int");

                    b.Property<string>("Secondname")
                        .HasColumnName("Secondname")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Sex")
                        .HasColumnName("Sex")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Tel")
                        .HasColumnName("Tel")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("Updated")
                        .HasColumnType("datetime2");

                    b.Property<int>("Version")
                        .HasColumnType("int");

                    b.HasKey("UserPersonId");

                    b.ToTable("UserPersons");
                });

            modelBuilder.Entity("UserPhoto", b =>
                {
                    b.Property<string>("UserPhotoId")
                        .HasColumnName("UserPhotoId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<DateTime>("Created")
                        .HasColumnType("datetime2");

                    b.Property<int>("Popularity")
                        .HasColumnType("int");

                    b.Property<int>("Rating")
                        .HasColumnType("int");

                    b.Property<DateTime>("Updated")
                        .HasColumnType("datetime2");

                    b.Property<string>("UserPhotoData")
                        .HasColumnName("UserPhotoData")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("UserPhotoFormat")
                        .HasColumnName("UserPhotoFormat")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("Version")
                        .HasColumnType("int");

                    b.HasKey("UserPhotoId");

                    b.ToTable("UserPhotos");
                });

            modelBuilder.Entity("UserRole", b =>
                {
                    b.Property<string>("UserRoleId")
                        .HasColumnName("UserRoleId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<DateTime>("Created")
                        .HasColumnType("datetime2");

                    b.Property<int>("Popularity")
                        .HasColumnType("int");

                    b.Property<int>("Rating")
                        .HasColumnType("int");

                    b.Property<DateTime>("Updated")
                        .HasColumnType("datetime2");

                    b.Property<string>("UserRoleDescription")
                        .HasColumnName("UserRoleDescription")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("UserRoleName")
                        .HasColumnName("UserRoleName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("Version")
                        .HasColumnType("int");

                    b.HasKey("UserRoleId");

                    b.ToTable("UserRoles");
                });

            modelBuilder.Entity("UserSetting", b =>
                {
                    b.Property<string>("UserSettingId")
                        .HasColumnName("UserSettingId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<DateTime>("Created")
                        .HasColumnType("datetime2");

                    b.Property<string>("DarkColors")
                        .IsRequired()
                        .HasColumnName("DarkColors")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("HelpMode")
                        .IsRequired()
                        .HasColumnName("HelpMode")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("MobileMode")
                        .IsRequired()
                        .HasColumnName("MobileMode")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("Popularity")
                        .HasColumnType("int");

                    b.Property<int>("Rating")
                        .HasColumnType("int");

                    b.Property<DateTime>("Updated")
                        .HasColumnType("datetime2");

                    b.Property<int>("Version")
                        .HasColumnType("int");

                    b.HasKey("UserSettingId");

                    b.ToTable("UserSettings");
                });

            modelBuilder.Entity("UserTodo", b =>
                {
                    b.Property<string>("UserTodoId")
                        .HasColumnName("UserTodoId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<DateTime>("Created")
                        .HasColumnType("datetime2");

                    b.Property<DateTime>("DateBegin")
                        .HasColumnName("DateBegin")
                        .HasColumnType("datetime2");

                    b.Property<DateTime>("DateEnd")
                        .HasColumnName("DateEnd")
                        .HasColumnType("datetime2");

                    b.Property<int>("Popularity")
                        .HasColumnType("int");

                    b.Property<int>("Rating")
                        .HasColumnType("int");

                    b.Property<string>("TodoDescription")
                        .HasColumnName("TodoDescription")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("TodoName")
                        .HasColumnName("TodoName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("Updated")
                        .HasColumnType("datetime2");

                    b.Property<int>("Version")
                        .HasColumnType("int");

                    b.HasKey("UserTodoId");

                    b.ToTable("UserTodos");
                });

            modelBuilder.Entity("WebApp", b =>
                {
                    b.Property<string>("WebAppId")
                        .HasColumnName("WebAppId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<DateTime>("Created")
                        .HasColumnType("datetime2");

                    b.Property<string>("Description")
                        .HasColumnName("Description")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .HasColumnName("Name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("Popularity")
                        .HasColumnType("int");

                    b.Property<int>("Rating")
                        .HasColumnType("int");

                    b.Property<DateTime>("Updated")
                        .HasColumnType("datetime2");

                    b.Property<int>("Version")
                        .HasColumnType("int");

                    b.HasKey("WebAppId");

                    b.ToTable("WebApps");
                });

            modelBuilder.Entity("WebPage", b =>
                {
                    b.Property<string>("WebPageId")
                        .HasColumnName("WebPageId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<int>("AppID")
                        .HasColumnName("AppID")
                        .HasColumnType("int");

                    b.Property<int>("AppWebappid")
                        .HasColumnName("App_WebAppId")
                        .HasColumnType("int");

                    b.Property<string>("Content")
                        .HasColumnName("Content")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("Created")
                        .HasColumnType("datetime2");

                    b.Property<string>("Description")
                        .HasColumnName("Description")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .HasColumnName("Name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("PWebAppId")
                        .HasColumnType("int");

                    b.Property<string>("PWebAppWebAppId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<int>("Popularity")
                        .HasColumnType("int");

                    b.Property<int>("Rating")
                        .HasColumnType("int");

                    b.Property<DateTime>("Updated")
                        .HasColumnType("datetime2");

                    b.Property<int>("Version")
                        .HasColumnType("int");

                    b.HasKey("WebPageId");

                    b.HasIndex("PWebAppWebAppId");

                    b.ToTable("WebPages");
                });

            modelBuilder.Entity("PublicBlog", b =>
                {
                    b.HasOne("PublicAuthor", "PPublicAuthor")
                        .WithMany()
                        .HasForeignKey("PPublicAuthorPublicAuthorId");
                });

            modelBuilder.Entity("PublicCatalog", b =>
                {
                    b.HasOne("PublicDatabase", "PPublicDatabase")
                        .WithMany()
                        .HasForeignKey("PPublicDatabasePublicDatabaseId");

                    b.HasOne("PublicLib", "PPublicLib")
                        .WithMany()
                        .HasForeignKey("PPublicLibPublicLibId");

                    b.HasOne("PublicServer", "PPublicServer")
                        .WithMany()
                        .HasForeignKey("PPublicServerPublicServerId");
                });

            modelBuilder.Entity("PublicDatabase", b =>
                {
                    b.HasOne("PublicDatabase", "PPublicDatabase")
                        .WithMany()
                        .HasForeignKey("PPublicDatabasePublicDatabaseId");
                });

            modelBuilder.Entity("PublicJournal", b =>
                {
                    b.HasOne("PublicLiterature", "PPublicLiterature")
                        .WithMany()
                        .HasForeignKey("PPublicLiteraturePublicLiteratureId");
                });

            modelBuilder.Entity("PublicPost", b =>
                {
                    b.HasOne("PublicBlog", "PPublicBlog")
                        .WithMany()
                        .HasForeignKey("PPublicBlogPublicBlogId");
                });

            modelBuilder.Entity("PublicReader", b =>
                {
                    b.HasOne("UserAccount", "PUserAccount")
                        .WithMany()
                        .HasForeignKey("PUserAccountUsername")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("UserAccount", b =>
                {
                    b.HasOne("UserGroup", "PUserGroup")
                        .WithMany()
                        .HasForeignKey("PUserGroupUserGroupId");

                    b.HasOne("UserPerson", "PUserPerson")
                        .WithMany()
                        .HasForeignKey("PUserPersonUserPersonId");

                    b.HasOne("UserPhoto", "PUserPhoto")
                        .WithMany()
                        .HasForeignKey("PUserPhotoUserPhotoId");

                    b.HasOne("UserRole", "PUserRole")
                        .WithMany()
                        .HasForeignKey("PUserRoleUserRoleId");

                    b.HasOne("UserSetting", "PUserSetting")
                        .WithMany()
                        .HasForeignKey("PUserSettingUserSettingId");
                });

            modelBuilder.Entity("UserMessage", b =>
                {
                    b.HasOne("UserAccount", "PUserAccount")
                        .WithMany()
                        .HasForeignKey("PUserAccountUsername");
                });

            modelBuilder.Entity("WebPage", b =>
                {
                    b.HasOne("WebApp", "PWebApp")
                        .WithMany()
                        .HasForeignKey("PWebAppWebAppId");
                });
#pragma warning restore 612, 618
        }
    }
}

﻿using Microsoft.Extensions.Logging;

using SpbPublicLibsCore.Domain.User.Profile.Movies;
using SpbPublicLibsCore.Testing;
using SpbPublicLibsCore.Utils;

using System;

namespace SpbPublicLibsUnit.DomainUnit
{
    public class TestUserMovies : TestingUnit
    {
        private IUserMovies _userMovies;
        private ILogger<TestUserMovies> _logger;

        public TestUserMovies(
            ILogger<TestUserMovies>  logger, 
            IUserMovies userMovies)
        {
            _logger = logger;
            _userMovies = userMovies;
            _logger.LogInformation("Create");
        }


        protected override void onTest()
        {
            _logger.LogInformation("onTest");
            canUserGetMovies();
            canUserReturnMovie();
        }


        private void canUserReturnMovie()
        {
            _logger.LogInformation($"canUserReturnMovie?");
            foreach( var movie in _userMovies.GetMovies())
            {
                _logger.LogInformation($"user try return movie: {movie.Title}");
                _userMovies.ReturnMovie(movie);
            }
            messages.Add(Naming.ToSpaceStyle("canUserReturnMovie"));
        }


        private void canUserGetMovies()
        {
            _logger.LogInformation($"canUserGetMovies?");
            foreach (var movie in _userMovies.GetMovies())
            {
                _logger.LogInformation($"user can see movie: {movie.Title}");
            }
            messages.Add(Naming.ToSpaceStyle("canUserGetMovies"));
        }
    }
}

﻿using ApplicationCore.Domain.Odbc.Metadata;
using ApplicationCore.Utils;

namespace ApplicationCore.Converter.Generators
{
    public class EntityControllerGenerator
    {
        /// <summary>
        /// Метод создания класса c#, реализующего CRUD операции с сущностью
        /// </summary>
        /// <param name="table"> модель данных сущности </param>
        /// <returns> код c#, реализующий CRUD операции над сущностью </returns>
        public string CreateEntityController( TableMetaData table )
        {
            string wscode = "import { Component,Inject,Injectable } from '@angular/core';\n" +
                            "import { HttpClient, HttpParams } from '@angular/common/http';\n" +
                            "import { " + table.getTableNameCapitalized() + " } from './" + table.getTableNameKebabed() + ".model';\n";
            wscode += "\n\n@Injectable({providedIn: 'root'})\nexport class " + table.getTableNameCapitalized() + "Service \n{\n" +
                "\t constructor(  private http: HttpClient ){} \n" +

                "\t toHttpParams(obj: any){\n" +
                "\t    const result = new HttpParams();\n" +
                "\t    Object.getOwnPropertyNames(obj).forEach(name => {\n" +
                "\t        result.set(name, JSON.stringify(obj[name]));\n" +
                "\t    });\n" +
                "\t    return result;\n" +
                "\t }\n" +
            "\t list( resolve: ( data: " + table.getTableNameCapitalized() + "[])=>any, reject? :(err)=>any ){ \n\t\tthis.http.get<" + table.getTableNameCapitalized() + "[]>('api/" + table.getTableNameCapitalized() + "').subscribe(resolve,reject);\n\t };\n" +
                "\t find( id: number, resolve: (data: " + table.getTableNameCapitalized() + ")=>any, reject? :(err)=>any ){ \n\t\tthis.http.get('api/" + table.getTableNameCapitalized() + "',{params:this.toHttpParams({ id: id })}).subscribe(resolve,reject);\n\t };\n" +
                "\t update( id: number, obj: " + table.getTableNameCapitalized() + ",resolve,reject ){ \n\t\tthis.http.put('api/" + table.getTableNameCapitalized() + "',{params:this.toHttpParams({id: id, " + table.getTableNameCapitalized() + ": obj })}).subscribe(resolve,reject);\n\t };\n" +
                "\t create( obj: " + table.getTableNameCapitalized() + ", resolve? :( status: number )=>any, reject? :(err)=>any ){ \n\t\tthis.http.post('api/" + table.getTableNameCapitalized() + "',{params:this.toHttpParams({ " + table.getTableNameCapitalized() + " : obj })}).subscribe(resolve,reject); };\n" +
                "\t remove( id: number, resolve? :(status: number)=>any, reject? :(err)=>any ){ \n\t\tthis.http.delete('api/" + table.getTableNameCapitalized() + "',{params: this.toHttpParams({id: id})}).subscribe(resolve,reject); \n\t};\n" +
            "}";
         
            return wscode;
        }        
    }
}

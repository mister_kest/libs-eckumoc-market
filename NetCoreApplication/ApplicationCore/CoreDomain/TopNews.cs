﻿using System.Linq;
using System.Collections.Generic;
using ApplicationDb;
using ApplicationDb.Entities;

namespace SpbPublicLibsWebAPI.Controllers
{
    public class TopNews
    {
        private readonly ApplicationDbContext _db;

        public TopNews( )
        {
       
        }


        public void PublishNews(News news)
        {
            _db.News.Add(news);
            _db.SaveChanges();
        }

        public List<News> GetTopNews()
        {
            return _db.News.ToList();
        }
    }
}
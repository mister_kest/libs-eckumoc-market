﻿using Microsoft.AspNetCore.Mvc;
using ApplicationCore.APIS.OpenWeatherAPI;
using System.Threading.Tasks;

namespace SpbPublicLibsWeb.Controllers.PagesPublic
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class WeatherController : ControllerBase
    {
        private OnecallForecastAPI _client = new OnecallForecastAPI();
        private WeatherForecastAPI _weather = new WeatherForecastAPI();


        public async Task<string> GetOneCall(double lat, double lon, long time)
        {
            return await this._client.GetOneCall(lat, lon, time);
        }

        public async Task<string> GetWeatherForecastByCity(string cityname)
        {
            return await this._weather.GetWeatherForecastByCity(cityname);
        }
    }
}

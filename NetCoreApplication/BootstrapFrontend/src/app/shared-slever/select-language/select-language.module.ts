import { SharedModule } from 'src/app/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelectLanguageComponent } from './select-language.component';



@NgModule({
  declarations: [SelectLanguageComponent],
  bootstrap: [SelectLanguageComponent],
  exports: [SelectLanguageComponent],
  imports: [
    CommonModule,
    SharedModule
  ]
})
export class SelectLanguageModule { }

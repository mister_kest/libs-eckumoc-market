import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AppLanguageService } from 'src/app/app-language.service';
import { SearchResult } from '../search-result';

@Component({
  selector:     'public-news',
  templateUrl:  './public-news.component.html'
})
export class PublicNewsComponent implements OnInit {

  model: SearchResult = new SearchResult();

  news = [{
    header: 'Новинка',
    title: 'Обновление каталога',
    time: '1 мин. назад',
    text: 'фывф ывфывфы вфывфывфыввф ывфывыф'
  }];

  constructor(
    private http: HttpClient,

    private lang: AppLanguageService  ) { }

  ngOnInit() {
    this.http.get('/api/Search/getMoviesTopRated',{
      params: {
      }
    }).subscribe(this.setter('model'));
  }

  setter( key ) {
    const ctrl = this;
    return function( value ){
      console.log( value );
      ctrl[key] = value;
    }
  }

  onMovieClick( movie ){
    /*this.checkout.products.push(
      { name: movie.title, price: 0}
    );*/
  }
}

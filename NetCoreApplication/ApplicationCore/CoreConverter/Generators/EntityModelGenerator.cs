﻿using ApplicationCore.Domain.Odbc.Metadata;
using ApplicationCore.Utils;

namespace ApplicationCore.Converter.Generators
{
    public class EntityModelGenerator
    {

        /// <summary>
        /// Метод создания класса TypeScript, реализующего модель данных сущности
        /// </summary>
        /// <param name="table"> модель данных сущности </param>
        /// <returns> код TypeScript, реализующий модель данных сущности </returns>
        public string CreateTypeScriptModel( TableMetaData table )
        {
            string header = $"export class {Naming.ToCapitalStyle(table.singlecount_name)}\n";
            string body = "";
            foreach (string columnName in table.columns.Keys)
            {
                ColumnMetaData column = table.columns[columnName];
                body += "\t" + Naming.ToCamelStyle(column.name) + ": " + getTypeScriptDataType(column.type) + "; \n";
            }
            string classCode = header + "{\n" + body + "}";
            return classCode;
        }


        /// <summary>
        /// Метод создания класса TypeScript, реализующего модель данных сущности
        /// </summary>
        /// <param name="table"> модель данных сущности </param>
        /// <returns> код TypeScript, реализующий модель данных сущности </returns>.
        
        public string CreateSharpModel(TableMetaData table)
        {
            string header = 
                $"[System.ComponentModel.DataAnnotations.Schema.Table(\"{table.name}\")]\n" +
                $"public class {Naming.ToCapitalStyle(table.singlecount_name)}\n";
            string body = "";
            foreach (string columnName in table.columns.Keys)
            {
                ColumnMetaData column = table.columns[columnName];
                if(column.primary == true || table.getPrimaryKey().ToLower()==column.name.ToLower())
                {
                    body += $"\t [System.ComponentModel.DataAnnotations.Key()]\n";
                }      
                foreach (var annotation in table.GetAnnotations(columnName))
                {
                    body += $"\t {annotation}\n";
                    
                }
                body += $"\t public {getSharpDataType(column.type)} {Naming.ToCapitalStyle(column.name)}"+ "{ get; set; }\n\n";


                body += "\n\n";
            }
            //body += $"\t public System.DateTime Created " + "{ get; set; }\n\n";
            //body += $"\t public System.DateTime Updated " + "{ get; set; }\n\n";
            //body += $"\t public int Version " + "{ get; set; }\n\n";
            //body += $"\t public int Popularity " + "{ get; set; }\n\n";
            //body += $"\t public int Rating " + "{ get; set; }\n\n";

            foreach ( var fk in table.fk)
            {
                body += $"\t [System.ComponentModel.DataAnnotations.Schema.ForeignKey(\"P{ Naming.ToCapitalStyle(fk.Value)}Id\")]\n";
                body += $"\t public int P{ Naming.ToCapitalStyle(fk.Value)}Id " + "{ get; set; }\n";
                body += $"\t public virtual {Naming.ToCapitalStyle(fk.Value)} P{Naming.ToCapitalStyle(fk.Value)}" + "{ get; set; }\n";
            }
            string classCode = header + "{\n" + body + "}";
            return classCode;
        }


        /// <summary>
        /// Метод сопоставления типов данных SQL с типами C#
        /// </summary>
        /// <param name="sqlDataType"> тип данных SQL </param>
        /// <returns> тип C# </returns>
        private object getSharpDataType( string sqlDataType )
        {
            switch (sqlDataType.ToLower())
            {
                case "date":
                case "datetime":
                    return "System.DateTime";
                case "int":
                case "int32":
                case "int64":
                case "long":
                case "double":
                case "float":
                case "decimal":
                case "integer":
                case "number":
                case "numeric":
                    return "int";
                default: 
                    return "string";
            }
        }


        /// <summary>
        /// Метод сопоставления типов данных SQL с типами TypeScript
        /// </summary>
        /// <param name="sqlDataType"> тип данных SQL </param>
        /// <returns> тип TypeScript </returns>
        public string getTypeScriptDataType( string sqlDataType )
        {
            switch (sqlDataType.ToLower())
            {
                case "date":
                case "datetime":
                    return "Date";
                case "int":
                case "int32":
                case "int64":
                case "long":
                case "double":
                case "float":
                case "decimal":
                case "integer":
                case "number":
                case "numeric":
                    return "number";
                default: return "string";
            }
        }
    }
}

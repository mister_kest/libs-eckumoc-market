﻿using Microsoft.AspNetCore.Hosting.Server;
using Microsoft.AspNetCore.Http.Features;

using SpbPublicLibsCore.Testing;

using SpbPublicLibsUnit.DomainUnit;

using System.Threading;
using System.Threading.Tasks;

namespace SpbPublicLibsUnit
{
    public class TestDomain : TestingUnit, IServer
    {

        public IFeatureCollection Features => throw new System.NotImplementedException();

        public TestDomain(): base()
        {
             

            this.Push(new TestReflection());
            this.Push(new TestOdbc());
            this.Push(new TestAuthentication());
            this.Push(new TestEncoding());
            
            this.Push(new TestRegistration());
            this.Push(new TestSession());
            
        }

        protected override void onTest()
        {
            
        }

        public Task StartAsync<TContext>(
                IHttpApplication<TContext> application, 
                CancellationToken cancellationToken)
        {
            return new Task(()=> { });
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return new Task(() => { });
        }

        

        public void Dispose()
        {
            throw new System.NotImplementedException();
        }
    }
}

using System.Linq;
using ApplicationDb;
 using LibsDb; 
using LibsDb.LibsEntities;
using ApplicationDb.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;


[ApiController]
[Route("api/LoginDaily/[action]")]
public class LoginDailyController: ControllerBase
{
	private readonly LibsDbContext _db; 


	public LoginDailyController( LibsDbContext db ){
		_db=db;
	}

	public void Create( LoginDaily record )	{
		_db.LoginDaily.Add(record);
		_db.SaveChanges();
	}

	public void Remove( int id ){
		_db.LoginDaily.Remove(this.Find(id));
		_db.SaveChanges();
	}

	public LoginDaily Find( int id )	{
		 return _db.LoginDaily.Find(id);
	}

	public System.Collections.Generic.List<LoginDaily> List(   )	{
		 return _db.LoginDaily.ToList();
	}

}
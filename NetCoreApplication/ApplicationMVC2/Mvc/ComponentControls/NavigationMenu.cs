﻿
using Mvc;
using Mvc.ComponentControls;

using SpbPublicLibsЬMVC.Mvc.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpbPublicLibsЬMVC.Mvc.ComponentControls
{
    public class NavigationMenu: MvcComponent 
    {    
        public ITreeNode<NavigationMenu> _parent;
        public Dictionary<string, ITreeNode<NavigationMenu>> _children =
            new Dictionary<string, ITreeNode<NavigationMenu>>();
        public readonly string _label;
        public MvcComponent _item;

        public NavigationMenu(string text)
        {
            _label = text;
            _item = new ControlButton(text,null);
        }
   
    }
}

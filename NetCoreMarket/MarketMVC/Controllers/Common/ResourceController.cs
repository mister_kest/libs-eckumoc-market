﻿using ApplicationDb.Entities;
using miac.ServerApp;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MvcMarketPlace.Data;
using MvcMarketPlace.Data.Entities;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpbPublicLibsWeb.Controllers 
{

    /// <summary>
    /// Необходимо уметь получать бинарные данные из ХД
    /// Сохранять в ХД
    /// Читать бинарные данные( отображать изображения )
    /// </summary>

    // GET: /api/CommmonResourceApi/List

    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ResourceController: ControllerBase
    {
        private readonly MarketDbContext _db;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="db"></param>
        public ResourceController(MarketDbContext db) : base()
        {           
            _db = db;       
        }


        public object Ping()
        {
            return new object();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<Dictionary<string, object>> List()
        {
            Dictionary<string, object> resp = new Dictionary<string, object>();
            resp["data"] = (from r in _db.Resources select r.ID).ToList();
            resp["status"] = "success";
            return resp;
        }


        /// <summary>
        /// https://localhost:44347/api/Resource/List
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("/api/Resource/Use/{id:int}")]
        public async Task Use( [FromRoute] int id)
        {
            Dictionary<string, object> resp = new Dictionary<string, object>();
            
            Resource resource = _db.Resources.Find(id);         
            Response.ContentType = resource.Mime;
            byte[] data = resource.Data;
            await Response.Body.WriteAsync(data, 0, data.Length);

            resp["status"] = "success";
            
        }


        //string PhotoUrl = $"https://localhost:44347/Market/Products/PhotoImage/" + productId;
        [HttpPost()]
        public async Task<Dictionary<string, object>> Create(  )
        {
            Dictionary<string, object> response = new Dictionary<string, object>();
            string resourceName = Request.Headers["Resource-Name"];
            string mimeType = Request.Headers["Mime-Type"];
            Resource resource = new Resource()
            {
                Name = resourceName,
                Mime = mimeType,
                Created = DateTime.Now
            };           
            long? length = this.HttpContext.Request.ContentLength;
            if (length != null)
            {
                resource.Data = new byte[(long)length];
                await this.HttpContext.Request.Body.ReadAsync(resource.Data, 0, (int)length);                
            }
            _db.Add(resource);
            await _db.SaveChangesAsync();
            
            response["statuc"] = "success";
            response["id"] = resource.ID;
            response["url"] = $"/api/{GetType().Name.Replace("Controller", "")}/Use/{resource.ID}";                
            return response;
        }
    }
}

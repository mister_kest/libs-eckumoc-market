﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using LibsDb;
using LibsDb.LibsEntities;

namespace LibsMVC.Controllers
{
    public class LitersController : Controller
    {
        private readonly LibsDbContext _context;

        public LitersController(LibsDbContext context)
        {
            _context = context;
        }

        // GET: Liters
        public async Task<IActionResult> Index()
        {
            var libsDbContext = _context.Liters.Include(l => l.Holder);
            return View(await libsDbContext.ToListAsync());
        }

        // GET: Liters/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var liter = await _context.Liters
                .Include(l => l.Holder)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (liter == null)
            {
                return NotFound();
            }

            return View(liter);
        }

        // GET: Liters/Create
        public IActionResult Create()
        {
            ViewData["HolderID"] = new SelectList(_context.Holders, "ID", "ID");
            return View();
        }

        // POST: Liters/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Title,Status,HolderID")] Liter liter)
        {
            if (ModelState.IsValid)
            {
                _context.Add(liter);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["HolderID"] = new SelectList(_context.Holders, "ID", "ID", liter.HolderID);
            return View(liter);
        }

        // GET: Liters/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var liter = await _context.Liters.FindAsync(id);
            if (liter == null)
            {
                return NotFound();
            }
            ViewData["HolderID"] = new SelectList(_context.Holders, "ID", "ID", liter.HolderID);
            return View(liter);
        }

        // POST: Liters/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Title,Status,HolderID")] Liter liter)
        {
            if (id != liter.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(liter);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!LiterExists(liter.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["HolderID"] = new SelectList(_context.Holders, "ID", "ID", liter.HolderID);
            return View(liter);
        }

        // GET: Liters/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var liter = await _context.Liters
                .Include(l => l.Holder)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (liter == null)
            {
                return NotFound();
            }

            return View(liter);
        }

        // POST: Liters/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var liter = await _context.Liters.FindAsync(id);
            _context.Liters.Remove(liter);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool LiterExists(int id)
        {
            return _context.Liters.Any(e => e.ID == id);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ApplicationDb.Entities
{
    public class App
    {
        public App()
        {
            Pages = new List<Page>();
        }


        [Key]
        public int ID { get; set; }


        [DisplayName("Наименование")]
        public string Name { get; set; }


        [DisplayName("Описание")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }


        [DisplayName("Обновление")]
        public DateTime Updated { get; set; }

        [DisplayName("Версия")]
        public string Version { get; set; }


        [DisplayName("Страницы")]
        public virtual List<Page> Pages { get; set; }
    }
}

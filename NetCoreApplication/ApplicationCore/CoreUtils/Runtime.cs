﻿using System;
using System.Timers;

namespace ApplicationCore.Utils
{
    /// <summary>
    /// Реализует методы управления синхронными операциями
    /// </summary>
    public class Runtime
    {

        /// <summary>
        /// Выполнение делегата по истечению заданного в милисекундах промежутка времени
        /// </summary>
        /// <param name="action"> делегат </param>
        /// <param name="ms"> кол-во миллисекунд </param>
        public static void SetTimeout(Action action, long ms)
        {
            System.Timers.Timer aTimer = new System.Timers.Timer(2000);
            aTimer.Elapsed += (Object source, ElapsedEventArgs e) => {
                action();
                aTimer.Enabled = false;
            };
            aTimer.AutoReset = false;
            aTimer.Enabled = true;
        }


        /// <summary>
        /// Выполнение делегата с заданным интервалом в милисекундах
        /// </summary>
        /// <param name="action"> делегат </param>
        /// <param name="ms"> кол-во миллисекунд </param>
        public static void SetInterval(Action action, long ms)
        {
            System.Timers.Timer aTimer = new System.Timers.Timer(ms);

            aTimer.Elapsed += (Object source, ElapsedEventArgs e) => {
                action();
              
            };
            aTimer.AutoReset = true;
            aTimer.Enabled = true;            
        }
    }
}

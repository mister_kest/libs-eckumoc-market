﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using miac.ServerApp;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MvcMarketPlace.Data;
using MvcMarketPlace.Data.Entities;

namespace EcKuMoC.Areas.Market.Controllers
{
    [Area("Market")]
    public class ProductsController : Controller
    {
        private readonly MarketDbContext _context;


        public ProductsController(MarketDbContext context)
        {
            _context = context;
        }
 

        
        public async Task<IActionResult> Index()
        {
            var MarketDbContext = _context.Products.Include(p => p.Category);
            return View(await MarketDbContext.ToListAsync());
        }

        
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var product = await _context.Products
                .Include(p => p.Category)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (product == null)
            {
                return NotFound();
            }
            return View(product);
        }


        
        public IActionResult Create()
        {
            ViewData["CategoryID"] = new SelectList(_context.ProductCategories, "ID", "Name");
            return View();
        }

        // POST: Market/Products/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,CategoryID,Name,Description,Photo")] Product product)
        {
            if (ModelState.IsValid)
            {

                long? length = this.HttpContext.Request.ContentLength;
                if (length != null)
                {
                    byte[] data = new byte[(long)length];
                    this.HttpContext.Request.Body.ReadAsync(data, 0, (int)length);
                    string mime = Request.ContentType;
                }


                _context.Add(product);
                await _context.SaveChangesAsync();
                return Redirect($"/api/ProductPhotoApi/Upload?productId={product.ID}");
            }
            ViewData["CategoryID"] = new SelectList(_context.ProductCategories, "ID", "ID", product.CategoryID);
            return View(product);
        }

        // GET: Market/Products/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await _context.Products.FindAsync(id);
            if (product == null)
            {
                return NotFound();
            }
            ViewData["CategoryID"] = new SelectList(_context.ProductCategories, "ID", "ID", product.CategoryID);
            return View(product);
        }

        // POST: Market/Products/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,CategoryID,Name,Description,Photo")] Product product)
        {
            if (id != product.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(product);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProductExists(product.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CategoryID"] = new SelectList(_context.ProductCategories, "ID", "ID", product.CategoryID);
            return View(product);
        }

        // GET: Market/Products/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await _context.Products
                .Include(p => p.Category)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (product == null)
            {
                return NotFound();
            }

            return View(product);
        }

        // POST: Market/Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var product = await _context.Products.FindAsync(id);
            _context.Products.Remove(product);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ProductExists(int id)
        {
            return _context.Products.Any(e => e.ID == id);
        }
    }
}

﻿
using ApplicationCore.APIS.TheMovieDatabaseAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EcKuMoC.Mvc.TemplateApplication
{
    public interface ISearchApplication
    {
        IDictionary<string, int> GetKeywords(string query);
        SearchResults Search(string query, int page);
    }
}

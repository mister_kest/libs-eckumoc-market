import { HttpClient } from '@angular/common/http';
import { Injectable, EventEmitter } from '@angular/core';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';
import { AppLanguageService } from './app-language.service';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  onSearch(evt: any) {
    this.searchEvent.emit(evt);
  }

  searchEvent = new EventEmitter();

  header = 'Libs';
  progress = false;
  marker = 'dark';
  theme: 'light'|'dark' = 'light';
  status = 'waiting';

  markers = ['success','danger','warning','info','light','dark','secondary','primary'];

  constructor(
    private router: Router,
    private http: HttpClient,
    private lang: AppLanguageService  ) {
    window['app'] = this;
    const ctrl = this;
    router.events.subscribe(evt=>{
      if( evt instanceof NavigationStart){
        ctrl.progress = true;
      }
      if( evt instanceof NavigationEnd){
        ctrl.progress = false;
      }
    });
  }

  set title( value ){
    document.title = value;
  }



  public keywordSearch( query, page='1'){
    return this.http.get('/api/search/keywordSearch',{
      params: {
        query:  query,
        page:   page
      }
    });
  }


}

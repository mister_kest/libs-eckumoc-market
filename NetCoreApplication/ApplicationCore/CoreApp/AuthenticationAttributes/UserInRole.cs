﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApplicationModels;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using SpbPublicLibsWeb.MVC.DescriptionAttributes;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace MvcMarketPlace.Mvc.AuthenticationAttributes
{
  
    public class IfUserInRole : Attribute, IControllerModelConvention
    {
        private readonly List<string> _roles = new List<string>();


        public IfUserInRole(string role1, string? role2 = null, string? role3 = null, string? role4 = null)
        {
            _roles.Add(role1);
            if(role2 != null)
            {
                _roles.Add(role2);
                if (role3 != null)
                {
                    _roles.Add(role3);
                    if (role4 != null)
                    {
                        _roles.Add(role4);
                    }

                }

            }
        }



        public void Apply(ControllerModel controllerModel)
        {
            
        }
    }
}

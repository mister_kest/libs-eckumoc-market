﻿
using ApplicationDb.Entities;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace MvcMarketPlace.Data.Entities
{
    public class MarketplaceUser: User
    {
        public MarketplaceUser()
        {
            this.Markets = new List<Market>();
        }
        
        [DisplayName("Ник")]
        public string Nickname { get; set; }


        [DisplayName("Денежные средства")]
        public float? Cash { get; set; }


        public virtual IList<Market> Markets { get; set; }
    }
}

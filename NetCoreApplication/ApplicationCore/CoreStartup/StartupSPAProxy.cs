using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace Startup
{
    public class StartupSPAProxy: IStartupElement
    {
        string url = null;
        public StartupSPAProxy(string url = "http://localhost:4200")
        {
            this.url = url; 
        }

        public void ConfigureServices(IServiceCollection services)
        {                    
        }        
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            Console.WriteLine($"spa-url: {url}");
            app.UseSpa(spa =>
            {
                spa.UseProxyToSpaDevelopmentServer(this.url);
            });
        }
    }
}

using System.Linq;
using ApplicationDb;
 using LibsDb; 
using LibsDb.LibsEntities;
using ApplicationDb.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;


[ApiController]
[Route("api/Books/[action]")]
public class BooksController: ControllerBase
{
	private readonly LibsDbContext _db; 


	public BooksController( LibsDbContext db ){
		_db=db;
	}

	public void Create( Book record )	{
		_db.Books.Add(record);
		_db.SaveChanges();
	}

	public void Remove( int id ){
		_db.Books.Remove(this.Find(id));
		_db.SaveChanges();
	}

	public Book Find( int id )	{
		 return _db.Books.Find(id);
	}

	public System.Collections.Generic.List<Book> List(   )	{
		 return _db.Books.ToList();
	}

}
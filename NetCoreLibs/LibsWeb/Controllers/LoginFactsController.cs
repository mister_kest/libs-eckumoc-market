using System.Linq;
using ApplicationDb;
 using LibsDb; 
using LibsDb.LibsEntities;
using ApplicationDb.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;


[ApiController]
[Route("api/LoginFacts/[action]")]
public class LoginFactsController: ControllerBase
{
	private readonly LibsDbContext _db; 


	public LoginFactsController( LibsDbContext db ){
		_db=db;
	}

	public void Create( LoginFact record )	{
		_db.LoginFacts.Add(record);
		_db.SaveChanges();
	}

	public void Remove( int id ){
		_db.LoginFacts.Remove(this.Find(id));
		_db.SaveChanges();
	}

	public LoginFact Find( int id )	{
		 return _db.LoginFacts.Find(id);
	}

	public System.Collections.Generic.List<LoginFact> List(   )	{
		 return _db.LoginFacts.ToList();
	}

}
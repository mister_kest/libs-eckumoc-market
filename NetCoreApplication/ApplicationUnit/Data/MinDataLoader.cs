﻿using SpbPublicLibsDb.DAL;
using SpbPublicLibsDb.DAL.SpbPublicLibs;
using System;
using System.Linq;

namespace SpbPublicLibsDb.MinData
{
    public class MinDataLoader
    {
        public static void LoadData()
        {
            InsertPublicLibs();
            InsertDatabases();
            InsertServers();
            InsertUserRoles();
            InsertWebApp();
            InsertStatisticsCallendar();
        }
        public static void InsertPublicLibs() {
            int l =System.IO.Directory.GetCurrentDirectory().IndexOf("SpbPublicLibsWebAPI");
            string filename =
                            System.IO.Directory.GetCurrentDirectory().Substring(0,
                            l) + "SpbPublicLibsWebAPI"+"\\"+"libs.csv";
            using (SpbPublicLibsDbContext db = new SpbPublicLibsDbContext())
            {
                if (db.PublicLibs.Count() == 0)
                {
                    return;
                }
                foreach (string line in System.IO.File.ReadAllText(filename).Split("\n"))
                {
                    string[] words = line.Split(";");
                    if (words.Length < 9)
                    {
                        Console.WriteLine(line);
                    }
                    else
                    {
                        db.PublicLibs.Add(new PublicLib()
                        {
                            Name = words[3],
                            NameShort = words[1],
                            Sigla = words[0],
                            Url = words[8]
                        });
                        Console.WriteLine(words[1] + ":" + words[8]);
                    }

                }
                db.SaveChanges();
            }
        }


        public static void InsertStatisticsCallendar() {
            using (SpbPublicLibsDbContext db = new SpbPublicLibsDbContext())
            {
                DateTime p = DateTime.Now;
                for (int i = 0; i < 300; i++)
                {
                    db.StatisticsCalendars.Add(new StatisticsCalendar()
                    {
                        StatisticsCalendarId = db.StatisticsCalendars.Count()+1,
                        Day = p.Day,
                        Hour = p.Hour,
                        
                        Minute = p.Minute,
                        Quarter = 1,
                        Month = p.Month,
                        Week = 1,
                        Year = p.Year
                        
                    });
                    p = p.AddDays(1);
                }
                db.SaveChanges();
            }
        }
        public static void InsertUserRoles() {
            using (SpbPublicLibsDbContext _context = new SpbPublicLibsDbContext())
            {
                if (_context.UserRoles.Count() == 0)
                {
                    _context.UserRoles.Add(new UserRole()
                    {
                        UserRoleName = "Reader",
                        UserRoleDescription = "Reader can take anyone from libs."
                    });
                    _context.UserRoles.Add(new UserRole()
                    {
                        UserRoleName = "Admin",
                        UserRoleDescription = "Admin can edit libs database"
                    });
                    _context.UserRoles.Add(new UserRole()
                    {
                        UserRoleName = "Developer",
                        UserRoleDescription = "Can create optional resources."
                    });
                    _context.SaveChanges();
                }
            }            
        }


        public static void InsertWebApp() {
            using (SpbPublicLibsDbContext _context = new SpbPublicLibsDbContext())
            {
                if (_context.WebApps.Count() == 0)
                {
                    _context.WebApps.Add(new WebApp()
                    {
                         Name = "TestApp",
                         Description = "This is a test",
                         Updated = DateTime.Now,
                         Version = "1"

                    });
                    _context.SaveChanges();


                }
            }
        }


        public static void InsertDatabases() { }
        public static void InsertServers() { }
    }
}

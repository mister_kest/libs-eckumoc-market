﻿using Newtonsoft.Json.Linq;

namespace ApplicationCore.Utils
{
    public class Json
    {
        public static string Stringify( object target )
        {
            if( target is string)
            {
                return (string)target;
            }
            else if (target is JObject)
            {
                return ((JObject)target).ToString();
            }
            else
            {
                return JObject.FromObject(target).ToString();
            }
        }
    }
}

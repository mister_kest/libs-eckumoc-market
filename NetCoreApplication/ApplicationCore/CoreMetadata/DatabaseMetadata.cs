﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApplicationCore.Domain.Odbc.Metadata
{
    /// <summary>
    /// Класс определяет свойства базы данных
    /// </summary>
    public class DatabaseMetadata
    {
        public string driver;
        public string database;
        public string serverVersion;
        public string connectionString;

        public Dictionary<string, TableMetaData> Tables = new Dictionary<string, TableMetaData>();
        public Dictionary<string, object> Metadata = new Dictionary<string, object>();

        public DatabaseMetadata() { }

        public override string ToString()
        {
            return Newtonsoft.Json.Linq.JObject.FromObject(this).ToString();
        }

        public void Validate()
        {
            foreach( var ptable in this.Tables)
            {
                if (ptable.Value.getPrimaryKey() == null)
                {
                    throw new Exception($"Primary key undefined for table: {ptable.Key}");
                }              
            }
        }
    }
}

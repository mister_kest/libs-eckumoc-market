﻿using System;

namespace Mvc.ComponentControls
{ 
    public class ControlButton: Mvc.MvcComponent
    {
        public Action _onClick;
        public string _label;


        public ControlButton( string label, Action click ) :base(  )
        {
            _onClick = click;
            _label = label;
        }


        public void _click()
        {
            _label += DateTime.Now.Millisecond;


        }
    }
}

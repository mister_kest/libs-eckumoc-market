using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Startup
{
    public class StartupSPA: IStartupElement
    {
        string npmScript = null;
        string distDir = null;
        string sourceDir = null;

        public StartupSPA(string distDir = "ClientApp\\dist", string sourceDir= "ClientApp", string npmScript="start" )
        {
            this.sourceDir = sourceDir;
            this.distDir = distDir;
            this.npmScript = npmScript;
        }

        public void ConfigureServices(IServiceCollection services)
        {        
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = this.distDir;
            });
        }        
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = this.sourceDir;
                if (env.IsDevelopment())
                {
                    spa.UseAngularCliServer(npmScript: this.npmScript);
                }
            });
        }
    }
}

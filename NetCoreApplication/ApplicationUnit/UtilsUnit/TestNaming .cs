﻿using SpbPublicLibsCore.Testing;
using SpbPublicLibsCore.Utils;


namespace SpbPublicLibsUnit.UtilsUnit
{
    public class TestNaming : TestingUnit
    {
        protected override void onTest()
        {
            canConvertNameToDiffrentStyles();
            canIdentifyCapitalStyleName();
            canIdentifySnakeStyleName();
            canIdentifyKebabStyleName();
        }

        private void canConvertNameToDiffrentStyles()
        {
            string capitalStyle = "AppModule";
            string snakeStyle = "app_module";
            string dollarStyle = "$appModule";
            string kebabStyle = "app-module";
            string camelStyle = "appModule";

            string lastname = camelStyle;
            if( snakeStyle != (lastname = Naming.ToSnakeStyle(lastname)))
                throw new System.Exception();
            if ( dollarStyle != (lastname = Naming.ToDollarStyle(lastname)))
                throw new System.Exception();
            if ( kebabStyle != (lastname = Naming.ToKebabStyle(lastname)))
                throw new System.Exception();
            if ( camelStyle != ( lastname = Naming.ToCamelStyle(lastname)))
                throw new System.Exception();
            if ( capitalStyle!= (lastname = Naming.ToCapitalStyle(lastname)))
                throw new System.Exception();

            this.messages.Add($"can convert name: {lastname} to diffrent styles");
        }

        private void canIdentifyCapitalStyleName()
        {
            string capitalStyle = "AppModule";
            string snakeStyle = "app_module";
            string dollarStyle = "$appModule";
            string kebabStyle = "app-module";
            string camelStyle = "appModule";

            if(true != Naming.IsCapitalStyle(capitalStyle))
                throw new System.Exception();
            if ( false != Naming.IsCapitalStyle(snakeStyle))
                throw new System.Exception();
            if ( false != Naming.IsCapitalStyle(dollarStyle))
                throw new System.Exception();
            if ( false != Naming.IsCapitalStyle(kebabStyle))
                throw new System.Exception();
            if ( false != Naming.IsCapitalStyle(camelStyle))
                throw new System.Exception();

            this.messages.Add($"can identify capital style name");
        }

        private void canIdentifySnakeStyleName()
        {
            string capitalStyle = "AppModule";
            string snakeStyle = "app_module";
            string dollarStyle = "$appModule";
            string kebabStyle = "app-module";
            string camelStyle = "appModule";

            if (false != Naming.IsSnakeStyle(capitalStyle))
                throw new System.Exception();
            if( true != Naming.IsSnakeStyle(snakeStyle))
                throw new System.Exception();
            if ( false != Naming.IsSnakeStyle(dollarStyle))
                throw new System.Exception();
            if ( false != Naming.IsSnakeStyle(kebabStyle))
                throw new System.Exception();
            if ( false != Naming.IsSnakeStyle(camelStyle))
                throw new System.Exception();

            this.messages.Add($"can identify snake style name");
        }

        private void canIdentifyKebabStyleName()
        {
            string capitalStyle = "AppModule";
            string snakeStyle = "app_module";
            string dollarStyle = "$appModule";
            string kebabStyle = "app-module";
            string camelStyle = "appModule";

            if( false != Naming.IsKebabStyle(capitalStyle))
                throw new System.Exception();
            if ( false != Naming.IsKebabStyle(snakeStyle))
                throw new System.Exception();
            if ( false != Naming.IsKebabStyle(dollarStyle))
                throw new System.Exception();
            if ( true != Naming.IsKebabStyle(kebabStyle))
                throw new System.Exception();
            if ( false != Naming.IsKebabStyle(camelStyle))
                throw new System.Exception();

            this.messages.Add($"can identify kebab style name");
        }

        
    }
}

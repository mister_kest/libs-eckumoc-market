﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;

namespace EcKuMoC.Controllers
{
    public class MoviesController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }


        public IActionResult SeeMostPopularMovies()
        {
            return View("Latest");
        }

        public IActionResult SeeNewestMovies()
        {
            return View("Populare");
        }

        public IActionResult SeeTopRatedMovies()
        {
            return View("Rating");
        }
    }
}

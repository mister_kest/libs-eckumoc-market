﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace LibsDb.LibsEntities
{
    [DisplayName("Литературная единица выдачи")]
    public class Liter
    {
        [Key]
        public int ID { get; set; }


        [DisplayName("Заголовок")]
        public string Title { get; set; }


        [DisplayName("Статус")]
        public int Status { get; set; }

        [DisplayName("Держатель")]
        public int HolderID { get; set; }
        public virtual Holder Holder { get; set; }


        [DisplayName("Обложка")]
        public int ImageID { get; set; }
        public virtual ApplicationDb.Entities.Resource Image { get; set; }
      
    }
}

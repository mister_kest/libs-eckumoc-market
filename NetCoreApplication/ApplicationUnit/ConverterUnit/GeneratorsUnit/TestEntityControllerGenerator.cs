﻿using SpbPublicLibsCore.Domain.Odbc.DataSource;
using SpbPublicLibsCore.Domain.Odbc.Metadata;
using SpbPublicLibsCore.Testing;

using System;

namespace SpbPublicLibsCore.Converter.Generators
{
    public class TestEntityControllerGenerator: TestingUnit
    {
 
        protected override void onTest()
        {
            canGenerateEnitityController();
        }

        private void canGenerateEnitityController()
        {            
            MySqlOdbcDataSource dataSource = new MySqlOdbcDataSource();
            EntityControllerGenerator generator = new EntityControllerGenerator();
            foreach (var ptable in dataSource.GetDatabaseMetadata().Tables)
            {
                TableMetaData metadata = dataSource.GetDatabaseMetadata().Tables[ptable.Key];
                //Console.WriteLine(generator.CreateEntityController(metadata));                
            }
            this.messages.Add("can generate entity controllers");
        }
    }
}

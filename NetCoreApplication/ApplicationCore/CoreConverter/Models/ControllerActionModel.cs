﻿using System.Collections.Generic;

namespace ApplicationCore.Converter.Models
{
    /// <summary>
    /// Модель параметров вызова удаленной процедуры
    /// </summary>
    public class ControllerActionModel
    {
        public string Name { get; set; }
        public string Path { get; set; }
        public Dictionary<string, ActionParameterModel> Parameters { get; set; }
         
    }
}
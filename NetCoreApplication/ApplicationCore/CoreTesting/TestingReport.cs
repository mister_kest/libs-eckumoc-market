﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;

namespace ApplicationCore.Testing
{
    /// <summary>
    /// Отчет о тестировании
    /// </summary>
    public class TestingReport 
    {
        public DateTime started { get; set; }
        public DateTime ended { get; set; }
        public bool failed { get; set; }
        public string name { get; set; }
        public List<string> messages { get; set; }
        public Dictionary<string, TestingReport> subreports { get; set; }


        public TestingReport()
        {
           this.subreports = new Dictionary<string, TestingReport>();
        }


        /// <summary>
        /// Метод получчения числовой информации о результатх тестирования 
        /// </summary>
        /// <returns> числовая информация о результатах тестирования </returns>
        public string GetStat()
        {
            string version = this.failed ? "0" : "1";
            if( this.subreports.Count() > 0)
            {                
                int inc = 0;
                foreach(var p in this.subreports )
                {
                    if( p.Value.failed)
                    {
                        break;
                    }
                    else
                    {
                        inc++;
                    }
                }
                version += "."+inc;
            }
            return version;
        }


        /// <summary>
        /// Составление текстового документа, содержащего информацию о результатах тестирования
        /// </summary>
        /// <param name="isTopReport"> true, если отчет составлен на верхнем уровне </param>
        /// <returns> теккстовый документ </returns>
        public string ToDocument(bool isTopReport=true)
        {
            string document = isTopReport?  $"Version: {this.GetStat()}\n": "";
            foreach (string message in messages)
            {
                document += message + "\n";
            }
            foreach( var pair in this.subreports)
            {
                document += pair.Key + "\n";
                document += pair.Value.ToDocument(false);
            }
            return document;
        }


        /// <summary>
        /// Преобразование в текстовый формат
        /// </summary>
        /// <returns> текстовые данные </returns>
        public override string ToString()
        {
            return JObject.FromObject(this).ToString();
        }
    }
}

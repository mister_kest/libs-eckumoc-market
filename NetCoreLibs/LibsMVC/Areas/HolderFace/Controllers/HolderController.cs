﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using LibsDb;
using LibsDb.LibsEntities;
using MvcMarketPlace.Mvc.AuthenticationAttributes;

namespace LibsMVC.Areas.HolderFace.Controllers
{
    [IfUserInRole("Holder")]
    [Area("HolderFace")]
    public class HolderController : Controller
    {
        private readonly LibsDbContext _context;
        
        public HolderController(LibsDbContext context)
        {
            _context = context;
        }

 
        
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var holder = await _context.Holders
                .Include(h => h.Lib)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (holder == null)
            {
                return NotFound();
            }
            return View(holder);
        }



        // GET: Holder/Holders/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var holder = await _context.Holders.FindAsync(id);
            if (holder == null)
            {
                return NotFound();
            }
            ViewData["LibID"] = new SelectList(_context.Libs, "ID", "ID", holder.LibID);
            return View(holder);
        }



        // POST: Holder/Holders/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Location,LibID")]  Holder holder)
        {
            if (id != holder.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(holder);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!HolderExists(holder.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["LibID"] = new SelectList(_context.Libs, "ID", "ID", holder.LibID);
            return View(holder);
        }


        private bool HolderExists(int id)
        {
            return _context.Holders.Any(e => e.ID == id);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using ApplicationCore.Domain;

namespace SpbPublicLibsWeb.Controllers.PagesUser.Admin
{
    
    

    [ApiController]
    [Route("[controller]")]
    public class NetworkController : ControllerBase
    {
        
        private readonly ILogger<NetworkController> _logger;

        public NetworkController(ILogger<NetworkController> logger )
        {
            _logger = logger;
        }

        [HttpGet]
        public List<PortInfo> Get()
        {
            IPGlobalProperties properties = IPGlobalProperties.GetIPGlobalProperties();
            IPEndPoint[] tcpEndPoints = properties.GetActiveTcpListeners();
            TcpConnectionInformation[] tcpConnections = properties.GetActiveTcpConnections();

            return tcpConnections.Select(p =>
            {
                return new PortInfo(
                    i: p.LocalEndPoint.Port,
                    local: String.Format("{0}:{1}", p.LocalEndPoint.Address, p.LocalEndPoint.Port),
                    remote: String.Format("{0}:{1}", p.RemoteEndPoint.Address, p.RemoteEndPoint.Port),
                    state: p.State.ToString());
            }).ToList();
        }
 
    }
}

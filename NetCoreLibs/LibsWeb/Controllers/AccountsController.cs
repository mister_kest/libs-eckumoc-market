using System.Linq;
using ApplicationDb;
 using LibsDb; 
using LibsDb.LibsEntities;
using ApplicationDb.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;


[ApiController]
[Route("api/Accounts/[action]")]
public class AccountsController: ControllerBase
{
	private readonly LibsDbContext _db; 


	public AccountsController( LibsDbContext db ){
		_db=db;
	}

	public void Create( Account record )	{
		_db.Accounts.Add(record);
		_db.SaveChanges();
	}

	public void Remove( int id ){
		_db.Accounts.Remove(this.Find(id));
		_db.SaveChanges();
	}

	public Account Find( int id )	{
		 return _db.Accounts.Find(id);
	}

	public System.Collections.Generic.List<Account> List(   )	{
		 return _db.Accounts.ToList();
	}

}
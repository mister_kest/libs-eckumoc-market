﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace Startup
{
    /// <summary>
    /// Интерфейс элемента конфигурации хоста
    /// </summary>
    public interface IStartupElement
    {
        public void ConfigureServices(IServiceCollection services);
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env);
    }
}

﻿using ApplicationDb.Entities;
using CoreApp.AppAPI;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using System;

namespace PublicLibsAspNet.WebAPI
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class AccountApiController : ControllerBase
    {
        private APIAuthorization _authorization;
        private ILogger<AccountApiController> _logger;

        public AccountApiController(
            ILogger<AccountApiController> logger,
            APIAuthorization authorization)
        {
            _authorization = authorization;
            _logger = logger;
        }

        public object IsSignin()
        {
            return new
            {
                IsSignin = _authorization.IsSignin()
            };
        }

        public object Signin(string Email, string Password)
        {
            try
            {
                _authorization.Signin(Email, Password);
            }
            catch (Exception ex)
            {
                return new
                {
                    status = "failed",
                    message = ex.Message
                };
            }
            return new
            {
                status = "success"
            };
        }

        public object Signout()
        {
            try
            {
                _authorization.Signout();
            }
            catch (Exception ex)
            {
                return new
                {
                    status = "failed",
                    message = ex.Message
                };
            }
            return new
            {
                status = "success"
            };
        }


        public object Signup(string Email, string Password, string Confirmation, string SurName, string FirstName, string LastName, DateTime Birthday)
        {
            try
            {
                _authorization.Signup(Email, Password, Confirmation, SurName, FirstName, LastName, Birthday, "");
            }
            catch (Exception ex)
            {
                return new
                {
                    status = "failed",
                    message = ex.Message
                };
            }
            return new
            {
                status = "success"
            };
        }

        public User Verify()
        {
            return _authorization.Verify();
        }

        public object Has(string Email)
        {
            return new
            {
                result = _authorization.Has(Email)
            };
        }
    }



}


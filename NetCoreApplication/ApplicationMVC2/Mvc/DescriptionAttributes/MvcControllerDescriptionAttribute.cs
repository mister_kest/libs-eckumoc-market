﻿using Microsoft.AspNetCore.Mvc.ApplicationModels;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpbPublicLibsWeb.MVC.DescriptionAttributes
{
    public class MvcControllerDescriptionAttribute : Attribute, IControllerModelConvention
    {
        private readonly string _description;

        public MvcControllerDescriptionAttribute(string description)
        {
            _description = description;
        }

        public void Apply(ControllerModel controllerModel)
        {
            controllerModel.Properties["description"] = _description;
        }
    }
}

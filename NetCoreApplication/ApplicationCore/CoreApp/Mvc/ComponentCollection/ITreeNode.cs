﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpbPublicLibsЬMVC.Mvc.API
{
    public interface ITreeNode<T>
    {
        T GetItem();
        void SetParent(ITreeNode<T> parent);
        Dictionary<string, ITreeNode<T>> GetChildren();
    }
}

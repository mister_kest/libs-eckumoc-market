﻿using Microsoft.AspNetCore.Builder;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Linq;

using ApplicationCore.Domain.Odbc.DataSource;

using Startup;

using System;

namespace ApplicationCore.Domain.Odbc
{
    
    /// <summary>
    /// Внешний модуль предоставляет возможность подключения службы Restfull для работы с ODBC источниками данных.
    /// </summary>
    public static class RestfullOdbcExtensions
    {


        public static IServiceCollection AddOdbc(this IServiceCollection services )
        {
            if (OdbcSingleton.CONFIGURED == false)
            {
                services.AddSingleton<OdbcSingleton>();
                OdbcSingleton.CONFIGURED = true;
            }
            return services;
        }

        /// <summary>
        /// Подключает компонент прожемуточного ПО, обрабатывающий http-запросы согласно 
        /// технологии построения служб Restfull
        /// </summary>
        /// <param name="builder"> обьект выполняющий настройку ПО промежуточного слоя </param>
        /// <param name="connectionString"> строка соединения ADO </param>
        /// <returns></returns>
        public static IApplicationBuilder UseOdbcForSqlServer(this IApplicationBuilder builder, string connectionString)
        {
            connectionString = FromAdoToOdbcConnectionStringForSqlServer(connectionString);
            SqlServerOdbcDataSource odbc = new SqlServerOdbcDataSource(connectionString);
            //Console.WriteLine(JObject.FromObject(odbc.GetDatabaseMetadata()).ToString());
            if(connectionString==null || connectionString.ToLower().IndexOf("server")==-1 || connectionString.ToLower().IndexOf("data") == -1)
            {
                throw new System.Exception(
                    "Проверите правильность строки подключения к SQL Server: "+connectionString+
                    "используйте параметры Server и Database");
            }
            return builder.UseMiddleware<RestfullOdbcComponent>(connectionString);
        }

        /// <summary>
        /// Форматирует строку подключения ADO в ODBC для SQL Server.        
        /// Необходимо добавить параметр указывающий на драйвер ODBC для SQL Server;
        /// </summary>
        /// <param name="adoConnectionString"> строка соединения ADO </param>
        /// <returns> строка соединения ODBC </returns>
        public static string FromAdoToOdbcConnectionStringForSqlServer( string adoConnectionString )
        {
            return "Driver={SQL Server};"+adoConnectionString.Replace(@"\\",@"\");
        }
        
    }
}

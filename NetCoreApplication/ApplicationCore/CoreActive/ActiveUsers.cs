﻿using ApplicationDb.Entities;

using CoreApp.AppAPI;
using Microsoft.Extensions.Logging;

namespace CoreApp.AppActive
{
    /// <summary>
    /// Служба уровня приложения, содержит пользователей сеансов
    /// </summary>
    public class ActiveUsers: ActiveCollection<User>, APIUsers
    {
        public ActiveUsers( ILogger<ActiveUsers> logger, ActiveOptions options ): base( logger, options){}
    }
}

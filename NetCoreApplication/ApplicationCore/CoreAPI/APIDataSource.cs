﻿using ApplicationCore.Domain.Odbc.Metadata;

using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.CoreAPI
{
    public interface APIDataSource
    {
        DatabaseMetadata GetDatabaseMetadata();
    }
}

﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MvcMarketPlace.Core
{
    public class MarketPlaceAuthenticationService: IMarketPlaceAuthentication
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly ILogger<MarketPlaceAuthenticationService> _logger;

        public MarketPlaceAuthenticationService(
            ILogger<MarketPlaceAuthenticationService> logger,
            IHttpContextAccessor httpContextAccessor,
            SignInManager<IdentityUser> signInManager,
            UserManager<IdentityUser> userManager)
        {
            _httpContextAccessor = httpContextAccessor;
            _signInManager = signInManager;
            _userManager = userManager;
            _logger = logger;
        }

        public bool Validate()
        {
            throw new NotImplementedException();
        }

        public bool Login(string Email, string Password, bool RememberMe)
        {
            var result = _signInManager.PasswordSignInAsync(Email, Password, RememberMe, lockoutOnFailure: false).Result;
            return result.Succeeded;
        }

        public bool Logout()
        {
            _signInManager.SignOutAsync().Wait();
            return true;
        }
    }
}

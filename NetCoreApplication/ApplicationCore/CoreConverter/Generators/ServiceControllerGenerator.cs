﻿using ApplicationCore.Converter.Models;
using ApplicationCore.Domain.Reflection;
using ApplicationCore.Utils;

using System;
using System.Collections.Generic;
using System.Reflection;

namespace ApplicationCore.Converter.Generators
{
    public class ServiceControllerGenerator
    {

        public Dictionary<string, string> GetWebApi()
        {
            Dictionary<string, string> api = new Dictionary<string, string>();
            foreach (var p in GetControllersMap())
            {
                api[p.Key]=this.CreateServiceController(p.Value);
            }
            return api;
        }
        


        /// <summary>
        /// Выполнение генерации сервиса, связанного с методами контроллера
        /// </summary>
        /// <param name="ServiceControllerModel"> модель операций контроллера </param> 
        /// <returns></returns>
        public string CreateServiceController(ServiceControllerModel controllerModel )
        {
            string typeScript = "import { HttpClient,HttpParams } from '@angular/common/http';\n";            
            typeScript += "import { Injectable } from '@angular/core';\n\n";
            typeScript += "@Injectable({ providedIn: 'root' })\n";
            typeScript += $"export class {controllerModel.Name}Service" + "\n{\n\n";
            typeScript += $"\tconstructor( private http: HttpClient )" + "{}\n\n";
            foreach( var actionKV in controllerModel.Actions)
            {
                ControllerActionModel actionModel = actionKV.Value;
                string tsName = Naming.ToCamelStyle(actionModel.Name);
                string tsParamDeclaration = "";
                string tsParamMap = "{\n";
                foreach ( var key in actionModel.Parameters.Keys)
                {
                    tsParamDeclaration += key + ",";
                    tsParamMap += $"\t\t\t{key}: {key},\n";
                }
                if (tsParamMap.EndsWith(",\n"))
                {
                    tsParamMap = tsParamMap.Substring(0, tsParamMap.Length - 2);
                }
                tsParamMap += "\n\t\t} ";
                if (tsParamDeclaration.EndsWith(","))
                {
                    tsParamDeclaration = tsParamDeclaration.Substring(0, tsParamDeclaration.Length - 1);
                }
                typeScript += $"\tpublic {tsName}( {tsParamDeclaration} )" + "{\n";
                typeScript += $"\t\tlet pars = this.toHttpParams({tsParamMap});\n";
                typeScript += $"\t\treturn this.http.get('{actionModel.Path}',pars);\n";
                typeScript += "\t}\n\n";
            }
            typeScript +=
            "\n\t toHttpParams(obj: any): {[property: string]: string} "+
            "\n\t { " +
            "\n\t     const result: {[property: string]: string} = { }; " +
            "\n\t     Object.getOwnPropertyNames(obj).forEach(name => { " +
            "\n\t         result[name] = JSON.stringify(obj[name]); " +
            "\n\t     }); " +
            "\n\t     return result; " +
            "\n\t }           \n";
            typeScript += "\t}\n";
            return typeScript;         
        }


        /// <summary>
        /// Метод получения публичных методов типа
        /// </summary>
        /// <param name="type"> тип </param>
        /// <returns> открытые методы </returns>
        private string GetPublicMethods(Type type)
        {
            string actions = "";
            foreach (MethodInfo method in new ReflectionService().GetOwnPublicMethods(type))
            {
                string path = "/" + type.Name;
                actions += "\n\t"+
                    method.Name + "(" + new ReflectionService().GetMethodParametersString(method) + "){ \n\t\t\t" +
                        "return this.http.get('" + path + "/" + method.Name + "',{params:" +
                            new ReflectionService().GetMethodParametersBlock(method) + "});\n\t\t\t}\n";
                /* new {
                 name = method.Name,
                 returns= method.ReturnType.Name,
                 args = ReflectionService.GetMethodParameters(method),
                 func = method.Name+"("+ ReflectionService.GetMethodParametersString(method) + 
                     "){ return this.http.get('" + path + "/"+method.Name+"',{params:"+
                         ReflectionService.GetMethodParametersBlock(method) + "});}",

                });*/
            }
            return actions;
        }




        /// <summary>
        /// Получение данных о контроллерах реализованных в данной сборке
        /// </summary>
        /// <returns> карта контроллеров </returns>
        public ControllersMapModel GetControllersMap()
        {
            ControllersMapModel controllersMap = new ControllersMapModel();
            foreach (Type controllerType in AssemblyReader.GetControllers(Assembly.GetExecutingAssembly()))
            {
                ServiceControllerModel model = new ServiceControllerModel() {
                    Name = controllerType.Name,
                    Path = "/" + controllerType.Name.Substring(0, controllerType.Name.Length - "Controller".Length) + "/",
                    Actions = new Dictionary<string, ControllerActionModel>()
                };
                controllersMap[controllerType.Name] = model;
                foreach (MethodInfo method in new ReflectionService().GetOwnPublicMethods(controllerType))
                {
                    if (method.IsPublic)
                    {
                        Dictionary<string, object> pars = new Dictionary<string, object>();
                        model.Actions[method.Name] = new ControllerActionModel() {
                            Name = method.Name,
                            Parameters = new Dictionary<string, ActionParameterModel>(),
                            Path = "/" + controllerType.Name.Substring(0, controllerType.Name.Length - "Controller".Length) + "/" + method.Name
                        };
                        foreach (ParameterInfo par in method.GetParameters())
                        {
                            model.Actions[method.Name].Parameters[par.Name] = new ActionParameterModel()
                            {
                                Name = par.Name,
                                Type = par.ParameterType.Name,
                                IsOptional = par.IsOptional
                            };
                        }
                    }
                }
            }
            return controllersMap;
        }
    }
}

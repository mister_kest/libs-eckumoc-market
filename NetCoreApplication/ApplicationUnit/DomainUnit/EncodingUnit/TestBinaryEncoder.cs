﻿using SpbPublicLibsCore.Testing;
using System;
using Xunit;

namespace SpbPublicLibsUnit.Encoder
{
    class TestBinaryEncoder: TestingUnit
    {
        protected override void onTest()
        {
            this.canConvertCharactersToBinaryCode();
            this.binaryConvertionTest();
        }

        private void canConvertCharactersToBinaryCode()
        {
            BinaryEncoder encoder = new BinaryEncoder();

            string text = "ABBCCCDDDDEEEEE";
            string encoded = encoder.ToBinary(text);
            string decoded = encoder.FromBinary(encoded);

            Console.WriteLine($"text: {text}");
            Console.WriteLine($"encoded: {encoded}");
            Console.WriteLine($"decoded: {decoded}");

            Assert.Equal(text, decoded);

            this.messages.Add("can convert characters to binary code and binary code to characters");
        }

        private void binaryConvertionTest()
        {
            string text = "this is a test";         
            BinaryEncoder haffman = new BinaryEncoder();
            string binary = haffman.ToBinary( text );
            string character = haffman.FromBinary( binary );
            if (character != text)
            {
                this.report.failed = true;
                throw new Exception("convert to/from binary test");
            }
        }
         
    }
}

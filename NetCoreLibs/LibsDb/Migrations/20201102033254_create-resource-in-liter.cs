﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LibsDb.Migrations
{
    public partial class createresourceinliter : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Deliveries_Calendars_CalendarID",
                table: "Deliveries");

            migrationBuilder.DropForeignKey(
                name: "FK_Deliveries_Liters_LiteratureID",
                table: "Deliveries");

            migrationBuilder.DropForeignKey(
                name: "FK_Deliveries_Users_UserID",
                table: "Deliveries");

            migrationBuilder.DropForeignKey(
                name: "FK_Recoveries_Calendars_CalendarID",
                table: "Recoveries");

            migrationBuilder.DropForeignKey(
                name: "FK_Recoveries_Liters_LiteratureID",
                table: "Recoveries");

            migrationBuilder.DropForeignKey(
                name: "FK_Recoveries_Users_UserID",
                table: "Recoveries");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Recoveries",
                table: "Recoveries");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Deliveries",
                table: "Deliveries");

            migrationBuilder.DropIndex(
                name: "IX_Deliveries_UserID",
                table: "Deliveries");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Deliveries");

            migrationBuilder.RenameTable(
                name: "Recoveries",
                newName: "RecoveryFacts");

            migrationBuilder.RenameTable(
                name: "Deliveries",
                newName: "DeliveryFacts");

            migrationBuilder.RenameIndex(
                name: "IX_Recoveries_UserID",
                table: "RecoveryFacts",
                newName: "IX_RecoveryFacts_UserID");

            migrationBuilder.RenameIndex(
                name: "IX_Recoveries_LiteratureID",
                table: "RecoveryFacts",
                newName: "IX_RecoveryFacts_LiteratureID");

            migrationBuilder.RenameIndex(
                name: "IX_Recoveries_CalendarID",
                table: "RecoveryFacts",
                newName: "IX_RecoveryFacts_CalendarID");

            migrationBuilder.RenameIndex(
                name: "IX_Deliveries_LiteratureID",
                table: "DeliveryFacts",
                newName: "IX_DeliveryFacts_LiteratureID");

            migrationBuilder.RenameIndex(
                name: "IX_Deliveries_CalendarID",
                table: "DeliveryFacts",
                newName: "IX_DeliveryFacts_CalendarID");

            migrationBuilder.AddColumn<int>(
                name: "ImageID",
                table: "Liters",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "OrderID",
                table: "DeliveryFacts",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_RecoveryFacts",
                table: "RecoveryFacts",
                column: "ID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_DeliveryFacts",
                table: "DeliveryFacts",
                column: "ID");

            migrationBuilder.CreateTable(
                name: "DeliveryOrders",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DeliveryOrders", x => x.ID);
                    table.ForeignKey(
                        name: "FK_DeliveryOrders_Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Liters_ImageID",
                table: "Liters",
                column: "ImageID");

            migrationBuilder.CreateIndex(
                name: "IX_DeliveryFacts_OrderID",
                table: "DeliveryFacts",
                column: "OrderID");

            migrationBuilder.CreateIndex(
                name: "IX_DeliveryOrders_UserID",
                table: "DeliveryOrders",
                column: "UserID");

            migrationBuilder.AddForeignKey(
                name: "FK_DeliveryFacts_Calendars_CalendarID",
                table: "DeliveryFacts",
                column: "CalendarID",
                principalTable: "Calendars",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_DeliveryFacts_Liters_LiteratureID",
                table: "DeliveryFacts",
                column: "LiteratureID",
                principalTable: "Liters",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_DeliveryFacts_DeliveryOrders_OrderID",
                table: "DeliveryFacts",
                column: "OrderID",
                principalTable: "DeliveryOrders",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Liters_Resources_ImageID",
                table: "Liters",
                column: "ImageID",
                principalTable: "Resources",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_RecoveryFacts_Calendars_CalendarID",
                table: "RecoveryFacts",
                column: "CalendarID",
                principalTable: "Calendars",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_RecoveryFacts_Liters_LiteratureID",
                table: "RecoveryFacts",
                column: "LiteratureID",
                principalTable: "Liters",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_RecoveryFacts_Users_UserID",
                table: "RecoveryFacts",
                column: "UserID",
                principalTable: "Users",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DeliveryFacts_Calendars_CalendarID",
                table: "DeliveryFacts");

            migrationBuilder.DropForeignKey(
                name: "FK_DeliveryFacts_Liters_LiteratureID",
                table: "DeliveryFacts");

            migrationBuilder.DropForeignKey(
                name: "FK_DeliveryFacts_DeliveryOrders_OrderID",
                table: "DeliveryFacts");

            migrationBuilder.DropForeignKey(
                name: "FK_Liters_Resources_ImageID",
                table: "Liters");

            migrationBuilder.DropForeignKey(
                name: "FK_RecoveryFacts_Calendars_CalendarID",
                table: "RecoveryFacts");

            migrationBuilder.DropForeignKey(
                name: "FK_RecoveryFacts_Liters_LiteratureID",
                table: "RecoveryFacts");

            migrationBuilder.DropForeignKey(
                name: "FK_RecoveryFacts_Users_UserID",
                table: "RecoveryFacts");

            migrationBuilder.DropTable(
                name: "DeliveryOrders");

            migrationBuilder.DropIndex(
                name: "IX_Liters_ImageID",
                table: "Liters");

            migrationBuilder.DropPrimaryKey(
                name: "PK_RecoveryFacts",
                table: "RecoveryFacts");

            migrationBuilder.DropPrimaryKey(
                name: "PK_DeliveryFacts",
                table: "DeliveryFacts");

            migrationBuilder.DropIndex(
                name: "IX_DeliveryFacts_OrderID",
                table: "DeliveryFacts");

            migrationBuilder.DropColumn(
                name: "ImageID",
                table: "Liters");

            migrationBuilder.DropColumn(
                name: "OrderID",
                table: "DeliveryFacts");

            migrationBuilder.RenameTable(
                name: "RecoveryFacts",
                newName: "Recoveries");

            migrationBuilder.RenameTable(
                name: "DeliveryFacts",
                newName: "Deliveries");

            migrationBuilder.RenameIndex(
                name: "IX_RecoveryFacts_UserID",
                table: "Recoveries",
                newName: "IX_Recoveries_UserID");

            migrationBuilder.RenameIndex(
                name: "IX_RecoveryFacts_LiteratureID",
                table: "Recoveries",
                newName: "IX_Recoveries_LiteratureID");

            migrationBuilder.RenameIndex(
                name: "IX_RecoveryFacts_CalendarID",
                table: "Recoveries",
                newName: "IX_Recoveries_CalendarID");

            migrationBuilder.RenameIndex(
                name: "IX_DeliveryFacts_LiteratureID",
                table: "Deliveries",
                newName: "IX_Deliveries_LiteratureID");

            migrationBuilder.RenameIndex(
                name: "IX_DeliveryFacts_CalendarID",
                table: "Deliveries",
                newName: "IX_Deliveries_CalendarID");

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Deliveries",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Recoveries",
                table: "Recoveries",
                column: "ID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Deliveries",
                table: "Deliveries",
                column: "ID");

            migrationBuilder.CreateIndex(
                name: "IX_Deliveries_UserID",
                table: "Deliveries",
                column: "UserID");

            migrationBuilder.AddForeignKey(
                name: "FK_Deliveries_Calendars_CalendarID",
                table: "Deliveries",
                column: "CalendarID",
                principalTable: "Calendars",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Deliveries_Liters_LiteratureID",
                table: "Deliveries",
                column: "LiteratureID",
                principalTable: "Liters",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Deliveries_Users_UserID",
                table: "Deliveries",
                column: "UserID",
                principalTable: "Users",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Recoveries_Calendars_CalendarID",
                table: "Recoveries",
                column: "CalendarID",
                principalTable: "Calendars",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Recoveries_Liters_LiteratureID",
                table: "Recoveries",
                column: "LiteratureID",
                principalTable: "Liters",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Recoveries_Users_UserID",
                table: "Recoveries",
                column: "UserID",
                principalTable: "Users",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

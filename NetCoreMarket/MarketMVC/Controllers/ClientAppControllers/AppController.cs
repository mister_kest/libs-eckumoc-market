﻿
using CoreApp.AppAPI;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ApplicationCore.Domain;
using ApplicationCore.Domain.Reflection;
using ApplicationCore.Messaging;

using System;
using System.Collections.Concurrent;

namespace SpbPublicLibsWeb.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AppController : ControllerBase
    {   
/*
        private readonly ILogger<AppController> _logger;
        private readonly IApplication _app;
        private readonly APIAuthorization _authentication;


        public AppController(
            ILogger<AppController> logger, 
            IApplication app,
            APIAuthorization authentication)
        {            
            _logger = logger;
            _app = app;
            _authentication = authentication;
            _logger.LogInformation("Create");
        }
        

        public ResponseMessage GetRequest(string path, string pars)
        {
            _logger.LogInformation("GetRequest");
            RequestMessage request = new RequestMessage();
            ResponseMessage response = new ResponseMessage();
            try
            {
                if (path == null && pars == null)
                {
                    //string token = HttpContext.Request.Headers["Authorization"];
                    ConcurrentDictionary<string,object> session =_authentication.Session();
                    response.data = (session != null) ? new ReflectionService().GetSkeleton(session) : null;
                    return response;
                }
                request.request = new RequestParams()
                {
                    path = path,
                    pars = JsonConvert.DeserializeObject<JObject>(pars)
                };
                response.request = request;
                this._app.OnMessage(request, response);
            }
            catch (Exception ex)
            {
                SendErrorMessage(response, ex);
            } 
            return response;
        }

       
        public ResponseMessage GetSchema( )
        {
            _logger.LogInformation("GetSchema");
            RequestMessage request = new RequestMessage();
            ResponseMessage response = new ResponseMessage();
            try
            {
                response.request = request;
                string token = HttpContext.Request.Headers["Authorization"];
                response.data = this._app.GetApiContoller(token);
            }
            catch (Exception ex)
            {
                SendErrorMessage(response, ex);
            }
            return response;
        }


        /// <summary>
        /// Send message with info about catcher error.
        /// </summary>
        /// <param name="response">output response message</param>
        /// <param name="ex"> catched exception</param>
        private void SendErrorMessage(ResponseMessage response, Exception ex)
        {
            _logger.LogInformation("SendErrorMessage");
            while (ex.InnerException!=null)
            {
                ex = ex.InnerException;
            }
            response.message = "" + ex.Message;
            response.status = "failed";
        }

        */
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using LibsDb;
using LibsDb.LibsEntities;

namespace LibsMVC.Areas.HolderFace.Controllers
{
    [Area("HolderFace")]
    public class HolderLitersController : Controller
    {
        private readonly LibsDbContext _context;

        public HolderLitersController(LibsDbContext context)
        {
            _context = context;
        }

        // GET: Holder/Liters
        public async Task<IActionResult> Index()
        {
            var libsDbContext = _context.Liters.Include(l => l.Holder);
            return View(await libsDbContext.ToListAsync());
        }

        // GET: Holder/Liters/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var liter = await _context.Liters
                .Include(l => l.Holder)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (liter == null)
            {
                return NotFound();
            }

            return View(liter);
        }

        // GET: Holder/Liters/Create
        public IActionResult Create()
        {
            ViewData["HolderID"] = new SelectList(_context.Holders, "ID", "ID");
            return View();
        }

        // POST: Holder/Liters/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Title,Status,HolderID")] Liter liter)
        {
            if (ModelState.IsValid)
            {
                _context.Add(liter);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["HolderID"] = new SelectList(_context.Holders, "ID", "ID", liter.HolderID);
            return View(liter);
        }

        // GET: Holder/Liters/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var liter = await _context.Liters.FindAsync(id);
            if (liter == null)
            {
                return NotFound();
            }
            ViewData["HolderID"] = new SelectList(_context.Holders, "ID", "ID", liter.HolderID);
            return View(liter);
        }

        // POST: Holder/Liters/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Title,Status,HolderID")] Liter liter)
        {
            if (id != liter.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(liter);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!LiterExists(liter.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["HolderID"] = new SelectList(_context.Holders, "ID", "ID", liter.HolderID);
            return View(liter);
        }

        // GET: Holder/Liters/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var liter = await _context.Liters
                .Include(l => l.Holder)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (liter == null)
            {
                return NotFound();
            }

            return View(liter);
        }

        // POST: Holder/Liters/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var liter = await _context.Liters.FindAsync(id);
            _context.Liters.Remove(liter);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool LiterExists(int id)
        {
            return _context.Liters.Any(e => e.ID == id);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationDb.Entities
{
    public class Address
    {
        public string Name { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
    }
}

using System.Linq;
using ApplicationDb;
 using LibsDb; 
using LibsDb.LibsEntities;
using ApplicationDb.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;


[ApiController]
[Route("api/Apps/[action]")]
public class AppsController: ControllerBase
{
	private readonly LibsDbContext _db; 


	public AppsController( LibsDbContext db ){
		_db=db;
	}

	public void Create( App record )	{
		_db.Apps.Add(record);
		_db.SaveChanges();
	}

	public void Remove( int id ){
		_db.Apps.Remove(this.Find(id));
		_db.SaveChanges();
	}

	public App Find( int id )	{
		 return _db.Apps.Find(id);
	}

	public System.Collections.Generic.List<App> List(   )	{
		 return _db.Apps.ToList();
	}

}
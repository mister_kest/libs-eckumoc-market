using System.Linq;
using ApplicationDb;
 using LibsDb; 
using LibsDb.LibsEntities;
using ApplicationDb.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;


[ApiController]
[Route("api/LoginWeekly/[action]")]
public class LoginWeeklyController: ControllerBase
{
	private readonly LibsDbContext _db; 


	public LoginWeeklyController( LibsDbContext db ){
		_db=db;
	}

	public void Create( LoginWeekly record )	{
		_db.LoginWeekly.Add(record);
		_db.SaveChanges();
	}

	public void Remove( int id ){
		_db.LoginWeekly.Remove(this.Find(id));
		_db.SaveChanges();
	}

	public LoginWeekly Find( int id )	{
		 return _db.LoginWeekly.Find(id);
	}

	public System.Collections.Generic.List<LoginWeekly> List(   )	{
		 return _db.LoginWeekly.ToList();
	}

}
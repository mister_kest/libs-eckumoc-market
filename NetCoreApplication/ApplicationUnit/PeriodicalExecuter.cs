using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using SpbPublicLibsUnit;

namespace SpbPublicLibsBackgroundTesting
{
    public class PeriodicalExecuter : BackgroundService
    {
        private readonly ILogger<PeriodicalExecuter> _logger;
        //private readonly TestDomain _test;
        private readonly JobService _jobs;


        public PeriodicalExecuter(
            ILogger<PeriodicalExecuter> logger,
            JobService jobs
            /*, TestDomain test*/)
        {
            _logger = logger;
            //_test = test;
            _jobs = jobs;
            _logger.LogInformation("Create");
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                _logger.LogInformation("Worker running at: {time}", DateTimeOffset.Now);
                // Console.WriteLine(_test.doTest().ToDocument());
                _jobs.DoCheck();
                await Task.Delay(5000, stoppingToken);
            }
        }
    }
}

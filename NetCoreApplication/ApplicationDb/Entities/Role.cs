﻿using ApplicationDb.Types;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ApplicationDb.Entities
{
    public partial class Role: DimensionTable
    {
        public Role()
        {
            Users = new List<User>();
        }

        [Key]
        public int ID { get; set; }


        [DisplayName("Наименование")]
        [Required(ErrorMessage = "Необходимо указать наменование")]
        public string Name { get; set; }


        [DisplayName("Описание")]
        [DataType(DataType.MultilineText)]
        [Required(ErrorMessage = "Необходимо указать описание роли")]
        public string Description { get; set; }


        [DisplayName("Пользователи")]        
        public virtual List<User> Users { get; set; }


        [DisplayName("Кодовое наименование")]
        [Required(ErrorMessage = "Необходимо указать код роли")]
        public string Code { get; set; }
    }
}

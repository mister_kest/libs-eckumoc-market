﻿using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.Razor.RuntimeCompilation;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace SpbPublicLibsWeb.MVC
{
    public static class MvcConfigurationExtensions
    {

        /// <summary>
        /// Подключает к приложению обьекты из внешней сборки
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddMvcApplicationPart(this IServiceCollection services, Assembly assembly)
        {
            Console.WriteLine($"MvcConfigurationExtensions.AddMvcApplicationPart({assembly.GetName()});");

            /* пример
            var assembly = typeof(FeaturesController).Assembly;
            services
                .AddControllersWithViews()
                .AddApplicationPart(assembly)
                .AddRazorRuntimeCompilation();
            */
            services.AddControllersWithViews().AddApplicationPart(assembly).AddRazorRuntimeCompilation();
            services.Configure<MvcRazorRuntimeCompilationOptions>(options => {
                options.FileProviders.Add(new EmbeddedFileProvider(assembly));
            });
            return services;
        }

         

        /// <summary>
        /// Регистрирует соглашения уровня приложения
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddMvcApplication( this IServiceCollection services )
        {
            Console.WriteLine($"MvcConfigurationExtensions.AddMvcApplication();");
             
            services.AddMvc(options=> {
                
                options.Conventions.Add(new MvcApplicationDescription("This is ASP.NET Core MVC Application"));
                options.Conventions.Add(new MvcNamespaceRoutingConvention("Namespace"));
            });
            return services;
        }
    }
}

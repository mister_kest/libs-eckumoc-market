import { Component, OnInit } from '@angular/core';
import { ReaderOrderService } from './reader-order.service';

@Component({
  selector:     'reader-order',
  templateUrl:  './reader-order.component.html'
})
export class ReaderOrderComponent implements OnInit {

  constructor( public service: ReaderOrderService ) { }

  ngOnInit() {
  }


  checkout(){
    //this.service.checkout();
  }

  submit( modal ){
    //this.service.checkout();
    modal.ok();
  }

}

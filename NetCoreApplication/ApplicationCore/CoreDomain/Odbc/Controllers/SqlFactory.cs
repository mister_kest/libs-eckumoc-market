﻿using ApplicationCore.Domain.Odbc.Metadata;

using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.Domain.Odbc.Controllers
{
    public class SqlFactory
    {

        public string CreateTable( TableMetaData metadata)
        {
            string sql = $"create table {metadata.name}\n";
            sql += "(";
            foreach(var p in metadata.columns)
            {
                sql += $"\n  {p.Key} {toSqlType(p.Value.type)} ";
                if(p.Key.ToLower() == metadata.pk.ToLower())
                {
                    sql += "primary key,";
                }
                else
                {
                    sql += ",";
                }
            }
            if (metadata.columns.Count > 0)
            {
                sql = sql.Substring(0, sql.Length - 1);
            }
            sql += "\n)\n";
            return sql;
        }

        private object toSqlType(string type)
        {
            switch (type.ToLower())
            {
                case "int": return "int";
                case "string": return "nvarchar(max)";
                default: throw new Exception("unsupported type");
            }
        }
    }
}

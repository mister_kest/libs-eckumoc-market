﻿using Microsoft.Extensions.Logging;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace ApplicationCore.Domain.Reflection
{
    public class ReflectionService: IReflection, ILogger<ReflectionService>
    {

        private HashSet<string> PrimitiveTypeNames = new HashSet<string>() {
                "String", "Boolean", "Double", "Int16", "Int32", "Int64", "UInt16", "UInt32", "UInt64" };
        private HashSet<string> ObjectMethods = new HashSet<string>() {
                "GetHashCode", "Equals", "ToString", "GetType", "ReferenceEquals" };


        private readonly ILogger<ReflectionService> _logger;


        public ReflectionService(ILogger<ReflectionService> logger=null)
        {
            _logger = logger;
            
        }


        public List<string> GetAnnotations(MethodInfo method)
        {
            List<string> args = new List<string>();
            foreach (var attribute in method.GetCustomAttributes())
            {
                
            }
            return args;

        }



        public List<string> GetArguments(MethodInfo method)
        {
            List<string> args = new List<string>();
            foreach (ParameterInfo pinfo in method.GetParameters())
            {
                args.Add(pinfo.Name);
            }
            return args;

        }

        public void copy(object item, object target)
        {
            foreach (FieldInfo field in target.GetType().GetFields())
            {
                if( field.GetValue(item) !=
                    target.GetType().GetField(field.Name).GetValue(target))
                {
                    object current, 
                            prev = target.GetType().GetField(field.Name);
                    target.GetType().GetField(field.Name).SetValue(target, current=field.GetValue(item));
                    object evt = new
                    {
                        prev = prev,
                        current = current

                    };
                }
            }
        }

        public object GetValue(object i, string v)
        {
            return i.GetType().GetField(v).GetValue(i);
        }

        public object GetSkeleton(object api)
        {
            return GetSkeleton(api, new List<string>());
        }

        /**
         * Метод получения семантики public-методов обьекта
         */
        public object GetSkeleton(object subject, List<string> path)
        {

            Dictionary<string, object> actionMetadata = new Dictionary<string, object>();
            if (subject == null || subject.GetType().IsPrimitive || PrimitiveTypeNames.Contains(subject.GetType().Name))
            {
                return actionMetadata;
            }
            else
            {
                if (subject is Dictionary<string, object>)
                {
                    foreach (var kv in ((Dictionary<string, object>)subject))
                    {
                        actionMetadata[kv.Key] = kv.Value;
                        if (!kv.Value.GetType().IsPrimitive && !PrimitiveTypeNames.Contains(kv.Value.GetType().Name))
                        {

                            List<string> childPath = new List<string>(path);
                            childPath.Add(kv.Key);
                            actionMetadata[kv.Key] = GetSkeleton(kv.Value, childPath);
                        }
                    };
                }
                else
                {
                    //Debug.WriteLine(JObject.FromObject(subject));
                    Type type = subject.GetType();
                    //Debug.WriteLine(type.Name, path);
                    foreach (MethodInfo info in type.GetMethods())
                    {
                        if (info.IsPublic && !ObjectMethods.Contains(info.Name))
                        {
                            Dictionary<string, object> args = new Dictionary<string, object>();
                            foreach (ParameterInfo pinfo in info.GetParameters())
                            {
                                args[pinfo.Name] = new
                                {
                                    type = pinfo.ParameterType.Name,
                                    optional = pinfo.IsOptional,
                                    name = pinfo.Name
                                };
                            }
                            List<string> actionPath = new List<string>(path);
                            actionPath.Add(info.Name);
                            actionMetadata[info.Name] = new
                            {
                                type = "method",
                                path = actionPath,
                                args = args
                            };
                        }
                    }
                    foreach (FieldInfo info in type.GetFields())
                    {
                        if (info.IsPublic)
                        {
                            if (!info.GetType().IsPrimitive && !PrimitiveTypeNames.Contains(info.GetType().Name))
                            {
                                List<string> childPath = new List<string>(path);
                                childPath.Add(info.Name);
                                actionMetadata[info.Name] = GetSkeleton(info.GetValue(subject), childPath);
                            }
                        }
                    }
                }
            }

            return actionMetadata;
        }


        public static ConstructorInfo GetDefaultConstructor(Type type)
        {
            return (from c in new List<ConstructorInfo>(type.GetConstructors()) where c.GetParameters().Length == 0 select c).SingleOrDefault();
        }


        public Dictionary<string, object> GetStaticMethods(Type type)
        {
            Dictionary<string, object> actionMetadata = new Dictionary<string, object>();
            foreach (MethodInfo info in type.GetMethods())
            {
                if (info.IsPublic && info.IsStatic)
                {
                    Dictionary<string, object> args = new Dictionary<string, object>();
                    foreach (ParameterInfo pinfo in info.GetParameters())
                    {
                        args[pinfo.Name] = new
                        {
                            type = pinfo.ParameterType.Name,
                            optional = pinfo.IsOptional,
                            name = pinfo.Name
                        };
                    }
                }
            }
            return actionMetadata;
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public List<MethodInfo> GetOwnPublicMethods(Type type)
        {
            return (from m in new List<MethodInfo>(type.GetMethods())
                    where m.IsPublic &&
                          !m.IsStatic &&
                          m.DeclaringType.FullName == type.FullName
                    select m).ToList<MethodInfo>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="method"></param>
        /// <returns></returns>
        public Dictionary<string, object> GetMethodParameters(MethodInfo method)
        {
            Dictionary<string, object> args = new Dictionary<string, object>();
            foreach (ParameterInfo pinfo in method.GetParameters())
            {
                args[pinfo.Name] = new
                {
                    type = pinfo.ParameterType.Name,
                    optional = pinfo.IsOptional,
                    name = pinfo.Name
                };
            }
            return args;
        }
     

        public object Invoke(MethodInfo method, object target, JObject args)
        {
            string state = "Поиск обьекта: ";
            Dictionary<string, object> pars;
            List<object> invArgs = null;
            try
            {
                pars = JsonConvert.DeserializeObject<Dictionary<string, object>>(args.ToString());
                invArgs = new List<object>();
                foreach (ParameterInfo pinfo in method.GetParameters())
                {
                    if (pinfo.IsOptional == false && pars.ContainsKey(pinfo.Name) == false)
                    {
                        throw new Exception("require argument " + pinfo.Name);
                    }
                    string parameterName = pinfo.ParameterType.Name;

                    if (parameterName.StartsWith("Dictionary"))
                    {
                        Dictionary<string, object> dictionary = JsonConvert.DeserializeObject<Dictionary<string, object>>(args[pinfo.Name].ToString());
                        invArgs.Add(dictionary);
                    }
                    else
                    {
                        invArgs.Add(pars[pinfo.Name]);
                    }

                }
            }
            catch (Exception ex)
            {
                throw new Exception("ArgumentsException: " + ex.Message, ex);
            }


            try
            {
                object result = method.Invoke(target, invArgs.ToArray());
                state = state.Substring(0, state.Length - 7) + "успех;";
                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error in controller function: " + ex.Message);
                throw ex;
            }

        }


        /**
         * Метод поиска обьекта 
         */
        public Dictionary<string, Object> Find(object subject, string path)
        {
            object p = subject;
            string[] ids = path.Split('.');
            for (int i = 0; i < (ids.Length - 1); i++)
            {
                string id = ids[i];
                if (p is Dictionary<string, object>)
                {
                    p = ((Dictionary<string, object>)p)[id];
                }
                else if (p is ConcurrentDictionary<string, object>)
                {
                    p = ((ConcurrentDictionary<string, object>)p)[id];
                }
                else
                {
                    p = p.GetType().GetField(id).GetValue(p);
                }
            }

            MethodInfo info = null;
            string methodName = ids[ids.Length - 1];

            foreach (var method in p.GetType().GetMethods())
            {
                if (String.Equals(methodName, method.Name))
                {
                    info = method;
                    break;
                }
            }
            Dictionary<string, Object> res = new Dictionary<string, Object>();
            res["method"] = info;
            res["target"] = p;
            res["path"] = path;


            return res;
        }




/*


        public object Invoke(MethodInfo method, object target, JObject args)
        {
            string state = "Поиск обьекта: ";
            Dictionary<string, object> pars;
            List<object> invArgs = null;
            try
            {
                pars = JsonConvert.DeserializeObject<Dictionary<string, object>>(args.ToString());
                invArgs = new List<object>();
                foreach (ParameterInfo pinfo in method.GetParameters())
                {
                    if (pinfo.IsOptional == false && pars.ContainsKey(pinfo.Name) == false)
                    {
                        throw new Exception("require argument " + pinfo.Name);
                    }
                    string parameterName = pinfo.ParameterType.Name;

                    if (parameterName.StartsWith("Dictionary"))
                    {
                        Dictionary<string, object> dictionary = JsonConvert.DeserializeObject<Dictionary<string, object>>(args[pinfo.Name].ToString());
                        invArgs.Add(dictionary);
                    }
                    else
                    {
                        invArgs.Add(pars[pinfo.Name]);
                    }

                }
            }
            catch (Exception ex)
            {
                throw new Exception("Arguments transformation failed orr they are not valid: " + ex.Message);
            }


            try
            {
                object result = method.Invoke(target, invArgs.ToArray());
                state = state.Substring(0, state.Length - 7) + "успех;";
                return result;
            }
            catch (Exception ex)
            {
                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                }
                throw new Exception("Error in controller function: " + ex.Message);
            }
        }
*/

        public string GetMethodParametersBlock(MethodInfo method)
        {
            string s = "{";
            bool needTrim = false;
            foreach (var pair in GetMethodParameters(method))
            {
                needTrim = true;
                s += pair.Key + ':' + pair.Key + ",";
            }
            if (needTrim == true)
                return s.Substring(0, s.Length - 1) + "}";
            else
            {
                return s + "}";
            }
        }


        public string GetMethodParametersString(MethodInfo method)
        {
            bool needTrim = false;
            string s = "";
            foreach (var p in GetMethodParameters(method) )
            {
                needTrim = true;
                s += p.Key + ",";// +":"+ p.Value + ",";
            }
            return needTrim == true ? s.Substring(0, s.Length - 1) : s;
        }


        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            
            throw new NotImplementedException();
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            throw new NotImplementedException();
        }

        public IDisposable BeginScope<TState>(TState state)
        {
            throw new NotImplementedException();
        }


        /**
         * Метод поиска обьекта 
        
        public Dictionary<string, Object> Find(object subject, string path)
        {
            object p = subject;
            if (p == null)
            {
                throw new Exception("subject references to null");
            }
            string[] ids = path.Split(".");
            for (int i = 0; i < (ids.Length - 1); i++)
            {
                string id = ids[i];

                if (p is ConcurrentDictionary<string, object>)
                {
                    p = ((ConcurrentDictionary<string, object>)p)[id];
                }
                else if (p is Dictionary<string, object>)
                {
                    p = ((Dictionary<string, object>)p)[id];
                }
                else
                {
                    p = p.GetType().GetField(id).GetValue(p);
                }
            }
            MethodInfo info = null;
            string methodName = ids[ids.Length - 1];

            foreach (var method in p.GetType().GetMethods())
            {
                if (String.Equals(methodName, method.Name))
                {
                    info = method;
                    break;
                }
            }
            Dictionary<string, Object> res = new Dictionary<string, Object>();
            res["method"] = info;
            res["target"] = p;
            res["path"] = path;

            if (info == null)
            {
                throw new Exception("Method not found.");
            }
            return res;
        } */
    }
}

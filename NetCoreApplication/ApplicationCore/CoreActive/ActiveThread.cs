﻿
using CoreApp.AppAPI;
using Microsoft.Extensions.Logging;

using Newtonsoft.Json.Linq;

using ApplicationCore;
using ApplicationCore.Messaging;

using System;
using System.Diagnostics;
using System.Threading;

namespace CoreApp.AppActive
{
    /// <summary>
    /// Поток управления жизненым циклом обьектов сеанса
    /// </summary>
    public class ActiveThread: IDisposable
    {
        private readonly ILogger<ActiveThread> _logger;
        private readonly ActiveOptions _options;
        private readonly APIUsers _users;
        private readonly APIServices _services;
        private readonly Thread _managment;
        private readonly DateTime _started;

        public ActiveThread( 
            ILogger<ActiveThread> logger,
            ActiveOptions options,
            APIUsers users,
            APIServices services )
        {
            _logger = logger;
            _options = options;
            _users = users;
            _started = DateTime.Now;

            _services = services;
            _managment = new Thread(new ThreadStart(() => {
                try
                {
                    //"wss://localhost:44326/Message"
                    //HubClient client = new HubClient();
                    //client.Connect();
                    while (true)
                    {
                        
                        //_logger.LogInformation($"DoCheck()");
                        _users.DoCheck();
                        _services.DoCheck();
                        Thread.Sleep(_options.CheckTimeout);

                        /*client.Request(new RequestParams()
                        {
                            path = "login.Login",
                            pars = JObject.FromObject(new
                            {
                                login = "eckumoc@gmail.com",
                                password = "sgdf1432"
                            })
                        }, (resp) =>
                        {
                            Debug.WriteLine(resp);
                            Console.WriteLine(resp);
                            return 1;
                        });*/
                    }
                }   
                catch(ThreadAbortException ex)
                {
                    Debug.WriteLine(ex);
                    Console.WriteLine(ex);
                    Dispose();
                }
            }));
            _managment.IsBackground = true;
            _managment.Start();
        }


        /// <summary>
        /// Уничтожение сервиса
        /// </summary>
        public void Dispose()
        {
            _logger.LogInformation("Dispose()");
            _managment.Abort();
        }
    }
}

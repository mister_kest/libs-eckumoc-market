﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MvcMarketPlace.Data.Entities;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MvcMarketPlace.Data
{
    public class MarketPlaceUserManager: UserManager<MarketplaceUser>
    {

        public MarketPlaceUserManager(
            IUserStore<MarketplaceUser> store,
            IOptions<IdentityOptions> optionsAccessor, 
            IPasswordHasher<MarketplaceUser> passwordHasher, 
            IEnumerable<IUserValidator<MarketplaceUser>> userValidators, 
            IEnumerable<IPasswordValidator<MarketplaceUser>> passwordValidators, 
            ILookupNormalizer keyNormalizer, 
            IdentityErrorDescriber errors, 
            IServiceProvider services, 
            ILogger<UserManager<MarketplaceUser>> logger) 
            : base(
                store, 
                optionsAccessor, 
                passwordHasher,               
                userValidators, 
                passwordValidators, 
                keyNormalizer, 
                errors, 
                services, 
                logger) { }
    }
}

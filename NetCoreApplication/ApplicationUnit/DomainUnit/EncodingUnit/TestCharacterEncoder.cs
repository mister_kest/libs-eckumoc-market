﻿using SpbPublicLibsCore.Testing;
using System;
using Xunit;

namespace SpbPublicLibsUnit.Encoder
{
    class TestCharacterEncoder: TestingUnit
    {
        protected override void onTest()
        {
            
            this.canEncodeAndDecodeText();
            this.canSeriallizeAndDeseriallizeState();
        }

        private void canEncodeAndDecodeText()
        {
            string text = "ABBCCCDDDDEEEEE";

            CharacterEncoder encoder = new CharacterEncoder(text);
            string encoded = encoder.Encode(text);
            Console.WriteLine(encoded);

            string decoded = encoder.Decode(encoded);            
            Console.WriteLine(decoded);

            if (decoded != text)
            {
                throw new Exception("can not encode and decode text message");
            }
            else
            {
                this.messages.Add("can encode and decode text message");
            }
        }

        private void canSeriallizeAndDeseriallizeState()
        {            
            string text = "ABBCCCDDDDEEEEE";
            string before, after;
            CharacterEncoder encoder = new CharacterEncoder(text);             
            Console.WriteLine($"encoding: {before=encoder.ToString()}");

            string state = encoder.Seriallize();
            Console.WriteLine($"state: {state}");

            encoder.Deseriallize(state);
            Console.WriteLine($"encoding: {after = encoder.ToString()}");

            if(after != before)
            {
                throw new Exception("seriallization failed");
            }
            else
            {
                this.messages.Add("can seriallize and deserrialize state of encoding");
            }
        }
    }
}

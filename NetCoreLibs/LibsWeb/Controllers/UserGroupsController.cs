using System.Linq;
using ApplicationDb;
 using LibsDb; 
using LibsDb.LibsEntities;
using ApplicationDb.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;


[ApiController]
[Route("api/UserGroups/[action]")]
public class UserGroupsController: ControllerBase
{
	private readonly LibsDbContext _db; 


	public UserGroupsController( LibsDbContext db ){
		_db=db;
	}

	public void Create(UserGroups record )	{
		_db.UserGroups.Add(record);
		_db.SaveChanges();
	}

	public void Remove( int id ){
		_db.UserGroups.Remove(this.Find(id));
		_db.SaveChanges();
	}

	public UserGroups Find( int id )	{
		 return _db.UserGroups.Find(id);
	}

	public System.Collections.Generic.List<UserGroups> List(   )	{
		 return _db.UserGroups.ToList();
	}

}
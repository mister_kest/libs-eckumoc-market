﻿using Microsoft.AspNetCore.Mvc.ApplicationModels;
using System;


namespace SpbPublicLibsWeb.MVC.DescriptionAttributes
{
    /// <summary>
    /// Применяется для изменении имени действия
    /// </summary>
    public class MvcCustomActionNameAttribute : Attribute, IActionModelConvention
    {
        private readonly string _actionName;

        public MvcCustomActionNameAttribute(string actionName)
        {
            _actionName = actionName;
        }

        public void Apply(ActionModel actionModel)
        {
            // this name will be used by routing
            actionModel.ActionName = _actionName;
        }
    }
}

using System.Linq;
using ApplicationDb;
 using LibsDb; 
using LibsDb.LibsEntities;
using ApplicationDb.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;


[ApiController]
[Route("api/Categories/[action]")]
public class CategoriesController: ControllerBase
{
	private readonly LibsDbContext _db; 


	public CategoriesController( LibsDbContext db ){
		_db=db;
	}

	public void Create( Category record )	{
		_db.Categories.Add(record);
		_db.SaveChanges();
	}

	public void Remove( int id ){
		_db.Categories.Remove(this.Find(id));
		_db.SaveChanges();
	}

	public Category Find( int id )	{
		 return _db.Categories.Find(id);
	}

	public System.Collections.Generic.List<Category> List(   )	{
		 return _db.Categories.ToList();
	}

}
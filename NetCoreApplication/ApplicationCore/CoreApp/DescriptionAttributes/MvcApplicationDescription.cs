﻿using Microsoft.AspNetCore.Mvc.ApplicationModels;

using System;

namespace SpbPublicLibsWeb.MVC
{
    public class MvcApplicationDescription : IApplicationModelConvention
    {
        private readonly string _description;

        public MvcApplicationDescription(string description)
        {
            _description = description;
        }

        public void Apply(ApplicationModel application)
        {
            Console.WriteLine("MvcApplicationDescription.Apply()");
            application.Properties["description"] = _description;
        }
    }
}

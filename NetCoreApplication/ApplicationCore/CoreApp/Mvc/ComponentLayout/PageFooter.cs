﻿using Mvc.ComponentControls;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Mvc.ControlsLayout
{
    public class PageFooter : MvcComponent
    {
        public string title = "this is a test";
        public Dictionary<string, ControlButton> menu = new Dictionary<string, ControlButton>();

        public PageFooter( ): base()
        {
            menu["about"] = this.createButton("about", ()=> {
                test();
            });
        }

        private void test()
        {
            for( int i=0; i<10000; i++)
            {
                Debug.WriteLine(i);
            }
        }

        ControlButton createButton( string label, Action click )
        {
            return new ControlButton(label, click  );
        }
    }
}

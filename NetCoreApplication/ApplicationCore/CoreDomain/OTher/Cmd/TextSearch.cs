﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApplicationCore.Domain
{
    public class TextSearch: Cmd
    {
        public TextSearch() : base()
        {
        }



        public string Search(string regularExpression, string filename)
        {
            this.Execute("findstr /n /r " + regularExpression + " " + filename, (linw)=> { return 1; });
            return "";

        }
        

        public Dictionary<string, object> Search( string regularExpression )
        {
            Dictionary<string, object> searchResults = new Dictionary<string, object>();
            foreach( string file in GetAppDataResources("documentation"))
            {
                //string result = this.Execute("findstr /n /r " + regularExpression + " "+file,(line)=> { return 1; });
                string[] results = null;// result.Split("\n");
                searchResults[file] = new HashSet<string>(results);
            }
            return searchResults;            
        }
    }
}

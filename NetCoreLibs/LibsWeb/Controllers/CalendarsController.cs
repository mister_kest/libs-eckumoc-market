using System.Linq;
using ApplicationDb;
 using LibsDb; 
using LibsDb.LibsEntities;
using ApplicationDb.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;


[ApiController]
[Route("api/Calendars/[action]")]
public class CalendarsController: ControllerBase
{
	private readonly LibsDbContext _db; 


	public CalendarsController( LibsDbContext db ){
		_db=db;
	}

	public void Create( Calendar record )	{
		_db.Calendars.Add(record);
		_db.SaveChanges();
	}

	public void Remove( int id ){
		_db.Calendars.Remove(this.Find(id));
		_db.SaveChanges();
	}

	public Calendar Find( int id )	{
		 return _db.Calendars.Find(id);
	}

	public System.Collections.Generic.List<Calendar> List(   )	{
		 return _db.Calendars.ToList();
	}

}
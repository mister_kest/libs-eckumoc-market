class MovieResult
{
    popularity: string;
    vote_count: string;
    video: string;
    poster_path: string;
    id: string;
    adult: string;
    backdrop_path: string;
    original_language: string;
    original_title: string;
    title: string;
    vote_average: string;
    overview: string;
    release_date: string;
}

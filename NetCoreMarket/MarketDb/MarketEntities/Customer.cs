﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MvcMarketPlace.Data.Entities
{
    public class Customer
    {
        public int ID { get; set; }
        public int UserID { get; set; }
    }
}

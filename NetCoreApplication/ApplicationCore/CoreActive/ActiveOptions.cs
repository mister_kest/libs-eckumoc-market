﻿using ApplicationDb.Entities;

namespace CoreApp.AppActive
{
    /// <summary>
    /// Параметры жизненого цикла обьектов сеанса
    /// </summary>
    public class ActiveOptions
    {
        public long SessionTimeout { get; set; }
        public int KeyLength { get; set; }
        public string UserCookie { get; set; }
        public string ServiceCookie { get; set; }
        public int CheckTimeout { get; set; }

        /// <summary>
        /// Роль пользователя по умолчанию,
        /// присваивается пользователям после 
        /// проведеня процедуры регистрации
        /// </summary>
        public string PublicRole { get; set; }
        public string PublicGroup { get; set; }
        

        public ActiveOptions()
        {
            this.SessionTimeout = 1000 * 60;
            this.KeyLength = 32;
            this.UserCookie = "UserKey";
            this.ServiceCookie = "ServiceKey";
            this.CheckTimeout = 1000;
            this.PublicRole = "Читатель";
            this.PublicGroup = "Читатель";
        }
    }
}

#pragma checksum "D:\wrk\market-eckumoc-libs\NetCoreLibs\LibsMVC\Areas\ReaderFace\Views\DeliveryOrders\Edit.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "47a8a70524268d8c742c4d552c187a9b8116ed67"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Areas_ReaderFace_Views_DeliveryOrders_Edit), @"mvc.1.0.view", @"/Areas/ReaderFace/Views/DeliveryOrders/Edit.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"47a8a70524268d8c742c4d552c187a9b8116ed67", @"/Areas/ReaderFace/Views/DeliveryOrders/Edit.cshtml")]
    public class Areas_ReaderFace_Views_DeliveryOrders_Edit : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<LibsDb.LibsEntities.DeliveryOrder>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 3 "D:\wrk\market-eckumoc-libs\NetCoreLibs\LibsMVC\Areas\ReaderFace\Views\DeliveryOrders\Edit.cshtml"
  
    ViewData["Title"] = "Edit";
    Layout = "~/Views/Shared/_Layout.cshtml";

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
<h1>Edit</h1>

<h4>DeliveryOrder</h4>
<hr />
<div class=""row"">
    <div class=""col-md-4"">
        <form asp-action=""Edit"">
            <div asp-validation-summary=""ModelOnly"" class=""text-danger""></div>
            <input type=""hidden"" asp-for=""ID"" />
            <div class=""form-group"">
                <label asp-for=""UserID"" class=""control-label""></label>
                <select asp-for=""UserID"" class=""form-control"" asp-items=""ViewBag.UserID""></select>
                <span asp-validation-for=""UserID"" class=""text-danger""></span>
            </div>
            <div class=""form-group"">
                <input type=""submit"" value=""Save"" class=""btn btn-primary"" />
            </div>
        </form>
    </div>
</div>

<div>
    <a asp-action=""Index"">Back to List</a>
</div>

");
            DefineSection("Scripts", async() => {
                WriteLiteral("\r\n");
#nullable restore
#line 34 "D:\wrk\market-eckumoc-libs\NetCoreLibs\LibsMVC\Areas\ReaderFace\Views\DeliveryOrders\Edit.cshtml"
      await Html.RenderPartialAsync("_ValidationScriptsPartial");

#line default
#line hidden
#nullable disable
            }
            );
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<LibsDb.LibsEntities.DeliveryOrder> Html { get; private set; }
    }
}
#pragma warning restore 1591

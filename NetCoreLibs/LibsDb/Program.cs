﻿using ApplicationCore.Converter;
using ApplicationCore.Domain.Odbc;
using ApplicationCore.Domain.Odbc.DataSource;

using LibsDb.LibsEntities;

using Newtonsoft.Json.Linq;

using System;

namespace LibsDb
{
    class Program
    {
        public static void TestCategoryPath()
        {
            Category ProductsCategory = new Category() { Name = "Products" };
            Category PhonesCategory = new Category() { Name = "Phones" };
            PhonesCategory.Parent = ProductsCategory;

            Console.WriteLine(PhonesCategory.GetPath("-"));
        }


        /// <summary>
        /// Выполняется генерация элементов Rest-архитектуры
        /// </summary>
        /// <param name="args"></param>
        public static void Generate()
        {
            string connectionString = LibsDbContext.DefaultConnectionString;
            string odbcConnectionString = RestfullOdbcExtensions.FromAdoToOdbcConnectionStringForSqlServer(connectionString);
            SqlServerOdbcDataSource dataSource = new SqlServerOdbcDataSource(odbcConnectionString);

            Console.WriteLine(JObject.FromObject(dataSource.GetDatabaseMetadata()));

            ConverterAngular ng = new ConverterAngular();
            ng.Do(dataSource, dataSource.GetDatabaseMetadata().database);
            ng.Save();
        }


        static void Main(string[] args)
        {             
            LibsDbInitiallizer initiallizer = new LibsDbInitiallizer();
            initiallizer.DoInitiallize();
        }
    }
}

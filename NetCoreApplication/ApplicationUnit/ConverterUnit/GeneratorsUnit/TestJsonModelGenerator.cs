﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SpbPublicLibsCore.APIS.TheMovieDatabaseAPI;
using SpbPublicLibsCore.Domain.Odbc.Metadata;
using SpbPublicLibsCore.Testing;

using System;

namespace SpbPublicLibsCore.Converter.Generators
{ 
    public class TestJsonModelGenerator : TestingUnit
    {
        protected override void onTest()
        {
            TheMovieDatabaseService mdb = new TheMovieDatabaseService();
            JsonModelGenerator generator = new JsonModelGenerator();
            JObject json = 
                JObject.FromObject(mdb.getMoviesTopRated().Result);
            TableMetaData table = generator.GenerateCapitalModelFromJson("PublicMovie",json);
            Console.WriteLine(JObject.FromObject(table));
            messages.Add("can generate datamodel from json object");
        }
    }
}

﻿using Microsoft.AspNetCore.Mvc;

namespace Mvc.Controllers
{
    public class AboutController : Controller
    {
        public IActionResult Contacts()
        {
            return View();
        }
        public IActionResult Delivery()
        {
            return View();
        }
        public IActionResult Docs()
        {
            return View();
        }
        public IActionResult Privacy()
        {
            return View();
        }
        public IActionResult Statistics()
        {
            return View();
        }
        public IActionResult Support()
        {
            return View();
        }
    }
}

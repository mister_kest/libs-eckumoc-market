﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using ApplicationCore.Domain.Odbc.Controllers;
using ApplicationCore.Messaging;
using System.Diagnostics;

namespace ApplicationCore.Domain.Odbc
{

    public class RestfullOdbcComponent: BaseComponent
    {        
        private readonly string _connectionString;
   
        private readonly ILogger<RestfullOdbcComponent> _logger;
        private readonly OdbcSingleton _odbcSingleton;

        public RestfullOdbcComponent( 
            RequestDelegate next, 
            OdbcSingleton odbcSingleton,
            ILogger<RestfullOdbcComponent> logger, 
            string connectionString) :base(next)
        {
                     
            _connectionString = connectionString;
            _odbcSingleton = odbcSingleton;
            _logger = logger;
            if(_logger!=null )_logger.LogInformation("Create");
        }

        public override void OnMessage(RequestMessage request, ResponseMessage response)
        {
            if (request.request.path == null || request.request.path.StartsWith("/odbc") == false)
            {
                return;
            }
            else
            {
                request.request.path = request.request.path.Length == 5 ? "" : request.request.path.Substring(6);
                
                switch (request.method)
                {
                    case "GET":
                    case "get":
                        this.DoGet(request, response);
                        break;
                    case "POST":
                    case "post":
                        this.DoPost(request, response);
                        break;
                    case "PUT":
                    case "put":
                        this.DoPut(request, response);
                        break;
                    case "DELETE":
                    case "delete":
                        this.DoDelete(request, response);
                        break;
                    case "PUTCH":
                    case "putch":
                        this.DoPatch(request, response);
                        break;
                }
            }
              
          }





          private long DoPut(RequestMessage request, ResponseMessage response)
          {
              long counter = 0;
              DatabaseManager dbm = _odbcSingleton.Get();
              Dictionary<string, object> pars = request.getParametersMap();
              List<SelectExpression> expressions = this.ParseUrl(request.request.path);
              foreach ( SelectExpression exp in expressions )
              {                
                  if(exp.id == null)
                  {
                      counter += dbm.Get(exp.entity).Create(pars);
                  }
                  else
                  {
                      pars[dbm.Get(exp.entity).GetMetadata().getPrimaryKey()] = exp.id;
                  }                                
              }
              return counter;
          }


          private long DoPost(RequestMessage request, ResponseMessage response)
          {
              long counter = 0;
                DatabaseManager dbm = _odbcSingleton.Get();
                Dictionary<string, object> pars = request.getParametersMap();
                List<SelectExpression> expressions = this.ParseUrl(request.request.path);
              foreach (SelectExpression exp in expressions)
              {
                  
                  if (exp.id != null)
                  {                                                     
                      pars[dbm.Get(exp.entity).GetMetadata().getPrimaryKey()] = exp.id;
                      counter += dbm.Get(exp.entity).Update(pars);
                  }
              }
              return counter;
          }

          public List<SelectExpression> ParseUrl(string url)
          {
              List<SelectExpression> result = new List<SelectExpression>();
              if (url.Length == 0)
              {
                  return result;
              }
              bool isKey = true;
              SelectExpression last = null;
              foreach (string id in url.Split("/"))
              {
                  if (isKey == true)
                  {
                      last = new SelectExpression()
                      {
                          entity = id
                      };
                      result.Add(last);
                  }
                  else
                  {
                      last.id = id;
                  }
                  isKey = isKey == true ? false : true;
              }
              return result;
          }


          private byte[] ReadRequestBody(HttpContext httpContext)
          {
              long? length = httpContext.Request.ContentLength;
              if (length != null)
              {                
                  byte[] data = new byte[(long)length];
                  httpContext.Request.Body.Read(data, 0, (int)length);
                  string mime = httpContext.Request.ContentType;
                  return data;
              }
              return new byte[0];
          }


         


        /// <summary>
        /// Запрос на выборку данных
        /// </summary>
        /// <param name="httpContext"> контекст данных HTTP </param>
        /// <param name="dbm"> обьект управления источниками данных ODBC </param>
        /// <returns></returns>
        private void DoGet(RequestMessage request, ResponseMessage response)
        {
            DatabaseManager dbm = _odbcSingleton==null? new DatabaseManager(_connectionString): _odbcSingleton.Get(_connectionString);
            Dictionary<string, object> pars = request.getParametersMap();
            List<SelectExpression> expressions = this.ParseUrl(request.request.path);
            JToken secret = null;
            List<SelectExpression> selected = new List<SelectExpression>();
            SelectExpression last = null;
            if (expressions.Count == 0)
            {

                response.data = dbm.GetAssociations("$list");
                response.confirmed = true;
            }
            else
            {
                TableManagerStatefull tms = null;
                foreach (SelectExpression ex in expressions)
                {
                    Console.WriteLine(ex.entity + " " + ex.id);
                    if (_logger != null) _logger.LogInformation(ex.entity);

                    if(tms == null)
                    {
                        object result = dbm.GetAssociations(ex.entity);
                        if(result != null)
                        {
                            response.data = result;
                            response.confirmed = true;
                            break;
                        }
                    }
                    else
                    {
                        object result = tms.GetAssociations(ex.entity);
                        if (result != null)
                        {
                            response.data = result;
                            response.confirmed = true;
                            break;
                        }
                    }
                    tms = dbm.Get(ex.entity);

                    
                    if(ex.id == null)
                    {
                        if (response.data == null)
                        {
                            response.data = tms.SelectAll();
                        }
                        else
                        {

                            long k = long.Parse(last.id);
                            response.data = tms.SelectNotReferencesTo(last.entity,k);
                        }
                    }
                    else
                    {
                        response.data = tms.Select(long.Parse(ex.id));
                    }
                    last = ex;
                    

                }
            }
            /*try { 
            }
            catch(Exception ex)
            {
                while (ex.InnerException!=null)
                {
                    ex = ex.InnerException;
                }
                response.message = ex.Message;
                response.status = "failed";
            }
            finally
            {
                if (response.status == null)
                {
                      response.status =  "success";

                }

            }*/
            response.confirmed = true;
        }

        private void SendResponse(HttpContext httpContext, Dictionary<string, object> response)
        {
            JObject responseText = JObject.FromObject(response);
            Console.WriteLine(responseText);
            httpContext.Response.WriteAsync(responseText.ToString(), System.Text.Encoding.Default);
        }

        private bool IsNumber(string text)
        {
            Regex regex = new Regex(@"[\d]");
            return regex.IsMatch(text);
        }

        private void DoDelete(RequestMessage request, ResponseMessage response)
        {
            DatabaseManager dbm = _odbcSingleton.Get();
            Dictionary<string, object> pars = request.getParametersMap();
            List<SelectExpression> expressions = this.ParseUrl(request.request.path);
            foreach (SelectExpression exp in expressions)
            {
                if (exp.id == null)
                {                    
                    dbm.Get(exp.entity).Delete((int)pars[dbm.Get(exp.entity).GetMetadata().getPrimaryKey()]);
                }
                else
                {
                    pars[dbm.Get(exp.entity).GetMetadata().getPrimaryKey()] = exp.id;
                }
            }
        }

        private void DoPatch(RequestMessage request, ResponseMessage response)
        {
            DatabaseManager dbm = _odbcSingleton.Get();
            Dictionary<string, object> parameters = request.getParametersMap();
            List<SelectExpression> expressions = this.ParseUrl(request.request.path);
            foreach (SelectExpression exp in expressions)
            {
                if (exp.id != null)
                {
                    parameters[dbm.Get(exp.entity).GetMetadata().getPrimaryKey()] = exp.id;
                    dbm.Get(exp.entity).Update(parameters);
                }
            }
        }

        protected override void OnException(Exception ex)
        {
            Debug.WriteLine(ex);
        }
    }
}
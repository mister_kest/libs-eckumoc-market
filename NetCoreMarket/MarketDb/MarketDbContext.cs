﻿using ApplicationDb;
using ApplicationDb.Entities;

using Microsoft.EntityFrameworkCore;
using MvcMarketPlace.Data.Entities;
using System;
using System.Diagnostics;

namespace MvcMarketPlace.Data
{
    public class MarketDbContext : ApplicationDbContext
    {
        public new static string DefaultConnectionString =
            "Server=DESKTOP-66CFM7U\\SQLSERVER;" +
            "Database=MarketDb;" +
            "Trusted_Connection=True;" +
            "MultipleActiveResultSets=True";

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Market> Markets { get; set; }
        public new DbSet<MarketplaceUser> Users { get; set; }
        public DbSet<MoneyTransfer> MoneyTransfers { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductCategory> ProductCategories { get; set; }
        public DbSet<ProductCountInfo> ProductCountInfos { get; set; }
        public DbSet<Resource> Resources { get; set; }
        public DbSet<SaleComposition> SaleCompositions { get; set; }
        public DbSet<SaleFacts> SaleFacts { get; set; }
        public DbSet<SepateSubdivision> SepateSubdivisions { get; set; }
        public DbSet<Warehouse> Warehouses { get; set; }


        public MarketDbContext() : base() { }

        public MarketDbContext(
                DbContextOptions options) : base(options) { }


        /// <summary>
        /// Настройка конфигурации контекста данных
        /// </summary>
        /// <param name="optionsBuilder"></param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            Console.WriteLine($"OnConfiguring(DbContextOptionsBuilder optionsBuilder)");
            Debug.WriteLine($"OnConfiguring(DbContextOptionsBuilder optionsBuilder)");
            if (!optionsBuilder.IsConfigured)
            {
                Debug.WriteLine($"\t connectionString={DefaultConnectionString}");
                optionsBuilder.UseSqlServer(DefaultConnectionString);
            }
        }

        
    }
}
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class AppLanguageService
{
  language = 'RU';
  languages = ['RU','EN','DE'];

  onLanguageChanged( language: string ){

    this.language = language;
  }
}

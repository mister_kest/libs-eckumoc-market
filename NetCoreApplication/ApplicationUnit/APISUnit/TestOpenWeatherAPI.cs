﻿using ApplicationCore.APIS.OpenWeatherAPI;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SpbPublicLibsCore.APIS.OpenWeatherAPI;
using SpbPublicLibsCore.Testing;
using SpbPublicLibsCore.Utils;
using System;

namespace SpbPublicLibsUnit.APISUnit
{
    public class TestOpenWeatherAPI : TestingUnit
    {
        protected override void onTest()
        {
            canGetWeatherForecast();
            canGetDailyForecast();
        }

        private void canGetWeatherForecast()
        {
            OnecallForecastAPI api = new OnecallForecastAPI();
            string forecastJson = 
                api.GetOneCall(60.042270, 30.320320, Timing.GetTime()).Result;
            Console.WriteLine(JsonConvert.DeserializeObject<JObject>(forecastJson).ToString());
            messages.Add("can get weather forecast by geo position and time");
        }

        private void canGetDailyForecast()
        {
            WeatherForecastAPI api = new WeatherForecastAPI();
            string forecastJson =
                api.GetWeatherForecastByCity("Moscow").Result;
            Console.WriteLine(JsonConvert.DeserializeObject<JObject>(forecastJson).ToString());
            messages.Add("can get weather forecast by city");

        }
    }
}

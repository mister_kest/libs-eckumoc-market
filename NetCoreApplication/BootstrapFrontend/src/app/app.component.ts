import { AppLanguageService } from './app-language.service';
import { HttpClient } from '@angular/common/http';
import { Component, ViewChild } from '@angular/core';
import { SearchResult } from './app-public/search-result';
import { AppService } from './app.service';
import { SearchComponent } from './shared/search/search.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {

  @ViewChild('searchComponentRef',{static: true}) searchComponentRef: SearchComponent;
  options = [];

  model: SearchResult;

  onInput(evt){

    const ctrl = this;
    this.app.keywordSearch( evt ).subscribe((keywords: any)=>{
      console.log(keywords);
      ctrl.options = keywords.results.map(r=>r.name);
    });
  }

  onSearch(evt){
    this.app.onSearch(evt);
    const ctrl = this;
    this.http.get('/api/search/movieSearch',{
      params: {
        query: evt,
        page: '1'
      }
    }).subscribe((results: any)=>{

      console.log(results);
      ctrl.model = results;
    });

  }


  constructor(
    public app: AppService,
    public lang: AppLanguageService,
    private http: HttpClient) {

  }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading.Channels;
using System.Threading.Tasks;

using CoreApp.AppAPI;


using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading.Channels;
using System.Threading.Tasks;
using ApplicationCore.Domain.Reflection;

namespace LibsWeb.Hubs
{


    public class MessageHub : Hub
    {
        private APIAuthorization _authorization;

        public MessageHub(APIAuthorization authorization)
        {
            _authorization = authorization;
        }

        public async Task Send(string message)
        {
            await Clients.All.SendAsync("Send", message).ConfigureAwait(true);
        }

        public async Task Upload(ChannelReader<string> stream)
        {
            while (await stream.WaitToReadAsync())
            {
                while (stream.TryRead(out var item))
                {
                    Debug.WriteLine(item);
                }
            }
        }



        public async Task Request(JObject requestMessage)
        {
            long start = GetTime();
            Debug.WriteLine(requestMessage);

            Dictionary<string, object> responseMessage = new Dictionary<string, object>();
            responseMessage["connection"] = Context.ConnectionId;
            responseMessage["request"] = requestMessage;
            responseMessage["start"] = start;
            object response = null;
            try
            {
                string token = (requestMessage["token"] != null) ? requestMessage["token"].Value<string>() : null;
                if (requestMessage["request"] == null)
                {

                    response = GetApi(token);
                }
                else
                {
                    JObject requestParameters = (JObject)requestMessage["request"];
                    object controller = GetContoller(token);
                    lock (controller)
                    {
                        if (requestParameters["path"] == null)
                        {
                            throw new Exception("required parameter 'request.path' is missed");
                        }
                        else if (requestParameters["pars"] == null)
                        {
                            throw new Exception("required parameter 'request.pars' is missed");
                        }
                        else if (controller == null)
                        {
                            throw new Exception("api controller object has not been founded");
                        }
                        else
                        {
                            string path, pars;
                            try
                            {
                                path = requestParameters["path"].Value<string>();
                                pars = requestParameters["pars"].ToString();
                            }
                            catch (Exception ex)
                            {
                                throw new Exception("Type of argument path or argument pars is wrong " + ex.Message);
                            }

                            Dictionary<string, Object> prototype = null;
                            if (path == "login.Login")
                            {
                                var args = JsonConvert.DeserializeObject<JObject>(pars);

                                _authorization.Signin(args["login"].Value<string>(), args["password"].Value<string>());
                                object result = null;
                                response = true;
                                responseMessage["status"] = "success";

                                //await Clients.Client(Context.ConnectionId).SendAsync("setup", responseMessage).ConfigureAwait(true);
                            }
                            else
                            {
                                try
                                {
                                    prototype = Find(controller, path);
                                }
                                catch (Exception ex)
                                {
                                    Debug.WriteLine(ex);
                                    //await Clients.Client(Context.ConnectionId).SendAsync("logout", responseMessage).ConfigureAwait(true);
                                    throw new Exception("Function " + path + " not founded.", ex);
                                }
                                try
                                {
                                    object result = Invoke((MethodInfo)prototype["method"], prototype["target"], JsonConvert.DeserializeObject<JObject>(pars));
                                    response = result;
                                    responseMessage["status"] = "success";

                                }
                                catch (Exception ex)
                                {
                                    throw new Exception("path: " + path + "\n error execute function: " + ex.Message, ex);
                                }
                            }


                        }
                    }

                }

            }
            catch (Exception ex)
            {
                responseMessage["status"] = "failed";
                responseMessage["error"] = ex.Message;
            }
            finally
            {
                responseMessage["response"] = response;
                responseMessage["end"] = GetTime();

                //Debug.WriteLine(JObject.FromObject(requestMessage));
                //Debug.WriteLine(JObject.FromObject(responseMessage));

                await Clients.Client(Context.ConnectionId).SendAsync("Response", responseMessage).ConfigureAwait(true);
            }




        }

        private object GetApi(string token)
        {
            return new ReflectionService().GetSkeleton(_authorization.Session());
        }

        private object GetContoller(string token)
        {
            return _authorization.Session();
        }


        /// <summary>
        /// Получение текущего времени в милисекундах
        /// </summary>
        /// <returns></returns>
        public static long GetTime()
        {
            TimeSpan uTimeSpan = (DateTime.Now - new DateTime(1970, 1, 1, 0, 0, 0));
            return (long)uTimeSpan.TotalMilliseconds;
        }


        private Dictionary<string, object> Find(object controller, string path)
        {
            return new ReflectionService().Find(controller,path);
        }

        private object Invoke(MethodInfo methodInfo, object v, JObject jObjects)
        {
            return new ReflectionService().Invoke(methodInfo,v, jObjects);
        }
    }


}
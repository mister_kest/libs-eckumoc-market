﻿using Microsoft.AspNetCore.Mvc.ApplicationModels;
using System;
using System.Linq;

namespace SpbPublicLibsWeb.MVC
{
    public class MvcNamespaceRoutingConvention : IApplicationModelConvention
    {
        private readonly string _suffix;


        /// <summary>
        /// Констурктор принимает в качестве параметра суффикс 
        /// наименования контроллеров к которым применяется 
        /// соглашение
        /// </summary>
        /// <param name="suffix"> суффикс </param>
        public MvcNamespaceRoutingConvention( string suffix )
        {
            _suffix = suffix;
        }


        /// <summary>
        /// Применение соглашения о наименовани контроллеров MVC
        /// </summary>
        /// <param name="application">модель приложения MVC</param>
        public void Apply(ApplicationModel application)
        {
            Console.WriteLine("MvcNamespaceRoutingConvention.Apply().Started");
            foreach (var controller in application.Controllers)
            {
                /*var hasAttributeRouteModels = controller.Selectors
                    .Any(selector => selector.AttributeRouteModel != null);
                if (!hasAttributeRouteModels
                    && controller.ControllerName.Contains(_suffix))                     
                {*/
                    /*string url =
                        controller.ControllerType.Namespace.Replace('.', '/') +
                        $"/{controller.ControllerName}/Description";
                    string template =
                        controller.ControllerType.Namespace.Replace('.', '/') +
                        "/[controller]/[action]";
                    Console.WriteLine(url);
                    // Console.WriteLine($"MvcNamespaceRoutingConvention.Apply( "+
                    //                   $"controller={controller.ControllerName}, template={template} )");
                    controller.Selectors[0].AttributeRouteModel = new AttributeRouteModel()
                    {                        
                        Template = template
                    };*/
                //}
            }            
        }
    }
}

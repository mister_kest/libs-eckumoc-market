﻿
using ApplicationDb;
using CoreApp.AppActive;
using CoreApp.AppAPI;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using PublicLibsAspNet.CoreApp;
using ApplicationCore.CoreDomain;
using ApplicationCore.Domain.Odbc;
using System;
using System.Linq;
using System.Diagnostics;
using System.Collections.Generic;
using ApplicationCore;
using Microsoft.AspNetCore.Http;

namespace CoreApp
{
    /// <summary>
    /// Подключение служб приложения
    /// </summary>
    public static class CoreExtensions
    {

        /// <summary>
        /// Конфигурация служб приложения
        /// </summary>
        /// <param name="services">коллекция сервисов</param>
        /// <param name="configuration">конфигурации приложения</param>
        /// <returns></returns>
        public static IServiceCollection AddCoreApp<TContext>( this IServiceCollection services, IConfiguration configuration, string connectionString  ) where TContext: DbContext
        {
            
            Console.WriteLine($"CoreAppExtensions.AddAddCoreApp() => {connectionString}");
            Debug.WriteLine($"CoreAppExtensions.AddAddCoreApp() => {connectionString}");
            
            services.AddHttpContextAccessor();
            

            //services.AddOdbc();
          
            services.AddSingleton<APIServices, ActiveServices>();
            services.AddSingleton<APIUsers,ActiveUsers>();
            services.AddSingleton<ActiveOptions>();
            services.AddSingleton<ActiveThread>();                        
            services.AddTransient<APIAuthorization, CoreAuthorization>();
            services.AddTransient<ApplicationCore.CRUD.CRUDProtocol>();
            services.AddTransient<CoreUser>();
             
            return services;
        }



        
        /// <summary>
        /// Конфигурация конвеера обработки запросов
        /// </summary>
        /// <param name="app">конвеер обработки запросов</param>
        /// <param name="configuration">конфигурация приложения</param>
        /// <returns></returns>
        public static IApplicationBuilder UseCoreApp( this IApplicationBuilder app, IConfiguration configuration, string connectionString)
        {
            // конфигурация доступа
            /*app.UseCanActivateComponent(
            options =>
            {
                options.LoginPagePath = "/Account/Login";
                options.SigninValdationRoutes.Add("/User");
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    foreach (var role in db.Roles)
                    {
                        List<string> routes = new List<string>() {
                            $"/{role.Code}Face"
                        };
                        options.RoleValidationRoutes[role.Name] = routes;
                    }
                    foreach (var user in (from u in db.Users where u.IsActive==true || u.SecretKey != null select u).ToList())
                    {
                        user.SecretKey = null;
                        user.IsActive = false;
                        db.SaveChanges();
                    }
                }
            });*/
            //app.UseOdbcForSqlServer(ApplicationDbContext.DefaultConnectionString);
            return app;
        }
    }
}

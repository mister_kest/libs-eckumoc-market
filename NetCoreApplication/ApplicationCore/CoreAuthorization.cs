﻿using ApplicationCore.Utils;

using ApplicationDb;
using ApplicationDb.Entities;

using CoreApp.AppActive;
using CoreApp.AppAPI;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace CoreApp
{
    /// <summary>
    /// Служба транспортного уровня, выполняет авторизацию, регистрацию и идентификацияю пользователей
    /// </summary>
    public class CoreAuthorization: APIAuthorization
    {
        private readonly ILogger<CoreAuthorization> _logger;
        private readonly APIUsers _users;        
        private readonly ApplicationDbContext _db;
        private readonly ActiveOptions _options;
        private readonly IHttpContextAccessor _accessor;

        public CoreAuthorization(
                ILogger<CoreAuthorization> logger,
                IHttpContextAccessor accessor,
                ActiveThread thread,
                ActiveOptions options,
                APIUsers users,                
                ApplicationDbContext db ) {
            _logger = logger;
            _users = users;            
            _db = db;
            _options = options;
            _accessor = accessor;
            _logger.LogInformation("Create");
        }


        /// <summary>
        /// Метод выхода пользователя из сеанса
        /// </summary>
        public void Signout()
        {
            User user = Verify();
            _users.Remove(user.SecretKey);
            _db.SaveChanges();
        }


        /// <summary>
        /// Авторизация пользователя в системе
        /// </summary>
        /// <param name="Email">электронный адрес</param>
        /// <param name="Password">пароль</param>
        public void Signin( string Email, string Password )
        {
            User current = (from user 
                            in _db.Users
                                    .Include(a=>a.Account)
                                    .Include(a => a.Settings)
                                    .Include(a => a.Person)
                                    .Include(a => a.Role)
                                    .Include(a => a.UserGroups)
                            where user.Account.Email == Email select user).FirstOrDefault();            

            if (current == null || GetHashSha256(Password) != current.Account.Hash)
            {
                throw new Exception("Учетные данные не зарегистрированы");
            }
            else
            {
                current.Groups = (from grs
                                  in _db.Groups
                                  where (from ug in current.UserGroups select ug.GroupID).Contains(grs.ID)
                                  select grs).ToList();
                string currentKey = _users.Find(current);
                current.SecretKey = currentKey!=null? currentKey: _users.Put(current);
                current.IsActive = true;
                long timestamp = Timing.GetTodayBeginTime();
                _db.SaveChanges();
                _db.LoginFacts.Add(new LoginFact() { 
                    Created=DateTime.Now,
                    User=current,
                    Calendar=(from cal in _db.Calendars where cal.Timestamp== timestamp select cal).First()
                });
                _accessor.HttpContext.Response.Cookies.Append(_options.UserCookie, current.SecretKey, new CookieOptions() { 
                    IsEssential = true,
                    Secure = true
                });
            }
        }


        /// <summary>
        /// Идентификация пользователя в системе
        /// </summary>
        /// <returns>ссылка на обьект сеанса</returns>
        public User Verify()
        {
            string SecretKey = null;
            _accessor.HttpContext.Request.Cookies.TryGetValue(_options.UserCookie, out SecretKey);
            if(SecretKey == null)
            {
                return null;
            }
            else
            {
                User user = _users.Take(SecretKey);
                if (user == null)
                {
                    _accessor.HttpContext.Response.Cookies.Delete(_options.UserCookie);
                }
                else
                {
                    _accessor.HttpContext.Response.Cookies.Append(_options.UserCookie, SecretKey, new CookieOptions()
                    {
                        IsEssential = true,
                        Secure = true
                    });
                }
                return user;
            }
        }


        /// <summary>
        /// Регистрация пользователя в системе
        /// </summary>
        /// <param name="Email">электронный адрес</param>
        /// <param name="Password">пароль</param>
        /// <param name="Confirmation">подтверждение</param>
        public void Signup(string Email, string Password, string Confirmation, string SurName, string FirstName, string LastName, DateTime Birthday, string Tel)
        {
            Account account = new Account()
            {
                Email = Email,
                Hash = GetHashSha256(Password)
            };
            Person person = new Person()
            {
                SurName = SurName,
                FirstName = FirstName,
                LastName = LastName,
                Birthday = Birthday,
                Tel = Tel
            };
            Settings settings = new Settings();
            Role role = (from r in _db.Roles where r.Name == _options.PublicRole select r).Single();
            Group group = (from g in _db.Groups where g.Name == _options.PublicGroup select g).Single();
            User user = new User()
            {
                Person = person,
                Account = account,
                Settings = settings,
                Role = role,                
                LastActive = GetTimestamp(),
                LoginCount = 0,
                IsActive = false                
            };

            _db.Persons.Add(person);
            _db.Accounts.Add(account);
            _db.Settings.Add(settings);
            _db.Users.Add(user);
            _db.SaveChanges();
        }


        /// <summary>
        /// Получечние текущего времени в милисекундах
        /// </summary>
        /// <returns></returns>
        private long GetTimestamp()
        {
            return (long)(((DateTime.Now - new DateTime(1970, 1, 1, 0, 0, 0))).TotalMilliseconds);
        }


        /// <summary>
        /// Проверка регистрации пользователя с заданным электронным адресом
        /// </summary>
        /// <param name="Email">электронный адрес</param>
        /// <returns></returns>
        public bool Has(string Email)
        {
            User current = (from user
                               in _db.Users.Include(a=>a.Account)
                               where user.Account.Email == Email select user).FirstOrDefault();
            if (current != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// Применение функции хэширования к последовательности символов
        /// </summary>
        /// <param name="text">последовательность</param>
        /// <returns>хэш</returns>
        private string GetHashSha256(string text)
        {
            byte[] bytes = Encoding.Unicode.GetBytes(text);
            SHA256Managed hashstring = new SHA256Managed();
            byte[] hash = hashstring.ComputeHash(bytes);
            string hashString = string.Empty;
            foreach (byte x in hash)
            {
                hashString += String.Format("{0:x2}", x);
            }
            return hashString;
        }


        /// <summary>
        /// Проверка выполнения процедуры авторизации
        /// </summary>
        /// <returns></returns>
        public bool IsSignin()
        {
            string SecretKey = null;
            _accessor.HttpContext.Request.Cookies.TryGetValue(_options.UserCookie, out SecretKey);
            if (SecretKey == null)
            {
                return false;
            }
            else
            {
                User user = _users.Take(SecretKey);
                if (user == null)
                {
                    _accessor.HttpContext.Response.Cookies.Delete(_options.UserCookie);
                    return false;
                }
                else
                {
                    _accessor.HttpContext.Response.Cookies.Append(_options.UserCookie, SecretKey, new CookieOptions()
                    {
                        IsEssential = true,
                        Secure = true
                    });
                    return true;
                }               
            }

        }


        /// <summary>
        /// Метод получения атрибутов сеанса
        /// </summary>
        /// <returns></returns>
        public ConcurrentDictionary<string, object> Session()
        {
            string SecretKey = null;
            _accessor.HttpContext.Request.Cookies.TryGetValue(_options.UserCookie, out SecretKey);
            if (SecretKey == null)
            {
                return null;
            }
            else
            {
                ConcurrentDictionary<string, object> session = _users.GetSession(SecretKey);
                if (session == null)
                {
                    _accessor.HttpContext.Response.Cookies.Delete(_options.UserCookie);
                }
                else
                {
                    _accessor.HttpContext.Response.Cookies.Append(_options.UserCookie, SecretKey, new CookieOptions()
                    {
                        IsEssential = true,
                        Secure = true
                    });
                }
                return session;
            }
        }



        /// <summary>
        /// Проверка принадлежности пользователя к роли
        /// </summary>
        /// <param name="roleName">наменование роли</param>
        /// <returns></returns>
        public bool InRole(string roleName)
        {
            User user = this.Verify();
            if (user == null)
            {
                return false;
            }
            else
            {
                return user.Role.Name == roleName;
            }
        }
    }
}

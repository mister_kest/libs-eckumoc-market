﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Domain.Reflection
{
    public class ReflectionComponent
    {     
        private readonly ILogger<ReflectionComponent> _logger;
        private readonly RequestDelegate _next;  

        public ReflectionComponent(
            ILogger<ReflectionComponent> logger,
            RequestDelegate next )
        {           
            _logger = logger;
            _next = next;
            _logger.LogInformation("CREATED");
        }

        public async Task Invoke(
            HttpContext httpContext)
        {
            _logger.LogInformation("Invoke");
            
            await _next.Invoke(httpContext);
        }        
    }
}

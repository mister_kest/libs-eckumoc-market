﻿using SpbPublicLibsCore.Testing;

using System;
using System.Collections.Generic;
using System.Text;

namespace SpbPublicLibsUnit.DomainUnit
{
    public class TestSession: TestingUnit
    {
        protected override void onTest()
        {
            canCreateSession();
            canFindSession();
            canDisposeSession();
        }

        private void canCreateSession()
        {
            
        }

        private void canFindSession()
        {
            
        }

        private void canDisposeSession()
        {
            
        }
    }
}

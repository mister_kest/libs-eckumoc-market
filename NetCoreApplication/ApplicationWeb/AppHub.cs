﻿using System;
using System.Threading.Tasks;

using CoreApp.AppAPI;

using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ApplicationCore.Domain;
using ApplicationCore.Messaging;

using SpbPublicLibsUnit.Encoder;

namespace ApplicationCore
{
    public class AppHub : Hub
    {   
        protected readonly ILogger<AppHub> _logger;
       
        protected readonly APIAuthorization _authorization;
        
        public AppHub(
            ILogger<AppHub> logger,     
            APIAuthorization authorization)
        {
            _logger = logger;     
            _authorization = authorization;
            _logger.LogInformation("Create");      
        }


        /// <summary>
        /// Process executing request operation
        /// </summary>
        /// <param name="json">json message with execution param</param>
        /// <returns> nothing </returns>
        public async Task SecureRequest(string encoded)
        {
            CharacterEncoder encoder = new CharacterEncoder();
            string message = encoder.Decode(encoded);
            await this.Request(message); 
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="response"></param>
        private void onMessage(RequestMessage request, ResponseMessage response)
        {
            //this._app.OnMessage( request, response );
        }

        /// <summary>
        /// Предназначен для тестирования
        /// </summary>
        /// <param name="message"> request message json</param>
        /// <returns></returns>
        public async Task CatchException(string message)
        {
            RequestMessage request = new RequestMessage();
            ResponseMessage response = new ResponseMessage();
            try
            {
                throw new Exception(message);
            }
            catch (Exception ex)
            {
                SendErrorMessage(response, ex);
            }
            finally
            {
                string result = JObject.FromObject(response).ToString();
                await Clients.Caller.SendAsync("Response", result);
            }
        }


        
        /// <summary>
        /// Process executing request operation
        /// </summary>
        /// <param name="json">json message with execution param</param>
        /// <returns> nothing </returns>
        public async Task Request(string json)
        {
            string requestText = json;// _haffman.Decode(json);
            RequestMessage request = new RequestMessage();
            ResponseMessage response = new ResponseMessage();
            try
            {
                request = JsonConvert.DeserializeObject<RequestMessage>(requestText);
                _logger.LogInformation("Request => ");
                _logger.LogInformation(JObject.FromObject(request).ToString());
                response.request = request;
                this.onMessage(request, response);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                this._logger.LogError(ex.ToString());
                SendErrorMessage(response, ex);
            }
            finally
            {
                _logger.LogInformation("Response => ");
                _logger.LogInformation(JObject.FromObject(response).ToString());

                string result = JsonConvert.SerializeObject(response);
                await Clients.Caller.SendAsync("Response", result);                
            }
        }


        /// <summary>
        /// Send message with info about catcher error.
        /// </summary>
        /// <param name="response">output response message</param>
        /// <param name="ex"> catched exception</param>
        private static void SendErrorMessage(ResponseMessage response, Exception ex)
        {
            while (ex.InnerException!=null)
            {
                ex = ex.InnerException;
            }
            response.message = "" + ex.Message;
            response.stack = ex.ToString();
            response.status = "failed";
        }
    }
}

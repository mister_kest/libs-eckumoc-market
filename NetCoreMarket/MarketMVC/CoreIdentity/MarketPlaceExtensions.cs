﻿using Microsoft.Extensions.DependencyInjection;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MvcMarketPlace.Core
{
    public static class MarketPlaceExtensions
    {
        public static IServiceCollection AddMarketPlace( this IServiceCollection services )
        {
            services.AddHttpContextAccessor();
            services.AddTransient<IMarketPlaceAuthentication, MarketPlaceAuthenticationService>();
            return services;
        }
    }
}

﻿using Newtonsoft.Json.Linq;

using ApplicationCore.Domain.Odbc.DataSource;
using ApplicationCore.Domain.Odbc.Metadata;
using ApplicationCore.Utils;

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace ApplicationCore.Domain.Odbc.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class DatabaseManager
    {
        private static ConcurrentDictionary<string, DatabaseManager> DATASOURCES = new ConcurrentDictionary<string, DatabaseManager>();
        private static ConcurrentDictionary<string, DatabaseMetadata> METADATA = new ConcurrentDictionary<string, DatabaseMetadata>();
        private static ConcurrentDictionary<string, int> KEYWORDS = new ConcurrentDictionary<string, int>();
        private static ConcurrentDictionary<string, Dictionary<string, int>> STATISTICS = new ConcurrentDictionary<string, Dictionary<string, int>>();

        private static string SQL_SERVERL_ODBC_DRIVER = "Driver={SQL Server};";

        public Dictionary<string, object> fasade;

        internal void UpdateDatabase( DatabaseMetadata metadata )
        {
            foreach(var tableVar in metadata.Tables)
            {
                TableMetaData tableMetaData = tableVar.Value;
            }
        }

        

        public static void createSqlServerAdoDataSource(string name, string connectionString)
        {
            string odbcConnectionString = SQL_SERVERL_ODBC_DRIVER + connectionString.Replace(@"\\", @"\");
            if (DATASOURCES.ContainsKey(odbcConnectionString) == false)
            {
                SqlServerOdbcDataSource ds = new SqlServerOdbcDataSource(odbcConnectionString);
                DatabaseManager dbm = new DatabaseManager(ds);
                DATASOURCES[name] = dbm;
                dbm.discovery();
            }            
        }

        public DatabaseManager()
        {
            Init(this.ds = new MySqlOdbcDataSource());
        }

        public DatabaseManager(OdbcDataSource odbc)
        {
            
            this.Init(this.ds = odbc);

        }

        public DatabaseManager(string odbc)
        {
            
            this.Init(this.ds = new SqlServerOdbcDataSource(odbc));
        }


        public void Use( OdbcDataSource ds )
        {
            this.fasade = new Dictionary<string, object>();            
            this.Init(this.ds = ds);
        }

       



        public Dictionary<string, int> GetKeywords()
        {
            if (this.keywords == null || this.keywords.Count==0)
                 
                this.discovery();
            return this.keywords;
        }

        public void discovery()
        {
            Console.WriteLine("DatabaseManager.discovery()");
            Dictionary<string, int> keywords = new Dictionary<string, int>(KEYWORDS);
            Dictionary<string, Dictionary<string, int>> statistics = new Dictionary<string, Dictionary<string, int>>();
            if( KEYWORDS.Count>0 && STATISTICS.Count>0)
            {
                this.keywords = new Dictionary<string, int>(KEYWORDS);
                this.statistics = new Dictionary<string, Dictionary<string, int>>(STATISTICS);
                return;
            }
            try
            {
                foreach (var pair in this.fasade)
                {
                    string name = pair.Key;
                    TableMetaData metadata = ((TableManagerStatefull)pair.Value).GetMetadata();                                        
                    string pk = this.GetMetaData().Tables.ContainsKey(metadata.singlecount_name)?
                                this.GetMetaData().Tables[metadata.singlecount_name].getPrimaryKey():
                                this.GetMetaData().Tables[metadata.multicount_name].getPrimaryKey();
                    if (pk == null)
                    {
                        throw new Exception("Primary key udefined for table " + name);
                    }
                    List<string> textColumns = ((TableManagerStatefull)pair.Value).GetMetadata().GetTextColumns();
                    foreach (JObject record in ((TableManagerStatefull)pair.Value).dataRecords)
                    {
                        try
                        {
                            //Console.WriteLine(record);
                            int id = record[pk].Value<int>();
                            //int relevation = 0;
                            Dictionary<string, int> statisticsForThisRecord = new Dictionary<string, int>();
                            foreach (string column in textColumns)
                            {
                                if (record[column] != null)
                                {
                                    string textValue = record[column].Value<string>();
                                    if (String.IsNullOrEmpty(textValue)) continue;
                                    foreach (string word in textValue.Split(" "))
                                    {
                                        if (keywords.ContainsKey(word))
                                        {
                                            keywords[word]++;
                                        }
                                        else
                                        {
                                            keywords[word] = 1;
                                        }

                                        if (statisticsForThisRecord.ContainsKey(word))
                                        {
                                            statisticsForThisRecord[word]++;
                                        }
                                        else
                                        {
                                            statisticsForThisRecord[word] = 1;
                                        }
                                    }
                                }
                            }

                            statistics[name + "/" + id] = statisticsForThisRecord;
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex);
                            continue;
                        }


                    }

                }
            }
            catch (Exception ex)
            {
                keywords[ex.Message] = 500;
                Console.WriteLine(ex);
            }
            this.keywords = keywords;
            this.statistics = statistics;
        }

     


        /// <summary>
        /// Метод получения обьектов управления таблицами базы данных
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, object> GetFasade()
        {
            return this.fasade;
        }


        /// <summary>
        /// Метод получения обьекта управления таблицей базы данных
        /// </summary>
        /// <param name="name"> имя таблицы или сущности</param>
        /// <returns></returns>
        public TableManagerStatefull Get(string name)
        {
            if (this.fasade.ContainsKey(name))
            {
                
                return (TableManagerStatefull)this.fasade[name];
            }
            else
            {
                throw new Exception($"Имя таблицы: {name} задано неверно");
            }
        }

        public object GetAssociations(string key)
        {
            switch (key)
            {
                case "$list": return this.GetCommands();
                case "$metadata": return this.GetMetadata();
                case "$popular": return this.GetPopular();
                case "$latest": return this.GetLatest();
                case "$rating": return this.GetRating();
                case "$stats": return this.GetStats();
                case "$keywords": return this.GetKeywords();
                case "$errors": return this.GetErrors();
                default: return null;
            }
        }

        private object GetErrors()
        {
            throw new NotImplementedException();
        }

        private object GetStats()
        {
            throw new NotImplementedException();
        }

        private object GetRating()
        {
            throw new NotImplementedException();
        }

        private object GetLatest()
        {
            throw new NotImplementedException();
        }

        private object GetPopular()
        {
            throw new NotImplementedException();
        }

        private object GetCommands()
        {
            return new string[] { "$stats", "$keywords", "$errors", "$rating", "$latest", "$popular", "$metadata", "$list" };
        }

        OdbcDataSource GetDataSource()
        {
            return this.ds;
        }


        public void DumpDatabase()
        {

        }

        public DatabaseMetadata GetMetadata()
        {
            if( METADATA.ContainsKey(this.ds.connectionString)==false)
            {
                METADATA[this.ds.connectionString] = this.ds.GetDatabaseMetadata();
            }
            return METADATA[this.ds.connectionString];           
        }


        /*private DatabaseSnapshot CreateDump()
        {
            DatabaseSnapshot dump = new DatabaseSnapshot();
            DatabaseMetadata dbm = GetDataSource().GetDatabaseMetadata();
            dump.metadata = dbm;
            foreach (string tm in dbm.Tables.Keys)
            {
                dump.datasets[tm] = GetDataSource().Execute("select * from " + tm);
            }
            return dump;
        }*/
        /*
        public DataModel( ) : base()
        {
            Database.EnsureCreated();
        }

        public DataModel( DbContextOptions<DataModel> options ) : base( options )
        {
            Database.EnsureCreated();
        }


        protected override void OnConfiguring( DbContextOptionsBuilder optionsBuilder )
        {"jdbc:postgresql://localhost:4200/postgres", "mister_kest", "Kest1423"
            //optionsBuilder.UseSqlServer(@"Server=511-5A;Database=webapp;Trusted_Connection=True;");
            //optionsBuilder.UseInMemoryDatabase();// UseMySql("server=localhost;UserId=root;Password=password;database=usersdb3;");
            //optionsBuilder.UseMySQL( "server=localhost;database=library;user=root;password=root" );
        }*/

        //new JdbcConnector("jdbc:postgresql://localhost:4200/postgres","mister_kest", "Kest1423");

        private OdbcDataSource ds;
        string datasource; string username; string password;
        Dictionary<string, Dictionary<string, int>> statistics = new Dictionary<string, Dictionary<string, int>>();

        Dictionary<string, int> keywords = new Dictionary<string, int>();

        

        public DatabaseManager(string datasource, string username, string password) : base()
        {
            this.datasource = datasource;
            this.username = username;
            this.password = password;
            this.ds = new OdbcDataSource(datasource, username, password);
            foreach (var prop in GetMetaData().Tables)
            {
                TableManager manager = new TableManager(prop.Key, GetDataSource(), prop.Value);
                fasade[prop.Key] = new TableManagerStatefull(this, manager);
            }

        }

        

        public void Init(OdbcDataSource ds)
        {
            this.ds = ds;
            this.fasade = new Dictionary<string, object>();
            foreach (var prop in GetMetaData().Tables)
            {
                TableManager manager = new TableManager(prop.Key, GetDataSource(), prop.Value);
                TableManagerStatefull statefull = new TableManagerStatefull(this, manager);
                fasade[prop.Key] = statefull;
                fasade[prop.Value.singlecount_name.ToUpper()] = statefull;
                fasade[prop.Value.multicount_name.ToUpper()] = statefull;
                

            }
        }

        public DatabaseMetadata GetMetaData()
        {
            return GetDataSource().GetDatabaseMetadata();
        }

        public Dictionary<string, object> ValidateDatabaseMetadata(Dictionary<string, object> tables)
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            foreach (var p in tables)
            {
                //TODO:
            }
            return result;
        }

        public object Execute(string sql)
        {
            return GetDataSource().Execute(sql);
        }





        public object RequestData(string beginDate, string endDate, Int64 granularity, JArray indicators, JArray locations)
        {
            if (indicators == null)
            {
                throw new ArgumentNullException("indicators argument references to null pointer");
            }
            if (beginDate == null)
            {
                throw new ArgumentNullException("beginDate argument references to null pointer");
            }
            if (endDate == null)
            {
                throw new ArgumentNullException("endDate argument references to null pointer");
            }
            if (locations == null)
            {
                throw new ArgumentNullException("locations argument references to null pointer");
            }
            string sindicators = indicators.ToString().Replace("{", "").Replace("}", "").Replace("[", "").Replace("]", "");
            string slocations = locations.ToString().Replace("{", "").Replace("}", "").Replace("[", "").Replace("]", "");
            string sqlQuery = "select * from datainput where indicator_id in (" + sindicators + ") and subject_id in(" + slocations + ") and begin_date between '" + beginDate + "' and '" + endDate + "' and granularity_id = " + granularity + " order by begin_date,subject_id,indicator_id";
            return GetDataSource().Execute(sqlQuery);
        }

        
    }
}

 
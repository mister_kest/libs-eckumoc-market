﻿using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationDb
{
    public abstract class AbstractDbInitiallizer<TDbContext> where TDbContext: DbContext
    {
        public abstract TDbContext CreateDbContext();

        public void DoInitiallize()
        {
            Console.WriteLine("Инициаллизация минимального набора данных");
            using (TDbContext db = CreateDbContext())
            {
                OnInitiallize(db);
            }
        }


        protected abstract void OnInitiallize(TDbContext db);
    }
}

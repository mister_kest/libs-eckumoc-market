﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ApplicationCore.Domain.Odbc.Metadata;
using ApplicationCore.Utils;
using System;
using System.Text.RegularExpressions;

namespace ApplicationCore.Converter.Generators
{
    public class JsonModelGenerator
    {
        /// <summary>
        /// Метод получения структуы данных из JSON-обьекта
        /// </summary>
        /// <param name="json"> json </param>
        /// <returns> структура данных </returns>
        public TableMetaData GenerateModelFromJson(string name, string json)
        {
            JObject jobject = JsonConvert.DeserializeObject<JObject>(json);
            return this.GenerateCapitalModelFromJson(name, jobject);
        }


        /// <summary>
        /// Метод получения структуы данных из JSON-обьекта
        /// </summary>
        /// <param name="json"> json </param>
        /// <returns> структура данных </returns>
        public TableMetaData GenerateCapitalModelFromJson(string name, JObject jobject)
        {
            Regex intRegex = new Regex(@"[\d]");
            Regex floatRegex = new Regex(@"^-?[0-9][0-9,\.]+$");
            TableMetaData metadata = new TableMetaData();
            metadata.name = name;
            foreach (var p in jobject)
            {    
                if(p.Key.ToLower() == "id")
                {
                    metadata.pk = Naming.ToCapitalStyle(p.Key);
                }
                metadata.columns[Naming.ToCapitalStyle(p.Key)] = new ColumnMetaData()
                {
                    name = Naming.ToCapitalStyle(p.Key),
                    nullable = true,
                    type = 
                        intRegex.IsMatch(p.Value.ToString()) ? "int" :
                        intRegex.IsMatch(p.Value.ToString()) ? "float" :
                        "string"
                };
            }
             
            return metadata;
        }




        /// <summary>
        /// Метод получения типа данных SQL совместимого с типом C#
        /// </summary>
        /// <param name="type"> тип c# </param>
        /// <returns> Тип SQL </returns>
        private string GetSqlDataType( Type type )
        {
            switch (type.Name.ToLower())
            {
                case "jarray":
                    return "List<object>";
                case "jvalue":
                    return "object";
                case "datetime":
                    return "datetime";
                case "int":
                case "int32":
                case "int64":
                case "long":
                case "double":
                case "float":
                case "decimal":
                case "integer":
                case "number":
                case "numeric":
                    return "int";
                case "string":
                    return "varchar";
                default:
                    throw new Exception($"Не совместимый тип данных: {type.Name}");
            }
        }
    }
}

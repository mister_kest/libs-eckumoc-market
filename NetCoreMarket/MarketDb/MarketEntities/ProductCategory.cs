﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace MvcMarketPlace.Data.Entities
{
    public class ProductCategory
    {


        public int ID { get; set; }

        [DisplayName("Наименование")]
        public string Name { get; set; }
        public int? ParentCategoryID { get; set; }

        [DisplayName("Корневой каталог")]
        public virtual ProductCategory ParentCategory { get; set; }
    }
}

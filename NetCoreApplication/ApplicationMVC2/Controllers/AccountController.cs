﻿using System;
using System.Threading.Tasks;
using CoreApp.AppAPI;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Mvc.Models;

namespace Mvc.Controllers
{
    public class AccountController : Controller
    {

        private readonly ILogger<AccountController> _logger;
        private readonly APIAuthorization _authorization;

        public AccountController(
                ILogger<AccountController> logger,
                APIAuthorization authorization)
        {
            _logger = logger;
            _authorization = authorization;
            _logger.LogInformation("Create");
        }


        /// <summary>
        /// Переход к представлению авторизации
        /// </summary>
        /// <returns></returns>
        public IActionResult Logout()
        {
            return View("Index","Home");
        }



        /// <summary>
        /// Переход к представлению авторизации
        /// </summary>
        /// <returns></returns>
        public IActionResult Login()
        {
            return View(new LoginViewModel());
        }


        /// <summary>
        /// Выполнение запроса авторизации пользователя
        /// </summary>
        /// <param name="model">модель представления авторизации</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login([Bind("Email,Password")] LoginViewModel model)
        {
            foreach (var state in ModelState)
            {
                switch (state.Key)
                {
                    case "Email": model.EmailValidationState = GetValidationState(state.Key); break;
                    case "Password": model.PasswordValidationState = GetValidationState(state.Key); break;
                }
            }
            if (ModelState.IsValid)
            {
                try
                {
                    _authorization.Signin(model.Email, model.Password);
                }
                catch (Exception ex)
                {
                    model.ErrorMessage = ex.Message;
                    return View(model);
                }
                return RedirectToAction("Index","Home");
            }
            return View(model);
        }


        /// <summary>
        /// Выполнение перехода на страницу регистрации ползователя
        /// </summary>
        /// <returns></returns>
        public IActionResult Registration()
        {
            return View(new RegistrationViewModel());
        }


        /// <summary>
        /// Выполнение запроса регистрации пользователя
        /// </summary>
        /// <param name="model">модель представления регистрации</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Registration([Bind("Email,Password,Confirmation,FirstName,LastName,SurName,Birthday,Tel")] RegistrationViewModel model)
        {
            if (model.Birthday.Year < 1920 || model.Birthday.Year>(DateTime.Now.Year-2))
            {
                ModelState.AddModelError("Birthday", "Дата рождения указана неверно");
                return View(model);
            }

            if (model.Confirmation != model.Password)
            {
                ModelState.AddModelError("Confirmation", "Подтверждение пароля отчается от самого пароля");
                return View(model);
            }

            if (_authorization.Has(model.Email) == true)
            {
                ModelState.AddModelError("Email", "Данный электронный адрес уже зарегистрирован");
                return View(model);
            }              
            if (!ModelState.IsValid)
            {
                foreach (var state in ModelState)
                {
                    switch (state.Key)
                    {
                        case "SurName":         model.SurNameValidationState = GetValidationState(state.Key); break;
                        case "Last":            model.LastNameValidationState = GetValidationState(state.Key); break;
                        case "First":           model.FirstNameValidationState = GetValidationState(state.Key); break;
                        case "Birthday":        model.BirthdayValidationState = GetValidationState(state.Key); break;
                        case "Email":           model.EmailValidationState = GetValidationState(state.Key); break;
                        case "Password":        model.PasswordValidationState = GetValidationState(state.Key); break;
                        case "Confirmation":    model.ConfirmationValidationState = GetValidationState(state.Key); break;
                        case "Tel":             model.TelValidationState = GetValidationState(state.Key); break;
                    }
                }
                return View(model);
            }
            try
            {
                _authorization.Signup(
                    model.Email, model.Password, model.Confirmation,
                    model.SurName, model.FirstName, model.LastName, model.Birthday, model.Tel);
                return RedirectToAction("Index","Home");
            }
            catch (Exception ex)
            {
                model.ErrorMessage = ex.Message;
                return View(model);
            }
        }


        /// <summary>
        /// Получение строки состояния элемента ввода для заданного свойства
        /// </summary>
        /// <param name="property">свойство</param>
        /// <returns></returns>
        private string GetValidationState(string property)
        {
            string state = ModelState[property] == null ? "valid" :
                ModelState[property].ValidationState == Microsoft.AspNetCore.Mvc.ModelBinding.ModelValidationState.Valid == true ? "valid" : "invalid";
            return state;
        }
    }
}

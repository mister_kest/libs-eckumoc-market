using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

using miac.ServerApp;

using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;

using ApplicationCore.Domain.Odbc.DataSource;

namespace Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UploadController : Controller
    {

        private readonly ResourceManager _resource;
        public UploadController() : base()
        {
       
            this._resource = new ResourceManager();
        }

        
        /**
         * �������� ����� */
        public IActionResult Index()
        {
            return new PhysicalFileResult(Path.Combine(ResourceManager.GetAppDataDirectory(), "index.html"), "text/html");
        }


        // GET: api/Upload/ConvertExcelToJson
        [HttpGet("[action]")]
        public object Json( )
        {
            Dictionary<string, object> response = new Dictionary<string, object>();
            long? length = this.HttpContext.Request.ContentLength;
            if ( length != null )
            {
                try
                {
                    byte[] data = new byte[( long ) length];
                    this.HttpContext.Request.Body.Read( data, 0, ( int ) length );
                    string mime = Request.ContentType;
                    string filename = ResourceManager.GetAppData() + "/upload/temp.xlsx";
                    ResourceManager.WriteFile(filename, data );                                                            
                    //response["response"] = JObject.FromObject(ExcelOleDataSource.ReadFile(filename));
                    response["status"] = "success";
                }
                catch(Exception e )
                {
                    response["status"] = "failed";
                    Console.WriteLine( e );
                }
            }
            return response;          
        }

        [HttpGet()]
        public object List()
        {
            return new
            {
                resources = ResourceManager.ResourceList()
            };
        }


        [HttpGet()]
        public void Download(string id)
        {
            System.Diagnostics.Debug.WriteLine(id);
            Response.ContentType = "image/png";
            byte[] data = ResourceManager.Download(id);
            Response.Body.Write(data,0,data.Length);            
        }


        // uri: api/Upload/Upload?name=icon1
        [HttpPost("[action]")]
        public Dictionary<string, object> Upload(string name)
        {
            Dictionary<string, object> response = new Dictionary<string, object>();
            try
            {
                long? length = this.HttpContext.Request.ContentLength;
                if ( length != null )
                {
                    byte[] data = new byte[( long ) length];
                    this.HttpContext.Request.Body.Read( data, 0, ( int ) length );
                    string mime = Request.ContentType;

                    ResourceManager.Upload( name, mime, data );
                }
                response["status"] = "success";
            }
            catch(Exception ex )
            {
                response["text"] = ex.Message;
                response["status"] = "failed";
            }
            return response;
        }
 
    }
}

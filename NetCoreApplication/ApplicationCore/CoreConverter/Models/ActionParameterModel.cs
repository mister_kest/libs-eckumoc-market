﻿namespace ApplicationCore.Converter.Models
{
    /// <summary>
    /// Модель параметра вызова метода или процедуры или функции.
    /// ПО этой модели приложение-клиент создаёт поле для ввода информации
    /// на форме выполнения операции.
    /// </summary>
    public class ActionParameterModel
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public bool IsOptional { get; set; }       
    }
}
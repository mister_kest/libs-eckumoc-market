﻿using MvcMarketPlace.Data.Entities;
using SpbPublicLibsЬMVC.Mvc.API;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mvc.API
{
    public class TreeNode: MvcComponent, ITreeNode<ProductCategory>
    {
        private readonly ProductCategory _item;
        private readonly Dictionary<string, ITreeNode<ProductCategory>> _children;
        private ITreeNode<ProductCategory> _parent;
        private string _name;
        public bool Expanded = true;
        public bool Editable = false;

        public TreeNode(TreeNode parent, ProductCategory item)
        {
            _item = item;
            _children = new Dictionary<string, ITreeNode<ProductCategory>>();
        }

       
        public ProductCategory GetItem()
        {
            return _item;
        }


        public void SetParent(ITreeNode<ProductCategory> parent)
        {
            
            if ( _parent != null && _parent.GetChildren().ContainsValue(this))
            {                
                _parent.GetChildren().Remove(this.GetName());
            }
            _parent = parent;
            _parent.GetChildren()[GetName()] = this;
        }

        public void toggleExpanded()
        {

            Expanded = Expanded == true ? false : true;
        }


        public void toggleSelected()
        {
            Selected = Selected == true ? false : true;
        }

        



        public string GetName()
        {
            return _item.Name;
        }


        public Dictionary<string, ITreeNode<ProductCategory>> GetChildren()
        {
            return _children;
        }
    }
}

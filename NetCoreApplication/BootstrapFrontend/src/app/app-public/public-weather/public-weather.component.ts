import { PublicWeatherService } from './public-weather.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector:   'public-weather',
  templateUrl: './public-weather.component.html'
})
export class PublicWeatherComponent implements OnInit {

  ready = false;
  weather: any = {};

  constructor( private service: PublicWeatherService ) { }

  ngOnInit() {
    const ctrl = this;
    navigator.geolocation.getCurrentPosition((positionResponse)=>{
      console.log(positionResponse);
      const response: any = ctrl.service.getWeatherForecast(
        positionResponse.coords.longitude,
        positionResponse.coords.latitude,
        positionResponse.timestamp
      );

      if( response ){
        response.subscribe(ctrl.getWeatherForecastCallback());
      }

    });
  }

  getWeatherForecastCallback(){
    const ctrl = this;
    return function( weather  ){
      console.log( weather );
      ctrl.weather = weather;
      ctrl.ready = true;
    }
  }

}

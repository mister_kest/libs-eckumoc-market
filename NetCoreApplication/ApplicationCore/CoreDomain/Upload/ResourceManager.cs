﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using ApplicationCore.Domain.Odbc.Controllers;
using ApplicationCore.Domain.Odbc.DataSource;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace miac.ServerApp
{
    public class ResourceManager
    {
        private static Dictionary<string, object> DEPS;
        private static string PATH_SEPARATOR = null;


        /**
         * Запись файла в каталог AppData
         */
        public static void WriteFile( string filename, byte[] data )
        {
            System.IO.File.WriteAllBytes( GetAppDataDirectory() + "/" + filename, data );
        }

        /**
         * Получение карты ресурсов типа изображение
         */
        public static Dictionary<string,string> GetFileIcons( )
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            foreach ( string filename in GetAppDataResources( "resources\\images\\icons" ) )
            {
                result[filename] = "api/App?path=resource.ReadFile&pars={path:\"" + filename + "\"}";
            }
            return result;
        }

        /**
         * Получение карты ресурсов типа изображение
         */
        public static Dictionary<string, string> GetFileIconsFromDir( string dir)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            foreach ( string filename in GetAppDataResources( dir ) )
            {
                result[filename] = "api/App?path=resource.ReadFile&pars={path:\"" + filename + "\"}";
            }
            return result;
        }


        /**
         * возвращает файловый разделитель 
         */
        public static string GetPathSeparator( )
        {
            if( PATH_SEPARATOR ==null )
            {
                string path = GetRootDirectory();
                int i0 = path.LastIndexOf( "/" );
                int i1 = path.LastIndexOf( "\\" );
                if ( i0 > i1 )
                {
                    PATH_SEPARATOR = "/";
                }
                else if ( i1 > i0 )
                {
                    PATH_SEPARATOR = "\\";
                }
                else
                {
                    throw new Exception("ResourceManager: can not compute file path separator");
                }
            }
            return PATH_SEPARATOR;
        }

        /**
         * Получение всех ресурсов каталога AppData
         */
        public static Dictionary<string, object> GetAppData( )
        {
            return GetAppDataForDirectory(GetRootDirectory());
        }

        /**
         * Получение всех ресурсов каталога AppData
         */
        public static Dictionary<string, object> GetAppDataForDirectory( string path )
        {
            Dictionary<string, object> resources = new Dictionary<string, object>();
            foreach( string dir in System.IO.Directory.GetDirectories( path ) )
            {
                string name = dir.Substring( dir.LastIndexOf( GetPathSeparator() ) + 1 );
                resources[name] = GetAppDataForDirectory( dir );
            }
            foreach ( string file in System.IO.Directory.GetFiles( path ) )
            {
                string name = file.Substring( file.LastIndexOf( GetPathSeparator() ) + 1 );
                resources[name] = System.IO.File.ReadAllBytes( file );
            }
            return resources;
        }

     


        /**
         * возвращает абсолютный путь к корневой директории проекта
         */
        private static string GetRootDirectory( )
        {
            return System.IO.Directory.GetCurrentDirectory();
        }

        /**
         * возвращает абсолютный путь директории статических ресурсов проекта
         */
        public static string GetAppDataDirectory( )
        {
            return System.IO.Directory.GetCurrentDirectory() + @"\AppData";
        }

        /**
         * Возвращает список файлов из заданного подкаталога AppData
         */
        protected static string[] GetAppDataResources( string dir )
        {
            List<string> files = new List<string>( System.IO.Directory.GetFiles( GetAppDataDirectory() + @"\" + dir ) );
            files.Sort();
            return files.ToArray();
        }

        /**
         * Возвращает ресурсы для клиентского приложения
         */
        public static Dictionary<string, object> GetClientAppDeps( )
        {
            if ( DEPS == null )
            {
                DEPS = new Dictionary<string, object>();
            
                foreach ( string file in GetAppDataResources( "client" ) )
                {
                    string data = System.IO.File.ReadAllText( file ); ;   
                    DEPS[file] = data;
                }
            }
            return DEPS;
        }

        /**
         * получение списка ресурсов
         */
        public static JArray ResourceList( )
        {
            return GetDataSource().Execute("select resource_id, resource_name, mime_type from resources");
        }

        /**
         * выгрузка бинарных данных из хранилище
         */
        public static byte[] Download(string id)
        {
            return GetDataSource().ReadBlob("select resource_data from resources where resource_id=" + id);
        }

        private static OdbcDataSource GetDataSource()
        {
            return new MySqlOdbcDataSource();
        }

        /**
         * загрузка бинарных данных в хранилище
         */
        public static void Upload(string name, string mime, byte[] data)
        {
            GetDataSource().InsertBlob("insert into resources (resource_name,mime_type,resource_data) values (\"" + name + "\",\"" + mime + "\",?)", "@bin_data", data);
        }


        /**
         * загрузка бинарных данных в хранилище
         */
        public static void UploadBinaryString(string name, string type, string data)
        {
            Dictionary<string, object> values = new Dictionary<string, object>();
            values["resource_name"] = name;
            values["mime_type"] = type;
            values["resource_data"] = data;

            //TableManagerStatefull fasade = (TableManagerStatefull)(((DatabaseManager)(GetRestfullModel()["database"])).fasade["resources"]);
            //fasade.Create(values);
        }


        private static void GetRestfullModel()
        {
            return;
        }


        /**
         * загрузка бинарных данных в хранилище
         */
        public static void UploadResource(string name, string type, string data)
        {
            byte[] bytes = new byte[data.Length];
            for (int i = 0; i < data.Length; i++)
            {
                bytes[i] = (byte)data[i];
            }
            ResourceManager.Upload(name, type, bytes);
        }

    }
}

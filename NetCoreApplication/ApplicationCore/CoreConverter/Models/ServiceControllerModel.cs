﻿using System.Collections.Generic;

namespace ApplicationCore.Converter.Models
{
    public class ServiceControllerModel
    {
        public string Name { get; set; }
        public string Path { get; set; }
        public Dictionary<string, ControllerActionModel> Actions { get; set; }


        public string GetAnnotationForService()
        {
            return "@Injectable({ providedIn: 'root' })\n";
        }


        public string GetImportsForService()
        {
            return
                "import { Observable } from 'rxjs';\n" +
                "import { Injectable } from '@angular/core';\n" +
                "import { HttpClient } from '@angular/common/http';\n\n";
        }
    }
}

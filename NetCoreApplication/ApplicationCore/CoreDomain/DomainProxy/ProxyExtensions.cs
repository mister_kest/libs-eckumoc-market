﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;

using ApplicationCore.Messaging;

using System;

namespace ApplicationCore.Domain.Proxy
{
    /// <summary>
    /// Внешний модуль предоставляет возможность делегирования запросов.
    /// </summary>
    public static class ProxyExtensions
    {

        /// <summary>
        /// Подключает компонент прожемуточного ПО делегирующего услуги доступа к информации
        /// </summary>
        /// <param name="builder"> обьект выполняющий настройку ПО промежуточного слоя </param>
        /// <param name="url"> url </param>
        /// <returns></returns>
        public static IApplicationBuilder UseProxyServer(this IApplicationBuilder builder, 
                string route, 
                string url,
                Func<RequestMessage,int> commit)
        {
            return builder.UseMiddleware<ProxyComponent>(route, url, commit);
            /*return builder.MapWhen((HttpContext httpContext) => {
                bool matched = route.Length!=0 && route.Equals(httpContext.Request.Path.Value.ToString().Substring(0,route.Length));
                if (matched)
                {
                    httpContext.Request.Path = route.Length== httpContext.Request.Path.Value.Length? 
                        "": httpContext.Request.Path.Value.Substring(route.Length);
                }                
                return matched;
            }, (IApplicationBuilder builder) => {
                
            });*/

        }



    }
     
       
    
}

﻿
using System;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Mvc.Models;
 

namespace SpbPublicLibsMVC.Controllers
{
    public class PublicController : Controller
    {
        private readonly ILogger<PublicController> _logger;


        public PublicController(
            ILogger<PublicController> logger)
        {
            _logger = logger;
            _logger.LogInformation("Create");                                       
        }

        public IActionResult Login( )
        {
          
            return View();
        }

        public IActionResult Register()
        {
            return View();
        }

      
 
    }
}

﻿using System.Collections.Generic;

namespace ApplicationCore.Converter.Models
{
    /// <summary>
    /// Коллекция сетевых сервисов
    /// </summary>
    public class ControllersMapModel: Dictionary<string, ServiceControllerModel>
    {
    }
}

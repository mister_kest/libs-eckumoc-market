#pragma checksum "D:\wrk\market-eckumoc-libs\NetCoreApplication\ApplicationMVC\Views\Pages\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "7aebdcc136e9f05698cda704e491fa4cd575e6ef"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Pages_Index), @"mvc.1.0.view", @"/Views/Pages/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"7aebdcc136e9f05698cda704e491fa4cd575e6ef", @"/Views/Pages/Index.cshtml")]
    public class Views_Pages_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<ApplicationDb.Entities.Page>>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 3 "D:\wrk\market-eckumoc-libs\NetCoreApplication\ApplicationMVC\Views\Pages\Index.cshtml"
  
    ViewData["Title"] = "Index";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n\r\n\r\n<p>\r\n    <a asp-action=\"Create\">�������</a>\r\n</p>\r\n<table class=\"table\">\r\n    <thead>\r\n        <tr>\r\n            <th>\r\n                ");
#nullable restore
#line 16 "D:\wrk\market-eckumoc-libs\NetCoreApplication\ApplicationMVC\Views\Pages\Index.cshtml"
           Write(Html.DisplayNameFor(model => model.Avatar));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </th>\r\n            <th>\r\n                ");
#nullable restore
#line 19 "D:\wrk\market-eckumoc-libs\NetCoreApplication\ApplicationMVC\Views\Pages\Index.cshtml"
           Write(Html.DisplayNameFor(model => model.Name));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </th>\r\n            <th>\r\n                ");
#nullable restore
#line 22 "D:\wrk\market-eckumoc-libs\NetCoreApplication\ApplicationMVC\Views\Pages\Index.cshtml"
           Write(Html.DisplayNameFor(model => model.Description));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </th>\r\n            <th>\r\n                ");
#nullable restore
#line 25 "D:\wrk\market-eckumoc-libs\NetCoreApplication\ApplicationMVC\Views\Pages\Index.cshtml"
           Write(Html.DisplayNameFor(model => model.Content));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </th>\r\n            <th>\r\n                ");
#nullable restore
#line 28 "D:\wrk\market-eckumoc-libs\NetCoreApplication\ApplicationMVC\Views\Pages\Index.cshtml"
           Write(Html.DisplayNameFor(model => model.Role));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </th>\r\n            <th>\r\n                ");
#nullable restore
#line 31 "D:\wrk\market-eckumoc-libs\NetCoreApplication\ApplicationMVC\Views\Pages\Index.cshtml"
           Write(Html.DisplayNameFor(model => model.App));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </th>\r\n            <th></th>\r\n        </tr>\r\n    </thead>\r\n    <tbody>\r\n");
#nullable restore
#line 37 "D:\wrk\market-eckumoc-libs\NetCoreApplication\ApplicationMVC\Views\Pages\Index.cshtml"
 foreach (var item in Model) {

#line default
#line hidden
#nullable disable
            WriteLiteral("        <tr>\r\n            <td>\r\n                ");
#nullable restore
#line 40 "D:\wrk\market-eckumoc-libs\NetCoreApplication\ApplicationMVC\Views\Pages\Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.Avatar.Mime));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
#nullable restore
#line 43 "D:\wrk\market-eckumoc-libs\NetCoreApplication\ApplicationMVC\Views\Pages\Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.Name));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
#nullable restore
#line 46 "D:\wrk\market-eckumoc-libs\NetCoreApplication\ApplicationMVC\Views\Pages\Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.Description));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
#nullable restore
#line 49 "D:\wrk\market-eckumoc-libs\NetCoreApplication\ApplicationMVC\Views\Pages\Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.Content));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
#nullable restore
#line 52 "D:\wrk\market-eckumoc-libs\NetCoreApplication\ApplicationMVC\Views\Pages\Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.Role.Description));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
#nullable restore
#line 55 "D:\wrk\market-eckumoc-libs\NetCoreApplication\ApplicationMVC\Views\Pages\Index.cshtml"
           Write(Html.DisplayFor(modelItem => item.App.ID));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                <a asp-action=\"Edit\"");
            BeginWriteAttribute("asp-route-id", " asp-route-id=\"", 1552, "\"", 1575, 1);
#nullable restore
#line 58 "D:\wrk\market-eckumoc-libs\NetCoreApplication\ApplicationMVC\Views\Pages\Index.cshtml"
WriteAttributeValue("", 1567, item.ID, 1567, 8, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">�������������</a> |\r\n                <a asp-action=\"Details\"");
            BeginWriteAttribute("asp-route-id", " asp-route-id=\"", 1637, "\"", 1660, 1);
#nullable restore
#line 59 "D:\wrk\market-eckumoc-libs\NetCoreApplication\ApplicationMVC\Views\Pages\Index.cshtml"
WriteAttributeValue("", 1652, item.ID, 1652, 8, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">��������</a> |\r\n                <a asp-action=\"Delete\"");
            BeginWriteAttribute("asp-route-id", " asp-route-id=\"", 1716, "\"", 1739, 1);
#nullable restore
#line 60 "D:\wrk\market-eckumoc-libs\NetCoreApplication\ApplicationMVC\Views\Pages\Index.cshtml"
WriteAttributeValue("", 1731, item.ID, 1731, 8, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">�������</a>\r\n            </td>\r\n        </tr>\r\n");
#nullable restore
#line 63 "D:\wrk\market-eckumoc-libs\NetCoreApplication\ApplicationMVC\Views\Pages\Index.cshtml"
}

#line default
#line hidden
#nullable disable
            WriteLiteral("    </tbody>\r\n</table>\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<ApplicationDb.Entities.Page>> Html { get; private set; }
    }
}
#pragma warning restore 1591

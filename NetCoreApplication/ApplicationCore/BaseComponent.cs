﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using ApplicationCore.Messaging;
using ApplicationCore.Utils;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Domain
{

    /// <summary>
    /// Посредник между потребителем и сервисом
    /// </summary>
    public abstract class BaseComponent
    {
        protected readonly RequestDelegate _next;
    
        public BaseComponent(
            RequestDelegate next)
        {
            _next = next;
        }


        /// <summary>
        /// Запрос http-абстрагируется 
        /// </summary>
        /// <param name="httpContext"> контекст протокола </param>
        /// <param name="_next"> следующий делегат </param>
        /// <param name="app"> сервис уровня приложения </param>
        /// <param name="auth"> сервис проверки подлинности </param>
        public async Task Invoke(HttpContext httpContext)
        {
            try
            {
                RequestMessage request = await this.toMessageRequest(httpContext);
                ResponseMessage response = new ResponseMessage();
                response.request = request;
                this.OnMessage(request, response);

                Console.WriteLine(Json.Stringify(response));
                if (response.confirmed)
                {
                    await httpContext.Response.WriteAsync(JObject.FromObject(response).ToString());
                }
                else
                {
                    await _next.Invoke(httpContext);
                }

            }
            catch(Exception ex)
            {
                OnException(ex);
                await _next.Invoke(httpContext);
            }

        }

        protected abstract void OnException(Exception ex);


        /// <summary>
        /// Метод обработки сообщения
        /// </summary>
        /// <param name="request"> запрос </param>
        /// <param name="response"> ответ </param>
        public abstract void OnMessage(RequestMessage request, ResponseMessage response);


        /// <summary>
        /// Извлечение модели запроса
        /// </summary>
        /// <param name="httpContext"> контекст протокола </param>
        protected async Task<RequestMessage> toMessageRequest(HttpContext httpContext)
        {

            string token = httpContext.Request.Headers["Authorization"];
            string path = httpContext.Request.Path.Value.ToString();
            RequestMessage request = new RequestMessage();
            request.method = httpContext.Request.Method;
            request.created = Timing.GetTime();
            request.token = httpContext.Request.Headers["Authorization"];
            request.request = new RequestParams()
            {
                path = path,
                pars = getHttpParams(httpContext),
                blob = await this.ReadRequestBody(httpContext)
            };
            return request;
        }


        /// <summary>
        /// Метод сериализации парамтеров в строку запроса
        /// </summary>
        /// <param name="parsDictionary"></param>
        /// <returns></returns>
        public string toHttpParams(Dictionary<string, object> parsDictionary)
        {
            string result = "?";
            foreach (var p in parsDictionary)
            {
                Type t = p.GetType();
                result += $"{p.Key}={p.Value}&";
            }
            return result;
        }


        /// <summary>
        /// Извлечение параметров запроса и контекста
        /// </summary>
        /// <param name="httpContext"> контекст протокола </param>
        /// <returns></returns>
        private JObject getHttpParams(HttpContext httpContext)
        {
            Dictionary<string, object> pars = new Dictionary<string, object>();
            foreach (var p in httpContext.Request.Query)
            {
                pars[p.Key] = p.Value;
            }
            return JObject.FromObject(pars);
        }


        /// <summary>
        /// Считывание бинарных данных в основном блоке сообщения
        /// </summary>
        /// <param name="httpContext"> контекст протокола </param>
        /// <returns></returns>
        private async Task<byte[]> ReadRequestBody(HttpContext httpContext)
        {
            long? length = httpContext.Request.ContentLength;
            if (length != null)
            {
                byte[] data = new byte[(long)length];
                await httpContext.Request.Body.ReadAsync(data, 0, (int)length);
                string mime = httpContext.Request.ContentType;
                return data;
            }
            return new byte[0];
        }
    }

}
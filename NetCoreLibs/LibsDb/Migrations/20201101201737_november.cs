﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LibsDb.Migrations
{
    public partial class november : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "Timestamp",
                table: "Calendars",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateTable(
                name: "LoginDaily",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BeginDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LoginDaily", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "LoginFacts",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Created = table.Column<DateTime>(nullable: false),
                    CalendarID = table.Column<int>(nullable: false),
                    UserID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LoginFacts", x => x.ID);
                    table.ForeignKey(
                        name: "FK_LoginFacts_Calendars_CalendarID",
                        column: x => x.CalendarID,
                        principalTable: "Calendars",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_LoginFacts_Users_UserID",
                        column: x => x.UserID,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "LoginWeekly",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BeginDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LoginWeekly", x => x.ID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_LoginFacts_CalendarID",
                table: "LoginFacts",
                column: "CalendarID");

            migrationBuilder.CreateIndex(
                name: "IX_LoginFacts_UserID",
                table: "LoginFacts",
                column: "UserID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "LoginDaily");

            migrationBuilder.DropTable(
                name: "LoginFacts");

            migrationBuilder.DropTable(
                name: "LoginWeekly");

            migrationBuilder.DropColumn(
                name: "Timestamp",
                table: "Calendars");
        }
    }
}

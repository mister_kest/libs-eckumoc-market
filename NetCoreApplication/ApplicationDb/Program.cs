﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace ApplicationDb
{
    
        class Program
        {
            private static string OutputDirectory = "SpbPublicLibsWeb";

            static void Main(string[] args)
            {

            Console.WriteLine(Assembly.GetExecutingAssembly().GetName());



                // get generation directory absolutely path
                string currentDirectory = System.IO.Directory.GetCurrentDirectory();
                int lastIndex = currentDirectory.IndexOf("SpbPublicLibsDb");
                string sourceDirectory = $"{currentDirectory.Substring(0, lastIndex)}{OutputDirectory}";

                // cleaning
                CleanBatchDir(sourceDirectory);

                // write command for generation razorpages
                string dbContext = "SpbPublicLibsDbContext";
                List<string> commands = new List<string>();
                foreach (Type type in Assembly.GetExecutingAssembly().GetTypes())
                {
                    if (type.Name.EndsWith("DbContext") == false && type.Name.StartsWith("_") == false && type.Name.StartsWith("<") == false && type.Name != "Program")
                    {

                        System.IO.File.WriteAllText(
                            $"{sourceDirectory}generate-{getNameKebabed(type.Name)}-edit-razorpage.bat",
                            $"dotnet aspnet-codegenerator razorpage Edit{type.Name} Edit -m {type.Name} -dc {dbContext} -outDir Pages/{type.Name}");
                        System.IO.File.WriteAllText(
                            $"{sourceDirectory}generate-{getNameKebabed(type.Name)}-delete-razorpage.bat",
                            $"dotnet aspnet-codegenerator razorpage Delete{type.Name} Delete -m {type.Name} -dc {dbContext} -outDir Pages/{type.Name}");
                        System.IO.File.WriteAllText(
                            $"{sourceDirectory}generate-{getNameKebabed(type.Name)}-details-razorpage.bat",
                            $"dotnet aspnet-codegenerator razorpage Form{type.Name} Details -m {type.Name} -dc {dbContext} -outDir Pages/{type.Name}");
                        System.IO.File.WriteAllText(
                            $"{sourceDirectory}generate-{getNameKebabed(type.Name)}-list-razorpage.bat",
                            $"dotnet aspnet-codegenerator razorpage List{type.Name} List -m {type.Name} -dc {dbContext} -outDir Pages/{type.Name}");
                        commands.Add($"{sourceDirectory}generate-{getNameKebabed(type.Name)}-edit-razorpage.bat");
                        commands.Add($"{sourceDirectory}generate-{getNameKebabed(type.Name)}-delete-razorpage.bat");
                        commands.Add($"{sourceDirectory}generate-{getNameKebabed(type.Name)}-details-razorpage.bat");
                        commands.Add($"{sourceDirectory}generate-{getNameKebabed(type.Name)}-list-razorpage.bat");
                    }
                }

                // write file for generation razorpages
                string batch = "";
                foreach (string cmd in commands)
                {
                    batch += cmd + "\n";
                }
                string batchOutput = $"{sourceDirectory}\\generate-razorpages.bat";
                System.IO.File.WriteAllText(batchOutput, batch);
                //Console.WriteLine(batchOutput);

              
                /*commander.Execute($"{sourceDirectory.Substring(0,2)} && cd {sourceDirectory} && generate-razorpages.bat",
                (text)=> {
                    Console.WriteLine(text);
                    return 1;
                });*/

                // write file to call generation razorpages
                //System.IO.File.WriteAllText(
                //$"{sourceDirectory}\\SpbPublicLibsWeb\\update-razorpages.bat", "for /f %f in ('dir /b *.bat') do %f");


            }


            /// <summary>
            /// Remove all files from directory
            /// </summary>
            /// <param name="dir"> directory </param>
            public static void CleanBatchDir(string dir)
            {
                foreach (var file in System.IO.Directory.GetFiles(dir))
                {
                    if (file.EndsWith(".bat") == true)
                        System.IO.File.Delete(file);
                }
            }


            /// <summary>
            /// Convert name to kebab case style
            /// </summary>
            /// <param name="name"> name </param>
            /// <returns></returns>
            public static string getNameKebabed(string name)
            {
                string kebab = "";
                for (int i = 0; i < name.Length; i++)
                {
                    if (i != 0 && name[i].ToString() == name[i].ToString().ToUpper())
                    {
                        kebab += "-" + name[i].ToString().ToLower();
                    }
                    else
                    {
                        kebab += name[i].ToString().ToLower();
                    }
                }
                return kebab;
            }


        }
   
}

﻿using Microsoft.Extensions.DependencyInjection;

using SpbPublicLibsCore.Testing;

using SpbPublicLibsUnit.DomainUnit;
using SpbPublicLibsUnit.Encoder;

namespace SpbPublicLibsUnit
{
    public static class TestExtensions
    {
        public static IServiceCollection AddTestSpbPublicLibs(this IServiceCollection collection)
        {
            TestSpbPublicLibs main = new TestSpbPublicLibs();
            TestingUnit p = main;
            foreach(object pchild in main.Values)
            {                
                collection.AddScoped<TestSpbPublicLibs>();
                collection.AddScoped<TestDomain>();
                collection.AddScoped<TestAuthentication>();
                collection.AddScoped<TestEncoding>();
                collection.AddScoped<TestReflection>();
                collection.AddScoped<TestRegistration>();
                collection.AddScoped<TestSession>();
                collection.AddScoped<TestBinaryEncoder>();
                collection.AddScoped<TestCharacterEncoder>();
            }
            collection.AddScoped<TestSpbPublicLibs>();
            return collection;
        }

    }
}

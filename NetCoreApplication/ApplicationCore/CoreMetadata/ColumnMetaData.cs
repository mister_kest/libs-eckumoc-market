﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Domain.Odbc.Metadata
{
    /// <summary>
    /// Класс определяет свойства атрибута сущности.
    /// </summary>
    public class ColumnMetaData
    {
        public string name;
        public string description;

        /// <summary>
        /// Тип данных Sql
        /// </summary>
        public string type;

        public bool primary;
        public bool incremental;
        public bool unique;
        public bool nullable;        
    }
}

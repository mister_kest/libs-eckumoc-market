import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PublicWeatherService {

  constructor( private http: HttpClient ) { }

  getWeatherForecast(longtide,latitude,time){
    return this.http.get('/api/Weather/GetOneCall?lat='+latitude+'&lon='+longtide+'&time='+time);
  }
}

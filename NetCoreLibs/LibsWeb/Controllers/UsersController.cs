using System.Linq;
using ApplicationDb;
 using LibsDb; 
using LibsDb.LibsEntities;
using ApplicationDb.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;


[ApiController]
[Route("api/Users/[action]")]
public class UsersController: ControllerBase
{
	private readonly LibsDbContext _db; 


	public UsersController( LibsDbContext db ){
		_db=db;
	}

	public void Create( User record )	{
		_db.Users.Add(record);
		_db.SaveChanges();
	}

	public void Remove( int id ){
		_db.Users.Remove(this.Find(id));
		_db.SaveChanges();
	}

	public User Find( int id )	{
		 return _db.Users.Find(id);
	}

	public System.Collections.Generic.List<User> List(   )	{
		 return _db.Users.ToList();
	}

}
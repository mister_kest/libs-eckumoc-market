import { PublicNewsModule } from './public-news/public-news.module';
import { PublicMoviesModule } from './public-movies/public-movies.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PublicWeatherModule } from './public-weather/public-weather.module';
import { PublicMoviesComponent } from './public-movies/public-movies.component';
import { PublicWeatherComponent } from './public-weather/public-weather.component';
import { PublicNewsComponent } from './public-news/public-news.component';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    PublicWeatherModule,
    PublicMoviesModule,
    PublicNewsModule
  ]
})
export class AppPublicModule {
  static routes = [
    {path: '', redirectTo: 'movies', pathMatch: 'full'},
    {path: 'news',    component: PublicNewsComponent },
    {path: 'movies',  component: PublicMoviesComponent },
    {path: 'weather',  component: PublicWeatherComponent },
  ];
}

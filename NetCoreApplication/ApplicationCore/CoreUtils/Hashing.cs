﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace ApplicationCore.Utils
{
    /// <summary>
    /// Реализует методы хэширования
    /// </summary>
    public class Hashing
    {  
        
        /// <summary>
        /// Хэширование текстых данных
        /// </summary>
        /// <param name="text"> текст </param>
        /// <returns> результат хэширования </returns>
        public static string GetHashSha256(string text)
        {
            byte[] bytes = Encoding.Unicode.GetBytes(text);
            SHA256Managed hashstring = new SHA256Managed();
            byte[] hash = hashstring.ComputeHash(bytes);
            string hashString = string.Empty;
            foreach (byte x in hash)
            {
                hashString += String.Format("{0:x2}", x);
            }
            return hashString;
        }


        /// <summary>
        /// Генерация псевдо-случайной последовательности символов
        /// </summary>
        /// <param name="length"> длина последовательности </param>
        /// <returns> псевдо-случайная последовательность символов </returns>
        public static string RandomString(int length)
        {
            Random random = new Random();
            string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
                            "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToLower() +
                            "0123456789";
            return new string(Enumerable.Repeat(chars, length)
                              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}

﻿using Microsoft.AspNetCore.Mvc.ApplicationModels;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpbPublicLibsWeb.MVC.DescriptionAttributes
{
    public class MvcControllerDescriptionAttribute : Attribute, IControllerModelConvention
    {
        private readonly string _icon;
        private readonly string _label;
        private readonly string _description;

        public MvcControllerDescriptionAttribute(string label, string icon, string description)
        {
            _icon = icon;
            _label = label;
            _description = description;
        }

        public void Apply(ControllerModel controllerModel)
        {
            controllerModel.Properties["description"] = _description;
        }
    }
}

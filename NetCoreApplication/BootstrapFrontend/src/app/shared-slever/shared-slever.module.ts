import { SelectLanguageModule } from './select-language/select-language.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    SelectLanguageModule
  ],
  exports: [
    SelectLanguageModule
  ]
})
export class SharedSleverModule { }

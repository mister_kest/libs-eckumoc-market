﻿using SpbPublicLibsCore.Domain;
using SpbPublicLibsCore.Testing;
using System.Collections.Generic;
using System.Linq;

namespace SpbPublicLibsUnit
{
    public class TestProfileManager: TestingUnit
    {
        protected override void onTest()
        {
            rolesMustBeInserted();
            canRemoveUserAccount();
            canFindUserAccountByCredentials();
            
            canGetUserMessages();
            canGetNewsUserMessagesFromUser();
            canGetUserMessagesFromUser();
            canSendMessageToUser();
        }

        public void canRemoveUserAccount()
        {
            using (ProfileManager manager = new ProfileManager())
            {
                SpbPublicLibsDb.DAL.SpbPublicLibs.UserAccount account = manager.FindUserAccount("eckumoc@gmail.com");
                if (account != null)
                {
                    manager.DeleteUserAccount(account);
                }
                //string account_password, string account_username, string person_birthday, string person_firstname, string person_lastname, string person_secondname, string person_sex                
                manager.SaveChanges();
            }

            messages.Add("can remove user account");
        }


        public void canFindUserAccountByCredentials()
        {
            using (ProfileManager manager = new ProfileManager())
            {
                string username = "eckumoc@gmail.com";
                string password = "sgdf1423";
                SpbPublicLibsDb.DAL.SpbPublicLibs.UserAccount account = manager.FindUserAccount(username, password);
            }
            messages.Add("can find user account");
            
        }




        public void rolesMustBeInserted()
        {
            using (ProfileManager manager = new ProfileManager())
            {
                manager.CheckRoles();
                manager.SaveChanges();
            }
             
        }

        private void canSendMessageToUser()
        {
            using (ProfileManager manager = new ProfileManager())
            {                
                manager.SendMessage(
                    manager._context.UserAccounts.FirstOrDefault<SpbPublicLibsDb.DAL.SpbPublicLibs.UserAccount>().Username,
                    manager._context.UserAccounts.FirstOrDefault<SpbPublicLibsDb.DAL.SpbPublicLibs.UserAccount>().Username,
                    "Testing",
                    "This is a test");
            }
            this.messages.Add("can send message to user");
        }

        private void canGetUserMessagesFromUser()
        {
            using (ProfileManager manager = new ProfileManager())
            {
                 
                ICollection<SpbPublicLibsDb.DAL.SpbPublicLibs.UserMessage> messages =
                    manager.GetInboxMessagesFromAccount(
                       manager._context.UserAccounts.FirstOrDefault<SpbPublicLibsDb.DAL.SpbPublicLibs.UserAccount>().Username,
                       manager._context.UserAccounts.FirstOrDefault<SpbPublicLibsDb.DAL.SpbPublicLibs.UserAccount>().Username
                    );
            }
            this.messages.Add("can select user messages witch arrived from another user");
        }

        private void canGetUserMessages()
        {
            using (ProfileManager manager = new ProfileManager())
            {
                 
                IEnumerable<SpbPublicLibsDb.DAL.SpbPublicLibs.UserMessage> messages =
                    manager.GetInboxMessages(
                        manager._context.UserAccounts.FirstOrDefault<SpbPublicLibsDb.DAL.SpbPublicLibs.UserAccount>().Username
                    );
            }
            this.messages.Add("can get user messages");
        }

        private void canGetNewsUserMessagesFromUser()
        {
            using (ProfileManager manager = new ProfileManager())
            {
                 
                List<SpbPublicLibsDb.DAL.SpbPublicLibs.UserMessage> messages =
                    manager.GetNewsMessages(
                        manager._context.UserAccounts.FirstOrDefault<SpbPublicLibsDb.DAL.SpbPublicLibs.UserAccount>().Username
                    );
            }
            this.messages.Add("can get news user messages");
        }
    }
}

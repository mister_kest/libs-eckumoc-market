﻿using Newtonsoft.Json.Linq;
using ApplicationCore.Domain.Odbc.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Home
{
    public class Analitics
    {
        private DatabaseManager databaseManager;
        private TableManagerStatefull dataInputController;

        public Analitics( DatabaseManager databaseManager )
        {
            this.databaseManager = databaseManager;
            this.dataInputController = ((TableManagerStatefull)this.databaseManager.Get("datainput"));
        }

        public object RequestIndicatorsAndLocations( )
        {
            Dictionary<string, object> response = new Dictionary<string, object>();
            response["indicators"] = ((TableManagerStatefull)this.databaseManager.Get("indicators")).SelectAll();
            response["subjects"] = ((TableManagerStatefull)this.databaseManager.Get("subjects")).SelectAll();

            return response;
        }


        public object RequestData( string beginDate, string endDate, Int64 granularity, JArray indicators, JArray locations )
        {
            if (indicators == null)
            {
                throw new ArgumentNullException("indicators argument references to null pointer");
            }
            if (beginDate == null)
            {
                throw new ArgumentNullException("beginDate argument references to null pointer");
            }
            if (endDate == null)
            {
                throw new ArgumentNullException("endDate argument references to null pointer");
            }
            if (locations == null)
            {
                throw new ArgumentNullException("locations argument references to null pointer");
            }
            List<int> indicatorsList = new List<int>();
            foreach (JValue val in indicators)
            {
                indicatorsList.Add(val.Value<int>());
            }
            List<int> locationsList = new List<int>();
            foreach (JValue val in locations)
            {
                locationsList.Add(val.Value<int>());
            }
            return (from p in this.dataInputController.dataRecords
             where p["begin_date"].Value<DateTime>() >= DateTime.Parse(beginDate) &&
                   p["begin_date"].Value<DateTime>() <= DateTime.Parse(endDate) &&
                   p["granularity_id"].Value<int>() == granularity &&
                   indicatorsList.Contains(p["indicator_id"].Value<int>()) &&
                   locationsList.Contains(p["subject_id"].Value<int>()) 
             select p).ToList();

            //return this.databaseManager.RequestData( beginDate, endDate, granularity, indicators, locations );
        }




        public string GeneratedRandomStatistics()
        {
            int ctn = 0;
            string query = "";
            DateTime started = DateTime.Now;
            foreach (var indicator in ((TableManagerStatefull)this.databaseManager.Get("indicators")).SelectAll()){
                foreach(var subject in ((TableManagerStatefull)this.databaseManager.Get("subjects")).SelectAll()){
                    Random rand = new Random();
                    DateTime beginDate = DateTime.Parse("01.01.2016");
                    while(beginDate < DateTime.Now)
                    {
                        float value = (float)Math.Floor(rand.NextDouble() * 100);
                        string sql = "INSERT INTO APP.DATAINPUT( BEGIN_DATE, GRANULARITY_ID, SUBJECT_ID, INDICATOR_ID, INDICATOR_VALUE ) VALUES("+ "'" + beginDate.Year + "-" + beginDate.Month + "-" + beginDate.Day + "', 6, "+subject["subject_id"].Value<int>()+", "+indicator["indicator_id"].Value<int>()+", "+value+");\n";
                        query += sql;
                        ctn++;
                        beginDate = beginDate.AddMonths(1);
                    }
                }
            }

            DateTime complete = DateTime.Now;
            /*return new
            {
                started = started,
                complete = complete,
                count = ctn,
                query = query
            };*/
            return query;



        }
          
    }
}

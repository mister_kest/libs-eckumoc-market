﻿using SpbPublicLibsCore.Testing;
using SpbPublicLibsUnit.ConverterUnit;


namespace SpbPublicLibsUnit
{
    public class TestSpbPublicLibs : TestingUnit
    {
        public TestSpbPublicLibs()
        {            
            this.Push(new TestUtils());
            this.Push(new TestAPIS());
            this.Push(new TestConverter());
        }

        protected override void onTest()
        {
            
        }
    }
}

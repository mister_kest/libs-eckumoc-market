﻿using ApplicationDb.Entities;
using ApplicationDb.Types;
 
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace LibsDb.LibsEntities
{


    [DisplayName("Сведения о выдаче литературы")]
    public class DeliveryFact: FactsTable
    {
        [Key]
        public int ID { get; set; }




        [DisplayName("Литература")]
        public int LiteratureID { get; set; }
        public virtual Liter Literature { get; set; }


        [DisplayName("Заявка")]
        public int OrderID { get; set; }
        public virtual DeliveryOrder Order { get; set; }
    }
}

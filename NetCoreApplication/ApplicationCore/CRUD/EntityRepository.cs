﻿using ApplicationCore.Domain.Reflection;

using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace ApplicationCore.CRUD
{

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class EntityRepository<T>
    {
        private readonly DbContext _context;
        private readonly dynamic _dbset;
        private readonly string _name;
         
        public EntityRepository( DbContext context, dynamic dbset, string name)
        {
            _context = context;
            _dbset = dbset;
            _name = name;             
        }


        

        /// <summary>
        /// Создание новой записи
        /// </summary>
        /// <param name="record"></param>
        public void Create(T record)
        {
            _dbset.Add(record);
            _context.SaveChanges();
        }


        /// <summary>
        /// Удаление записи
        /// </summary>
        /// <param name="id"></param>
        public void Remove(int id)
        {
            _dbset.Remove(_dbset.Find(id));
            _context.SaveChanges();
        }


        /// <summary>
        /// Удаление
        /// </summary>
        /// <param name="item"></param>
        public void Remove(T item)
        {
            _dbset.Remove(item);
            _context.SaveChanges();
        }


        /// <summary>
        /// Поиск
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public T Find(int id)
        {
            return _dbset.Find(id);
        }


        /// <summary>
        /// Поиск
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<T> FindRange(int x1, int x2 )
        {
            ReflectionService reflection = new ReflectionService();
            return (from i in ((IQueryable<T>)_dbset)
             where (int)reflection.GetValue(i, "ID") >= x1
                 && (int)reflection.GetValue(i, "ID") < x2
             select i).ToList();
        }


        public void Update(dynamic item)
        {
            object target = _dbset.Find(item.ID);
            ReflectionService reflection = new ReflectionService();
            reflection.copy(item,target );            
            _context.SaveChanges();
        }


        public System.Collections.Generic.List<T> List<T>()
        {            
            return ((IQueryable<T>)_dbset).ToList();
        }



        public System.Collections.Generic.List<T> Page( int number, int size )
        {
            return ((IQueryable<T>)_dbset).Skip(size*number).Take(size).ToList();
        }



        





        public IEnumerable<INavigation> GetNavigationProperties()
        {                    
            IEntityType entity = (from navs in _context.Model.GetEntityTypes() where navs.Name == typeof(T).FullName select navs).SingleOrDefault();
            return entity.GetNavigations();          
        }

    }
}

﻿using ApplicationCore.Testing;

using ApplicationDb;
using ApplicationDb.Entities;

using Microsoft.EntityFrameworkCore;

 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ApplicationCore.CRUD
{
    public class TestEntityRepository: TestingUnit
    {
        protected override void onTest()
        {
            using (ApplicationDbContext db = new ApplicationDbContext())
            {                
                EntityRepository<News> repository = new EntityRepository<News>(db, db.News, "News");
                News message = new News()
                {
                    Time = DateTime.Now,
                    Title = "Test",
                    Description = "This is a test",
                    Href = "http://localhost:4200"
                };
                repository.Create(message);
                int x = 0;

                foreach (var item in repository.List<object>())
                {
                    Console.WriteLine(item);
                }
                message.Title = "test 2";
                repository.Update(message);
                message.Title = "test 3";
                repository.Update(message);
            }

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using LibsDb;
using LibsDb.LibsEntities;

namespace LibsMVC.Areas.ReaderFace.Controllers
{
    public class DeliveryOrderController : Controller
    {
        private readonly LibsDbContext _context;

        public DeliveryOrderController(LibsDbContext context)
        {
            _context = context;
        }

        // GET: DeliveryOrders
        public async Task<IActionResult> Index()
        {
            var libsDbContext = _context.DeliveryOrders.Include(d => d.User);
            return View(await libsDbContext.ToListAsync());
        }

        // GET: DeliveryOrders/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var deliveryOrder = await _context.DeliveryOrders
                .Include(d => d.User)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (deliveryOrder == null)
            {
                return NotFound();
            }

            return View(deliveryOrder);
        }

        // GET: DeliveryOrders/Create
        public IActionResult Create()
        {
            ViewData["UserID"] = new SelectList(_context.Users, "ID", "ID");
            return View();
        }

        // POST: DeliveryOrders/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,UserID")] DeliveryOrder deliveryOrder)
        {
            if (ModelState.IsValid)
            {
                _context.Add(deliveryOrder);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["UserID"] = new SelectList(_context.Users, "ID", "ID", deliveryOrder.UserID);
            return View(deliveryOrder);
        }

        // GET: DeliveryOrders/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var deliveryOrder = await _context.DeliveryOrders.FindAsync(id);
            if (deliveryOrder == null)
            {
                return NotFound();
            }
            ViewData["UserID"] = new SelectList(_context.Users, "ID", "ID", deliveryOrder.UserID);
            return View(deliveryOrder);
        }

        // POST: DeliveryOrders/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,UserID")] DeliveryOrder deliveryOrder)
        {
            if (id != deliveryOrder.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(deliveryOrder);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DeliveryOrderExists(deliveryOrder.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["UserID"] = new SelectList(_context.Users, "ID", "ID", deliveryOrder.UserID);
            return View(deliveryOrder);
        }

        // GET: DeliveryOrders/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var deliveryOrder = await _context.DeliveryOrders
                .Include(d => d.User)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (deliveryOrder == null)
            {
                return NotFound();
            }

            return View(deliveryOrder);
        }

        // POST: DeliveryOrders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var deliveryOrder = await _context.DeliveryOrders.FindAsync(id);
            _context.DeliveryOrders.Remove(deliveryOrder);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DeliveryOrderExists(int id)
        {
            return _context.DeliveryOrders.Any(e => e.ID == id);
        }
    }
}

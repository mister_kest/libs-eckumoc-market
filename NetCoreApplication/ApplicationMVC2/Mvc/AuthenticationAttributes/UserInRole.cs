﻿using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace MvcMarketPlace.Mvc.AuthenticationAttributes
{
    public class UserInRole: ActionFilterAttribute
    {

        public UserInRole( string roleName )
        {

        }

        public void OnActionExecuted(ActionExecutedContext context) { }
        public void OnActionExecuting(ActionExecutingContext context) {
            
        }

        [DebuggerStepThrough]
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next) { }
        public void OnResultExecuted(ResultExecutedContext context) { }
        public void OnResultExecuting(ResultExecutingContext context) { }

        [DebuggerStepThrough]
        public async Task OnResultExecutionAsync(ResultExecutingContext context, ResultExecutionDelegate next) { }

    }
}

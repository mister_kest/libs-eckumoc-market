﻿using System.Linq;
using System.Collections.Generic;
using System.Text;
using ApplicationCore.Converter.Models;
using System.Reflection;
using System;

namespace ApplicationCore.Domain.Reflection
{
    public class ReflectionScope: ServiceControllerModel
    {
        private readonly string _path;
        private readonly object _target;


        public ReflectionScope( string path, object target ): base( )
        {
            _path = path;
            Name = $"{target.GetType().Name}Scope";
            _target = target;
            Actions = new Dictionary<string, ControllerActionModel>();
            List<string> methodNames = new List<string>();
            foreach (MethodInfo method in _target.GetType().GetMethods())
            {
                if (method.IsPublic && method.IsStatic == false)
                {
                    Actions[method.Name] = ToModel(method);

                }                    
            }
        }


        private ControllerActionModel ToModel(MethodInfo method)
        {
            ControllerActionModel model = new ControllerActionModel();
            model.Parameters = new Dictionary<string, ActionParameterModel>();
            model.Name = method.Name;
            model.Path = _path + "/" + model.Name;
            foreach(var p in method.GetParameters())
            {
                IsTypeSupported(p.GetType());
                model.Parameters[p.Name] = new ActionParameterModel()
                {
                    Name = p.Name,
                    Type = p.GetType().Name,
                    IsOptional = p.IsOptional
                };
            }
            return model;
        }


        private void IsTypeSupported(Type type)
        {
            //TODO:
        }

  


        public ReflectionScope Get(string name)
        {
            var field = _target.GetType().GetField(name);
            return field != null ? new ReflectionScope($"{_path}/{name}", field.GetValue(_target)) : null;
        }


        public ReflectionScope Call(string name, ActionParameterModel parameters )
        {
            var field = _target.GetType().GetField(name);
            return field != null ? new ReflectionScope($"{_path}/{name}", field.GetValue(_target)) : null;
        }


        public List<string> ListPropertyNames()
        {
            List<string> methodNames = new List<string>();
            foreach (var method in _target.GetType().GetFields())
            {
                methodNames.Add(method.Name);
            }
            return methodNames;
        }


        public List<string> ListMethodNames()
        {
            List<string> methodNames = new List<string>();
            foreach (var method in _target.GetType().GetMethods())
            {                
                if (method.IsPublic && method.IsStatic==false)
                    methodNames.Add(method.Name);
            }
            return methodNames;
        }
    }
}

import { ReaderOrderService } from './reader-order.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReaderOrderComponent } from './reader-order.component';
import { SharedModule } from 'src/app/shared/shared.module';



@NgModule({
  declarations: [ReaderOrderComponent],
  exports:      [ReaderOrderComponent],
  providers:    [ReaderOrderService],
  imports: [
    CommonModule,
    SharedModule
  ]
})
export class ReaderOrderModule { }

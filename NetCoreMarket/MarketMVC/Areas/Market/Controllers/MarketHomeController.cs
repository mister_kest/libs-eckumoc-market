﻿using CoreApp.AppAPI;

using EcKuMoC.Areas.Market.Models;
using EcKuMoC.Mvc;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace EcKuMoC.Areas.Market
{
    [Area("Market")]
    public class MarketHomeController : Controller
    {
        private static string MARKET_HOME_MODEL_TYPE_NAME = typeof(MarketViewModel).FullName;
        private readonly ILogger<MarketHomeController> _logger;
        private readonly APIAuthorization _auth;

        public MarketHomeController( 


            ILogger<MarketHomeController> logger, 
            APIAuthorization auth)

        {
            _logger = logger;
            _auth = auth;
            _logger.LogInformation("Create");
        }

        public IActionResult Index()
        {

            
            return View("Edit",new MvcMarketPlace.Data.Entities.Market() {
                Name = "EcKuMoC market",
                Description = "We are saling enternet markets."
                
            });
        }

        public IActionResult SelectSubdivisions()
        {
            return PartialView();
        }

        
    }
}

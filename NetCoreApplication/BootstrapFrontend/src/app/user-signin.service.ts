import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { pipe,Observable } from "rxjs";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class UserSigninService{

  constructor( private http: HttpClient ){}

  signin( email, password ){
    return this.http.get('/api/AccountApi/Signin',{
      params:{
        Email: email,
        Password: password
      }
    });
  }

  signout(    ){
    return this.http.get('/api/AccountApi/Signout',{
      params:{
      }
    });
  }

  signup( email, password, firstname, surname, lastname, birthday, tel  ){
    return this.http.get('/api/AccountApi/Signup',{
      params:{
        Email:     email,
        Password:  password,
        FirstName: firstname,
        SurName:   surname,
        LastName:  lastname,
        Birthday:  birthday,
        Tel:       tel
      }
    });
  }

  validate(): Observable<boolean>{
    return this.http.get('/api/AccountApi/IsSignin').pipe(map((resp:any)=>{
      console.log(resp);
      return resp.isSignin;
    }));
  }
}

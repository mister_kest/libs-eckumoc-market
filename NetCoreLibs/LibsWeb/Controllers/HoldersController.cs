using System.Linq;
using ApplicationDb;
 using LibsDb; 
using LibsDb.LibsEntities;
using ApplicationDb.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;


[ApiController]
[Route("api/Holders/[action]")]
public class HoldersController: ControllerBase
{
	private readonly LibsDbContext _db; 


	public HoldersController( LibsDbContext db ){
		_db=db;
	}

	public void Create( Holder record )	{
		_db.Holders.Add(record);
		_db.SaveChanges();
	}

	public void Remove( int id ){
		_db.Holders.Remove(this.Find(id));
		_db.SaveChanges();
	}

	public Holder Find( int id )	{
		 return _db.Holders.Find(id);
	}

	public System.Collections.Generic.List<Holder> List(   )	{
		 return _db.Holders.ToList();
	}

}
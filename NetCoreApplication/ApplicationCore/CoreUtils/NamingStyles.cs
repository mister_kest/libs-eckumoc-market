﻿namespace ApplicationCore.Utils
{
    /// <summary>
    /// Перечисление стилей записи идентификаторов
    /// </summary>
    public enum NamingStyles
    {
        Capital, Kebab, Snake, Dollar, Camel
    }
}

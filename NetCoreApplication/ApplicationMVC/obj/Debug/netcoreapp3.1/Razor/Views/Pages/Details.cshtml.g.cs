#pragma checksum "D:\wrk\market-eckumoc-libs\NetCoreApplication\ApplicationMVC\Views\Pages\Details.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "cf51a8581677a13989c05b950ee0a286276e39f1"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Pages_Details), @"mvc.1.0.view", @"/Views/Pages/Details.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"cf51a8581677a13989c05b950ee0a286276e39f1", @"/Views/Pages/Details.cshtml")]
    public class Views_Pages_Details : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<ApplicationDb.Entities.Page>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 3 "D:\wrk\market-eckumoc-libs\NetCoreApplication\ApplicationMVC\Views\Pages\Details.cshtml"
  
    ViewData["Title"] = "Details";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n<h1>Details</h1>\r\n\r\n<div>\r\n    <h4>Page</h4>\r\n    <hr />\r\n    <dl class=\"row\">\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 14 "D:\wrk\market-eckumoc-libs\NetCoreApplication\ApplicationMVC\Views\Pages\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.Avatar));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 17 "D:\wrk\market-eckumoc-libs\NetCoreApplication\ApplicationMVC\Views\Pages\Details.cshtml"
       Write(Html.DisplayFor(model => model.Avatar.Mime));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 20 "D:\wrk\market-eckumoc-libs\NetCoreApplication\ApplicationMVC\Views\Pages\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.Name));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 23 "D:\wrk\market-eckumoc-libs\NetCoreApplication\ApplicationMVC\Views\Pages\Details.cshtml"
       Write(Html.DisplayFor(model => model.Name));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 26 "D:\wrk\market-eckumoc-libs\NetCoreApplication\ApplicationMVC\Views\Pages\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.Description));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 29 "D:\wrk\market-eckumoc-libs\NetCoreApplication\ApplicationMVC\Views\Pages\Details.cshtml"
       Write(Html.DisplayFor(model => model.Description));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 32 "D:\wrk\market-eckumoc-libs\NetCoreApplication\ApplicationMVC\Views\Pages\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.Content));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 35 "D:\wrk\market-eckumoc-libs\NetCoreApplication\ApplicationMVC\Views\Pages\Details.cshtml"
       Write(Html.DisplayFor(model => model.Content));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 38 "D:\wrk\market-eckumoc-libs\NetCoreApplication\ApplicationMVC\Views\Pages\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.Role));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 41 "D:\wrk\market-eckumoc-libs\NetCoreApplication\ApplicationMVC\Views\Pages\Details.cshtml"
       Write(Html.DisplayFor(model => model.Role.Description));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n        <dt class = \"col-sm-2\">\r\n            ");
#nullable restore
#line 44 "D:\wrk\market-eckumoc-libs\NetCoreApplication\ApplicationMVC\Views\Pages\Details.cshtml"
       Write(Html.DisplayNameFor(model => model.App));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dt>\r\n        <dd class = \"col-sm-10\">\r\n            ");
#nullable restore
#line 47 "D:\wrk\market-eckumoc-libs\NetCoreApplication\ApplicationMVC\Views\Pages\Details.cshtml"
       Write(Html.DisplayFor(model => model.App.ID));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </dd>\r\n    </dl>\r\n</div>\r\n<div>\r\n    <a asp-action=\"Edit\"");
            BeginWriteAttribute("asp-route-id", " asp-route-id=\"", 1473, "\"", 1497, 1);
#nullable restore
#line 52 "D:\wrk\market-eckumoc-libs\NetCoreApplication\ApplicationMVC\Views\Pages\Details.cshtml"
WriteAttributeValue("", 1488, Model.ID, 1488, 9, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">Edit</a> |\r\n    <a asp-action=\"Index\">��������� � ������</a>\r\n</div>\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<ApplicationDb.Entities.Page> Html { get; private set; }
    }
}
#pragma warning restore 1591

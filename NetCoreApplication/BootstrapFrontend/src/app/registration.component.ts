import { Component } from "@angular/core";

@Component({
  template: `
  <app-frame>
  <div class="frame-header display-4"><img src="/assets/icons/icon-72x72.png"/>
    Регистрация
  </div>
<div>

  <div class="tab-content" id="v-pills-tabContent">
    <form>
      <!-- <div class="form-group row">
        <label for="firstname" class="col-sm-2 col-form-label" style="word-break: keep-all;">
          Имя
        </label>
        <div class="col-sm-10">
          <input type="text" class="form-control" id="firstname" placeholder="Имя">
        </div>
      </div> -->
      <!-- <div class="form-group row">
        <label for="secondname" class="col-sm-2 col-form-label" style="word-break: keep-all;">Фамилия</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" id="secondname" placeholder="Фамилия">
        </div>
      </div> -->
      <!-- <div class="form-group row">
        <label for="lastname" class="col-sm-2 col-form-label" style="word-break: keep-all;">Отчество</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" id="lastname" placeholder="Отчество">
        </div>
      </div> -->
      <!-- <div class="form-group row">
        <label for="birthday" class="col-sm-2 col-form-label" style="word-break: keep-all;">Дата рождения</label>
        <div class="col-sm-10">
          <input type="date" class="form-control" id="birthday" placeholder="Дата рождения">
        </div>
      </div> -->

      <h3>Параметры входа</h3>
      <div class="form-group" align="left">
        <label for="exampleInputEmail1"><b>Адрес электронной почты</b></label>
        <input [(ngModel)]="account.email" type="email" class="form-control" aria-describedby="emailHelp" placeholder="Адрес электронной почты">
        <small id="emailHelp" class="form-text text-muted">
          В дальнейшем для входа в систему необходимо указать этот адрес
        </small>
      </div>
      <div class="form-group" align="left">
        <label><b>Пароль</b></label>
        <input [(ngModel)]="account.password" type="password" class="form-control" placeholder="Пароль">
      </div>
      <div class="form-group" align="left">
        <label><b>Подтверждение пароля</b></label>
        <input [(ngModel)]="account.confirmation" type="password" class="form-control" placeholder="Подтверждение пароля">
      </div>
      <hr style="width: 100%;"/>

      <h3>Личные данные</h3>
      <div class="form-group" align="left">
        <label><b>Имя</b></label>
        <input [(ngModel)]="person.firstname" type="text" class="form-control" placeholder="Имя">
      </div>
      <div class="form-group" align="left">
        <label><b>Фамилия</b></label>
        <input [(ngModel)]="person.surname" type="text" class="form-control" placeholder="Фамилия">
      </div>
      <div class="form-group" align="left">
        <label><b>Отчество</b></label>
        <input [(ngModel)]="person.lastname" type="text" class="form-control" placeholder="Отчество">
      </div>
      <div class="form-group" align="left">
        <label><b>Дата рождения</b></label>
        <input [(ngModel)]="person.birthday" type="date" class="form-control" placeholder="Дата рождения">
      </div>
      <div class="form-group" align="left">
        <label><b>Телефон</b></label>
        <input [(ngModel)]="person.tel" type="tel" class="form-control" placeholder="Телефон">
      </div>


      <div class="form-check">
        <input type="checkbox" class="form-check-input" id="exampleCheck1">
        <label class="form-check-label" for="exampleCheck1" >
          Я обязуюсь соблюдать правила пользования, изложенные в
        </label>
        <a [routerLink]="['/rules']">"Политика конфиденциальности"</a>
      </div>
      <div class="text-danger" align="center">{{  error }}</div>
      <hr/>
    </form>
  </div>



  <div align="right">
    <button class="btn btn-primary" (click)="signup()"> login </button>
  </div>
</div>
</app-frame>
<div align=center><a [routerLink]="['/login']"> login </a></div>



  `,
  selector: 'registration'
})
export class RegistrationComponent
{

  account = {
    email: '',
    password: '',
    confirmation: ''
  }

  person = {
    firstname: '',
    surname: '',
    lastname: '',
    birthday: new Date(),
    tel: ''
  }

  error = '';


  signup(){

  }
}

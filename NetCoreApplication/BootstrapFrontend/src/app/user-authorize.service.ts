import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { UserNotificationsService } from './user-notifications.service';
import { UserProfileService } from './user-profile.service';

import { UserSigninService } from './user-signin.service';




@Injectable({
  providedIn: 'root'
})
export class AuthorizeService{

  user: any;

  constructor(
    public signin: UserSigninService,
    private profile: UserProfileService,
    private notifications: UserNotificationsService ){}

  onSigninSuccess(){
    this.profile.fetchUserInfo();
    this.notifications.subscribe();
  }

  onSigninFailed(){
    this.user = null;
    this.notifications.unsubscribe();
  }

  onSignoutSuccess(){
    this.notifications.unsubscribe();
  }

}


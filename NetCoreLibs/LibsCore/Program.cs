﻿using LibsDb;

using System;

namespace LibsCore
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            using (LibsDbContext db = new LibsDbContext())
            {
                foreach(var a in db.Model.GetAnnotations())
                {
                    Console.WriteLine(a.Name);
                }
            }
        }
    }
}

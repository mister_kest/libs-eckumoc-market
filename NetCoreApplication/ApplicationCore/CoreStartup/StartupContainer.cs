﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace Startup
{

    /// <summary>
    /// Контейнер конфигурации делегирует функциии дочерним обьектам
    /// </summary>
    public class StartupContainer: IStartupElement
    {
        private Dictionary<string, IStartupElement> elements = new Dictionary<string, IStartupElement>();



        protected void AddStartup( string name, IStartupElement element )
        {
            this.elements[name] = element;
        }



        public void ConfigureServices(IServiceCollection services)
        {
            foreach( var p in elements)
            {
                System.Diagnostics.Debug.WriteLine("ConfigureServices: " + p.Key);
                p.Value.ConfigureServices(services);
            }
        }



        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            foreach (var p in elements)
            {
                System.Diagnostics.Debug.WriteLine("Configure: " + p.Key);
                p.Value.Configure(app, env);
            }
        }
    }
}

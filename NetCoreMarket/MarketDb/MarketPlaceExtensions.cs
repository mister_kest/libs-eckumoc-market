﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

using MvcMarketPlace.Data;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EcKuMoC.Data
{
    public static class MarketPlaceExtensions
    {

        public static IServiceCollection AddMarketPlace(this IServiceCollection services)
        {
            services.AddDbContext<MarketDbContext>(options=> {
                options.UseSqlServer(MarketDbContext.DefaultConnectionString);
            });
            return services;
        }
    }
}

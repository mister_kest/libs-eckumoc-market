﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.CoreDomain
{
    /// <summary>
    /// Параметры фильтрации запросов
    /// </summary>
    public class CanActivateOptions
    {
        /// <summary>
        /// Маршруты только для авторизованных пользователей
        /// </summary>
        public List<string> SigninValdationRoutes = new List<string>{};

        public Dictionary<string, List<string>> RoleValidationRoutes
            = new Dictionary<string, List<string>>();

        public string LoginPagePath { get; set; } = "/Account/Login";       
    }
}

﻿using ApplicationDb.Types;

using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace LibsDb.LibsEntities
{
    [DisplayName("Место выдачи")]
    public class Holder: DimensionTable
    {
        
        public Holder() {
            Liters = new List<Liter>();
        }

        [Key]
        public int ID { get; set; }

        [DisplayName("Адрес")]
        public string Location { get; set; }


        [DisplayName("Библиотека")]
        public int LibID { get; set; }
        public virtual Lib Lib { get; set; }

        [DisplayName("Литература")]
        public virtual List<Liter> Liters { get; set; }
        
    }
}

import { Component, OnInit } from '@angular/core';
import { AuthorizeService } from './user-authorize.service';

@Component({
  selector: 'app-login',
  template: `
  <app-frame>
  <div class="frame-header display-4"><img src="/assets/icons/icon-72x72.png"/>
    User login
  </div>
  <h2 *ngIf="enabled">Welcome to your new application</h2>
  <h2 *ngIf="!enabled">Please wait</h2>
  <div *ngIf="!enabled">
    <app-progressbar style="width: 100%;" [name]="'authorization'"></app-progressbar>
  </div>
  <div *ngIf="enabled">
    <div class="form-group" align="left">
      <label for="exampleInputEmail1"><b>Email address</b></label>
      <input (input)="username=$event.target.value" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
      <small id="emailHelp" class="form-text text-muted">
        We'll never share your email with anyone else.
      </small>
    </div>
    <div class="form-group" align="left">
      <label for="exampleInputPassword1"><b>Password</b></label>
      <input (input)="password=$event.target.value" type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
    </div>
    <div class="form-check">
      <input type="checkbox" class="form-check-input" id="exampleCheck1">
      <label class="form-check-label" for="exampleCheck1" >Check me out</label>
    </div>
    <div class="text-danger" align="center">{{  error }}</div>
    <hr/>
  </div>
  <div align="right">
    <button class="btn btn-primary" (click)="login(username,password)"> login </button>
  </div>

</app-frame>

<div align=center><a [routerLink]="['/registration']"> registration </a></div>






`
})
export class LoginComponent implements OnInit {

  username = '';
  password = '';

  enabled = true;
  error = '';

  constructor( private authorize: AuthorizeService ){ }

  login( username, password ){
    const ctrl = this;
    this.authorize.signin.signin(username,password).subscribe((message:any)=>{
      console.log(message);
      if( message.status !== 'success'){
        ctrl.error = 'Authorization failed'
      }else{
        ctrl.error = '';
      }
    });
    this.enabled = false;
    setTimeout(()=>{ ctrl.enabled = true; },3000);
  }

  ngOnInit() {
  }

}

﻿using SpbPublicLibsCore.Testing;
using SpbPublicLibsUnit.UtilsUnit;

namespace SpbPublicLibsUnit
{   
    public class TestUtils : TestingUnit
    {
        public TestUtils() : base()
        {
            this.Push(new TestNaming());
            this.Push(new TestHashing());
            this.Push(new TestTyping());
            this.Push(new TestRuntime());
            this.Push(new TestTiming());
        }

        protected override void onTest()
        {            
        }
    }
}

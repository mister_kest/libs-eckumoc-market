using System.Linq;
using ApplicationDb;
 using LibsDb; 
using LibsDb.LibsEntities;
using ApplicationDb.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;


[ApiController]
[Route("api/News/[action]")]
public class NewsController: ControllerBase
{
	private readonly LibsDbContext _db; 


	public NewsController( LibsDbContext db ){
		_db=db;
	}

	public void Create( News record )	{
		_db.News.Add(record);
		_db.SaveChanges();
	}

	public void Remove( int id ){
		_db.News.Remove(this.Find(id));
		_db.SaveChanges();
	}

	public News Find( int id )	{
		 return _db.News.Find(id);
	}

	public System.Collections.Generic.List<News> List(   )	{
		 return _db.News.ToList();
	}

}
﻿using Microsoft.AspNetCore.Mvc;

namespace EcKuMoC.Mvc
{
    public abstract class MvcComponentController: Controller
    {
        protected MvcComponentController( string ModelType )
        {
        }

        public abstract object GetModel();
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SpbPublicLibsWebAPI.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PublicAuthors",
                columns: table => new
                {
                    PublicAuthorId = table.Column<string>(nullable: false),
                    AuthorNationality = table.Column<string>(nullable: true),
                    PublicAlias = table.Column<string>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false),
                    Updated = table.Column<DateTime>(nullable: false),
                    Version = table.Column<int>(nullable: false),
                    Popularity = table.Column<int>(nullable: false),
                    Rating = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PublicAuthors", x => x.PublicAuthorId);
                });

            migrationBuilder.CreateTable(
                name: "PublicCustomers",
                columns: table => new
                {
                    PublicCustomerId = table.Column<string>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    Updated = table.Column<DateTime>(nullable: false),
                    Version = table.Column<int>(nullable: false),
                    Popularity = table.Column<int>(nullable: false),
                    Rating = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PublicCustomers", x => x.PublicCustomerId);
                });

            migrationBuilder.CreateTable(
                name: "PublicDatabases",
                columns: table => new
                {
                    PublicDatabaseId = table.Column<string>(nullable: false),
                    ServerDatabase_PublicDatabaseId = table.Column<int>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    Updated = table.Column<DateTime>(nullable: false),
                    Version = table.Column<int>(nullable: false),
                    Popularity = table.Column<int>(nullable: false),
                    Rating = table.Column<int>(nullable: false),
                    PPublicDatabaseId = table.Column<int>(nullable: false),
                    PPublicDatabasePublicDatabaseId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PublicDatabases", x => x.PublicDatabaseId);
                    table.ForeignKey(
                        name: "FK_PublicDatabases_PublicDatabases_PPublicDatabasePublicDatabaseId",
                        column: x => x.PPublicDatabasePublicDatabaseId,
                        principalTable: "PublicDatabases",
                        principalColumn: "PublicDatabaseId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PublicLibs",
                columns: table => new
                {
                    PublicLibId = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    NameShort = table.Column<string>(nullable: true),
                    Sigla = table.Column<string>(nullable: true),
                    Url = table.Column<string>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false),
                    Updated = table.Column<DateTime>(nullable: false),
                    Version = table.Column<int>(nullable: false),
                    Popularity = table.Column<int>(nullable: false),
                    Rating = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PublicLibs", x => x.PublicLibId);
                });

            migrationBuilder.CreateTable(
                name: "PublicLiteratures",
                columns: table => new
                {
                    PublicLiteratureId = table.Column<string>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    Updated = table.Column<DateTime>(nullable: false),
                    Version = table.Column<int>(nullable: false),
                    Popularity = table.Column<int>(nullable: false),
                    Rating = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PublicLiteratures", x => x.PublicLiteratureId);
                });

            migrationBuilder.CreateTable(
                name: "PublicServers",
                columns: table => new
                {
                    PublicServerId = table.Column<string>(nullable: false),
                    ServerName = table.Column<string>(nullable: true),
                    ServerIp = table.Column<string>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false),
                    Updated = table.Column<DateTime>(nullable: false),
                    Version = table.Column<int>(nullable: false),
                    Popularity = table.Column<int>(nullable: false),
                    Rating = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PublicServers", x => x.PublicServerId);
                });

            migrationBuilder.CreateTable(
                name: "StatisticsCalendars",
                columns: table => new
                {
                    StatisticsCalendarId = table.Column<string>(nullable: false),
                    Day = table.Column<int>(nullable: false),
                    Week = table.Column<int>(nullable: false),
                    Month = table.Column<int>(nullable: false),
                    Quarter = table.Column<int>(nullable: false),
                    Year = table.Column<int>(nullable: false),
                    Hour = table.Column<int>(nullable: false),
                    Minute = table.Column<int>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    Updated = table.Column<DateTime>(nullable: false),
                    Version = table.Column<int>(nullable: false),
                    Popularity = table.Column<int>(nullable: false),
                    Rating = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StatisticsCalendars", x => x.StatisticsCalendarId);
                });

            migrationBuilder.CreateTable(
                name: "UserGroups",
                columns: table => new
                {
                    UserGroupId = table.Column<string>(nullable: false),
                    UserGroupName = table.Column<string>(nullable: true),
                    UserGroupDescription = table.Column<string>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false),
                    Updated = table.Column<DateTime>(nullable: false),
                    Version = table.Column<int>(nullable: false),
                    Popularity = table.Column<int>(nullable: false),
                    Rating = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserGroups", x => x.UserGroupId);
                });

            migrationBuilder.CreateTable(
                name: "UserPersons",
                columns: table => new
                {
                    UserPersonId = table.Column<string>(nullable: false),
                    Firstname = table.Column<string>(nullable: true),
                    Secondname = table.Column<string>(nullable: true),
                    Lastname = table.Column<string>(nullable: true),
                    Sex = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Tel = table.Column<string>(nullable: true),
                    Birthday = table.Column<DateTime>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    Updated = table.Column<DateTime>(nullable: false),
                    Version = table.Column<int>(nullable: false),
                    Popularity = table.Column<int>(nullable: false),
                    Rating = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserPersons", x => x.UserPersonId);
                });

            migrationBuilder.CreateTable(
                name: "UserPhotos",
                columns: table => new
                {
                    UserPhotoId = table.Column<string>(nullable: false),
                    UserPhotoFormat = table.Column<string>(nullable: true),
                    UserPhotoData = table.Column<string>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false),
                    Updated = table.Column<DateTime>(nullable: false),
                    Version = table.Column<int>(nullable: false),
                    Popularity = table.Column<int>(nullable: false),
                    Rating = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserPhotos", x => x.UserPhotoId);
                });

            migrationBuilder.CreateTable(
                name: "UserRoles",
                columns: table => new
                {
                    UserRoleId = table.Column<string>(nullable: false),
                    UserRoleName = table.Column<string>(nullable: true),
                    UserRoleDescription = table.Column<string>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false),
                    Updated = table.Column<DateTime>(nullable: false),
                    Version = table.Column<int>(nullable: false),
                    Popularity = table.Column<int>(nullable: false),
                    Rating = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserRoles", x => x.UserRoleId);
                });

            migrationBuilder.CreateTable(
                name: "UserSettings",
                columns: table => new
                {
                    UserSettingId = table.Column<string>(nullable: false),
                    HelpMode = table.Column<string>(nullable: false),
                    DarkColors = table.Column<string>(nullable: false),
                    MobileMode = table.Column<string>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    Updated = table.Column<DateTime>(nullable: false),
                    Version = table.Column<int>(nullable: false),
                    Popularity = table.Column<int>(nullable: false),
                    Rating = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserSettings", x => x.UserSettingId);
                });

            migrationBuilder.CreateTable(
                name: "UserTodos",
                columns: table => new
                {
                    UserTodoId = table.Column<string>(nullable: false),
                    DateBegin = table.Column<DateTime>(nullable: false),
                    DateEnd = table.Column<DateTime>(nullable: false),
                    TodoName = table.Column<string>(nullable: true),
                    TodoDescription = table.Column<string>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false),
                    Updated = table.Column<DateTime>(nullable: false),
                    Version = table.Column<int>(nullable: false),
                    Popularity = table.Column<int>(nullable: false),
                    Rating = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserTodos", x => x.UserTodoId);
                });

            migrationBuilder.CreateTable(
                name: "WebApps",
                columns: table => new
                {
                    WebAppId = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false),
                    Updated = table.Column<DateTime>(nullable: false),
                    Version = table.Column<int>(nullable: false),
                    Popularity = table.Column<int>(nullable: false),
                    Rating = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WebApps", x => x.WebAppId);
                });

            migrationBuilder.CreateTable(
                name: "PublicBlogs",
                columns: table => new
                {
                    PublicBlogId = table.Column<string>(nullable: false),
                    PublicAuthorId = table.Column<int>(nullable: false),
                    BlogUrl = table.Column<string>(nullable: true),
                    BlogRating = table.Column<int>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    Updated = table.Column<DateTime>(nullable: false),
                    Version = table.Column<int>(nullable: false),
                    Popularity = table.Column<int>(nullable: false),
                    Rating = table.Column<int>(nullable: false),
                    PPublicAuthorId = table.Column<int>(nullable: false),
                    PPublicAuthorPublicAuthorId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PublicBlogs", x => x.PublicBlogId);
                    table.ForeignKey(
                        name: "FK_PublicBlogs_PublicAuthors_PPublicAuthorPublicAuthorId",
                        column: x => x.PPublicAuthorPublicAuthorId,
                        principalTable: "PublicAuthors",
                        principalColumn: "PublicAuthorId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PublicJournals",
                columns: table => new
                {
                    PublicJournalId = table.Column<string>(nullable: false),
                    JournalLiteratureId = table.Column<int>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    Updated = table.Column<DateTime>(nullable: false),
                    Version = table.Column<int>(nullable: false),
                    Popularity = table.Column<int>(nullable: false),
                    Rating = table.Column<int>(nullable: false),
                    PPublicLiteratureId = table.Column<int>(nullable: false),
                    PPublicLiteraturePublicLiteratureId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PublicJournals", x => x.PublicJournalId);
                    table.ForeignKey(
                        name: "FK_PublicJournals_PublicLiteratures_PPublicLiteraturePublicLiteratureId",
                        column: x => x.PPublicLiteraturePublicLiteratureId,
                        principalTable: "PublicLiteratures",
                        principalColumn: "PublicLiteratureId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PublicCatalogs",
                columns: table => new
                {
                    PublicCatalogId = table.Column<string>(nullable: false),
                    CatalogServerId = table.Column<int>(nullable: false),
                    CatalogDatabaseId = table.Column<int>(nullable: false),
                    PublicLibId = table.Column<int>(nullable: false),
                    CatalogDatabase_PublicDatabaseId = table.Column<int>(nullable: false),
                    CatalogServer_PublicServerId = table.Column<int>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    Updated = table.Column<DateTime>(nullable: false),
                    Version = table.Column<int>(nullable: false),
                    Popularity = table.Column<int>(nullable: false),
                    Rating = table.Column<int>(nullable: false),
                    PPublicDatabaseId = table.Column<int>(nullable: false),
                    PPublicDatabasePublicDatabaseId = table.Column<string>(nullable: true),
                    PPublicLibId = table.Column<int>(nullable: false),
                    PPublicLibPublicLibId = table.Column<string>(nullable: true),
                    PPublicServerId = table.Column<int>(nullable: false),
                    PPublicServerPublicServerId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PublicCatalogs", x => x.PublicCatalogId);
                    table.ForeignKey(
                        name: "FK_PublicCatalogs_PublicDatabases_PPublicDatabasePublicDatabaseId",
                        column: x => x.PPublicDatabasePublicDatabaseId,
                        principalTable: "PublicDatabases",
                        principalColumn: "PublicDatabaseId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PublicCatalogs_PublicLibs_PPublicLibPublicLibId",
                        column: x => x.PPublicLibPublicLibId,
                        principalTable: "PublicLibs",
                        principalColumn: "PublicLibId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PublicCatalogs_PublicServers_PPublicServerPublicServerId",
                        column: x => x.PPublicServerPublicServerId,
                        principalTable: "PublicServers",
                        principalColumn: "PublicServerId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserAccounts",
                columns: table => new
                {
                    Username = table.Column<string>(nullable: false),
                    Password = table.Column<string>(nullable: true),
                    Hashcode = table.Column<string>(nullable: true),
                    LoginCount = table.Column<int>(nullable: false),
                    LastActive = table.Column<DateTime>(nullable: false),
                    UserPhotoId = table.Column<int>(nullable: false),
                    UserConfigId = table.Column<int>(nullable: false),
                    UserPersonId = table.Column<int>(nullable: false),
                    UserRoleId = table.Column<int>(nullable: false),
                    UserGroupId = table.Column<int>(nullable: false),
                    settings_UserSettingId = table.Column<int>(nullable: false),
                    InSystemNow = table.Column<string>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    Updated = table.Column<DateTime>(nullable: false),
                    Version = table.Column<int>(nullable: false),
                    Popularity = table.Column<int>(nullable: false),
                    Rating = table.Column<int>(nullable: false),
                    PUserGroupId = table.Column<int>(nullable: false),
                    PUserGroupUserGroupId = table.Column<string>(nullable: true),
                    PUserPersonId = table.Column<int>(nullable: false),
                    PUserPersonUserPersonId = table.Column<string>(nullable: true),
                    PUserPhotoId = table.Column<int>(nullable: false),
                    PUserPhotoUserPhotoId = table.Column<string>(nullable: true),
                    PUserRoleId = table.Column<int>(nullable: false),
                    PUserRoleUserRoleId = table.Column<string>(nullable: true),
                    PUserSettingId = table.Column<int>(nullable: false),
                    PUserSettingUserSettingId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserAccounts", x => x.Username);
                    table.ForeignKey(
                        name: "FK_UserAccounts_UserGroups_PUserGroupUserGroupId",
                        column: x => x.PUserGroupUserGroupId,
                        principalTable: "UserGroups",
                        principalColumn: "UserGroupId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserAccounts_UserPersons_PUserPersonUserPersonId",
                        column: x => x.PUserPersonUserPersonId,
                        principalTable: "UserPersons",
                        principalColumn: "UserPersonId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserAccounts_UserPhotos_PUserPhotoUserPhotoId",
                        column: x => x.PUserPhotoUserPhotoId,
                        principalTable: "UserPhotos",
                        principalColumn: "UserPhotoId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserAccounts_UserRoles_PUserRoleUserRoleId",
                        column: x => x.PUserRoleUserRoleId,
                        principalTable: "UserRoles",
                        principalColumn: "UserRoleId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserAccounts_UserSettings_PUserSettingUserSettingId",
                        column: x => x.PUserSettingUserSettingId,
                        principalTable: "UserSettings",
                        principalColumn: "UserSettingId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "WebPages",
                columns: table => new
                {
                    WebPageId = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Content = table.Column<string>(nullable: true),
                    AppID = table.Column<int>(nullable: false),
                    App_WebAppId = table.Column<int>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    Updated = table.Column<DateTime>(nullable: false),
                    Version = table.Column<int>(nullable: false),
                    Popularity = table.Column<int>(nullable: false),
                    Rating = table.Column<int>(nullable: false),
                    PWebAppId = table.Column<int>(nullable: false),
                    PWebAppWebAppId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WebPages", x => x.WebPageId);
                    table.ForeignKey(
                        name: "FK_WebPages_WebApps_PWebAppWebAppId",
                        column: x => x.PWebAppWebAppId,
                        principalTable: "WebApps",
                        principalColumn: "WebAppId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PublicPosts",
                columns: table => new
                {
                    PublicPostId = table.Column<string>(nullable: false),
                    PostTitle = table.Column<string>(nullable: true),
                    PostContent = table.Column<string>(nullable: true),
                    BlogId = table.Column<int>(nullable: false),
                    PublicBlog_PublicBlogId = table.Column<int>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    Updated = table.Column<DateTime>(nullable: false),
                    Version = table.Column<int>(nullable: false),
                    Popularity = table.Column<int>(nullable: false),
                    Rating = table.Column<int>(nullable: false),
                    PPublicBlogId = table.Column<int>(nullable: false),
                    PPublicBlogPublicBlogId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PublicPosts", x => x.PublicPostId);
                    table.ForeignKey(
                        name: "FK_PublicPosts_PublicBlogs_PPublicBlogPublicBlogId",
                        column: x => x.PPublicBlogPublicBlogId,
                        principalTable: "PublicBlogs",
                        principalColumn: "PublicBlogId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PublicReaders",
                columns: table => new
                {
                    PublicReaderId = table.Column<string>(nullable: false),
                    ReaderAccountId = table.Column<int>(nullable: false),
                    ReaderAccount_UserAccountId = table.Column<string>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false),
                    Updated = table.Column<DateTime>(nullable: false),
                    Version = table.Column<int>(nullable: false),
                    Popularity = table.Column<int>(nullable: false),
                    Rating = table.Column<int>(nullable: false),
                    PUserAccountId = table.Column<int>(nullable: false),
                    PUserAccountUsername = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PublicReaders", x => x.PublicReaderId);
                    table.ForeignKey(
                        name: "FK_PublicReaders_UserAccounts_PUserAccountUsername",
                        column: x => x.PUserAccountUsername,
                        principalTable: "UserAccounts",
                        principalColumn: "Username",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserMessages",
                columns: table => new
                {
                    UserMessageId = table.Column<string>(nullable: false),
                    Sender = table.Column<string>(nullable: true),
                    MessageSubject = table.Column<string>(nullable: true),
                    MessageText = table.Column<string>(nullable: true),
                    Readed = table.Column<string>(nullable: false),
                    UserAccountId = table.Column<string>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false),
                    Updated = table.Column<DateTime>(nullable: false),
                    Version = table.Column<int>(nullable: false),
                    Popularity = table.Column<int>(nullable: false),
                    Rating = table.Column<int>(nullable: false),
                    PUserAccountId = table.Column<int>(nullable: false),
                    PUserAccountUsername = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserMessages", x => x.UserMessageId);
                    table.ForeignKey(
                        name: "FK_UserMessages_UserAccounts_PUserAccountUsername",
                        column: x => x.PUserAccountUsername,
                        principalTable: "UserAccounts",
                        principalColumn: "Username",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PublicBlogs_PPublicAuthorPublicAuthorId",
                table: "PublicBlogs",
                column: "PPublicAuthorPublicAuthorId");

            migrationBuilder.CreateIndex(
                name: "IX_PublicCatalogs_PPublicDatabasePublicDatabaseId",
                table: "PublicCatalogs",
                column: "PPublicDatabasePublicDatabaseId");

            migrationBuilder.CreateIndex(
                name: "IX_PublicCatalogs_PPublicLibPublicLibId",
                table: "PublicCatalogs",
                column: "PPublicLibPublicLibId");

            migrationBuilder.CreateIndex(
                name: "IX_PublicCatalogs_PPublicServerPublicServerId",
                table: "PublicCatalogs",
                column: "PPublicServerPublicServerId");

            migrationBuilder.CreateIndex(
                name: "IX_PublicDatabases_PPublicDatabasePublicDatabaseId",
                table: "PublicDatabases",
                column: "PPublicDatabasePublicDatabaseId");

            migrationBuilder.CreateIndex(
                name: "IX_PublicJournals_PPublicLiteraturePublicLiteratureId",
                table: "PublicJournals",
                column: "PPublicLiteraturePublicLiteratureId");

            migrationBuilder.CreateIndex(
                name: "IX_PublicPosts_PPublicBlogPublicBlogId",
                table: "PublicPosts",
                column: "PPublicBlogPublicBlogId");

            migrationBuilder.CreateIndex(
                name: "IX_PublicReaders_PUserAccountUsername",
                table: "PublicReaders",
                column: "PUserAccountUsername");

            migrationBuilder.CreateIndex(
                name: "IX_UserAccounts_PUserGroupUserGroupId",
                table: "UserAccounts",
                column: "PUserGroupUserGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_UserAccounts_PUserPersonUserPersonId",
                table: "UserAccounts",
                column: "PUserPersonUserPersonId");

            migrationBuilder.CreateIndex(
                name: "IX_UserAccounts_PUserPhotoUserPhotoId",
                table: "UserAccounts",
                column: "PUserPhotoUserPhotoId");

            migrationBuilder.CreateIndex(
                name: "IX_UserAccounts_PUserRoleUserRoleId",
                table: "UserAccounts",
                column: "PUserRoleUserRoleId");

            migrationBuilder.CreateIndex(
                name: "IX_UserAccounts_PUserSettingUserSettingId",
                table: "UserAccounts",
                column: "PUserSettingUserSettingId");

            migrationBuilder.CreateIndex(
                name: "IX_UserMessages_PUserAccountUsername",
                table: "UserMessages",
                column: "PUserAccountUsername");

            migrationBuilder.CreateIndex(
                name: "IX_WebPages_PWebAppWebAppId",
                table: "WebPages",
                column: "PWebAppWebAppId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PublicCatalogs");

            migrationBuilder.DropTable(
                name: "PublicCustomers");

            migrationBuilder.DropTable(
                name: "PublicJournals");

            migrationBuilder.DropTable(
                name: "PublicPosts");

            migrationBuilder.DropTable(
                name: "PublicReaders");

            migrationBuilder.DropTable(
                name: "StatisticsCalendars");

            migrationBuilder.DropTable(
                name: "UserMessages");

            migrationBuilder.DropTable(
                name: "UserTodos");

            migrationBuilder.DropTable(
                name: "WebPages");

            migrationBuilder.DropTable(
                name: "PublicDatabases");

            migrationBuilder.DropTable(
                name: "PublicLibs");

            migrationBuilder.DropTable(
                name: "PublicServers");

            migrationBuilder.DropTable(
                name: "PublicLiteratures");

            migrationBuilder.DropTable(
                name: "PublicBlogs");

            migrationBuilder.DropTable(
                name: "UserAccounts");

            migrationBuilder.DropTable(
                name: "WebApps");

            migrationBuilder.DropTable(
                name: "PublicAuthors");

            migrationBuilder.DropTable(
                name: "UserGroups");

            migrationBuilder.DropTable(
                name: "UserPersons");

            migrationBuilder.DropTable(
                name: "UserPhotos");

            migrationBuilder.DropTable(
                name: "UserRoles");

            migrationBuilder.DropTable(
                name: "UserSettings");
        }
    }
}

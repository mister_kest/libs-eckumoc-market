﻿using SpbPublicLibsCore.Converter.Generators;
using SpbPublicLibsCore.Domain.Odbc.DataSource;
using SpbPublicLibsCore.Domain.Odbc.Metadata;
using SpbPublicLibsCore.Testing;
using System;

namespace SpbPublicLibsUnit.ConverterUnit
{
    public class TestEntityModelGenerator : TestingUnit
    {
        protected override void onTest()
        {
            MySqlOdbcDataSource dataSource = new MySqlOdbcDataSource();
            EntityModelGenerator generator = new EntityModelGenerator();
            foreach(var ptable in dataSource.GetDatabaseMetadata().Tables)
            {
                TableMetaData metadata = dataSource.GetDatabaseMetadata().Tables[ptable.Key];
                //Console.WriteLine(generator.CreateSharpModel(metadata));
                //Console.WriteLine(generator.CreateTypeScriptModel(metadata));
            }
            messages.Add("can generate entity model classes into typescript and csharp");
        }
    }
}
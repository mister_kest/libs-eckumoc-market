﻿using Microsoft.AspNetCore.Builder;
using ApplicationCore.CoreDomain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PublicLibsAspNet.CoreApp
{
    public static class CanActivateExtensions
    {

        public static IApplicationBuilder UseCanActivateComponent( this IApplicationBuilder app, Action<CanActivateOptions> configuration )
        {
            CanActivateOptions options = new CanActivateOptions();
            configuration(options);
            app.UseMiddleware<CanActivateComponent>(options);
            return app;
        }
    }
}

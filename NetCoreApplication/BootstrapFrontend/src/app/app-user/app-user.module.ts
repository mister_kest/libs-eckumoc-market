import { UserReaderModule } from './user-reader/user-reader.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserAdminModule } from './user-admin/user-admin.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    UserAdminModule,
    UserReaderModule
  ],
  exports:[
    UserAdminModule,
    UserReaderModule
  ]
})
export class AppUserModule {
  static routes = [
    {path: '',        redirectTo: 'reader', pathMatch: 'full'},
    {path: 'reader',  children: UserReaderModule.routes },
    {path: 'admin',   children: UserAdminModule.routes }
  ];
}

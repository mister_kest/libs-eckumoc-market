﻿using SpbPublicLibsCore.Testing;

namespace SpbPublicLibsUnit.DomainUnit
{
    public class TestAuthentication: TestingUnit
    {
        protected override void onTest()
        {
            canLogin();
            canValidate();            
            canLogout();
        }

        private void canLogout()
        {
            //
        }

        private void canLogin()
        {
            //
        }

        private void canValidate()
        {
            //
        }
    }
}

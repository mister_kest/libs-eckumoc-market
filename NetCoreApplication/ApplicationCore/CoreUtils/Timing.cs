﻿using System;

namespace ApplicationCore.Utils
{
    /// <summary>
    /// Реализует методы работы с системным временем
    /// </summary>
    public class Timing
    {

        /// <summary>
        /// Получение текущего времени в милисекундах
        /// </summary>
        /// <returns></returns>
        public static long GetTime()
        {
            TimeSpan uTimeSpan = (DateTime.Now - new DateTime(1970, 1, 1, 0, 0, 0));
            return (long)uTimeSpan.TotalMilliseconds;
        }



        /// <summary>
        /// Время начала сегодняшнего дня в миличекундах
        /// </summary>
        /// <returns></returns>
        public static long GetTodayBeginTime()
        {
            
            TimeSpan uTimeSpan = (DateTime.Today - new DateTime(1970, 1, 1, 0, 0, 0));
            return (long)uTimeSpan.TotalMilliseconds;
        }

        
    }
}

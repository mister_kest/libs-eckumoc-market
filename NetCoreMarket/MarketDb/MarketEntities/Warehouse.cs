﻿using System.Collections.Generic;

namespace MvcMarketPlace.Data.Entities
{
    public class Warehouse
    {
        public Warehouse()
        {
            ProductCountInfos = new List<ProductCountInfo>();
        }

        public int ID { get; set; }

        public virtual List<ProductCountInfo> ProductCountInfos { get; set; }
    }
}
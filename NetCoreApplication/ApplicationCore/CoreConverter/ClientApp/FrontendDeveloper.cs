﻿using eckumoc_netcore_codegen;

using ApplicationCore.Domain.Reflection;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace ApplicationCore.Converter.ClientApp
{
    public class FrontendDeveloper
    {


        public static void CreateDataLevelApp()
        {
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static Dictionary<string, object> GenerateHttpServicesForControllers(string outputDir)
        {
            Dictionary<string, object> map = new Dictionary<string, object>();
            HashSet<object> controllers = new HashSet<object>();
            foreach (Type type in AssemblyReader.GetControllers(Assembly.GetExecutingAssembly()))
            {
                List<object> actions = GetPublicMethods(type);
                string actionsString = toStrings(actions);
                controllers.Add(new
                {
                    name = type.Name,
                    type = "import { HttpClient } from '@angular/common/http';  " +
                        "class " + type.Name + "HttpService{ constructor( private http: HttpClient ){} \n\n" +
                        actionsString + "}"
                });
                System.IO.File.WriteAllText(
                    outputDir + type.Name.ToLower() + ".httpservice.ts",
                    "import { HttpClient } from '@angular/common/http'; \n" +
                    "import { Injectable } from '@angular/core';\n\n" +
                        "@Injectable({ providedIn: 'root' })" +
                        "export class " + type.Name + "HttpService{ \n\n constructor( private http: HttpClient ){} \n\n  " +
                        actionsString + "\n}");
            }
            map["controllers"] = controllers;
            return map;
        }


        private static string toStrings(List<object> actions)
        {
            string str = "";
            foreach (object action in actions)
            {
                str += action + ";\n  \n  ";
            }
            return str;
        }


        private static List<object> GetPublicMethods(Type type)
        {
            TypeScriptGenerator gen = new TypeScriptGenerator();
            List<object> actions = new List<object>();
            foreach (MethodInfo method in new ReflectionService().GetOwnPublicMethods(type))
            {
                string path = null;
                int lastIndex = (type.Name.IndexOf("Hub") != -1) ? type.Name.IndexOf("Hub") :
                                (type.Name.IndexOf("Controller") != -1) ? type.Name.IndexOf("Controller") : type.Name.Length;
                path = type.Name.Substring(0, lastIndex);
                Dictionary<string, object> parameters = new ReflectionService().GetMethodParameters(method);
                string methodDeclaration = gen.GetMethodDeclaration(method.Name, new ReflectionService().GetMethodParameters(method));

                Dictionary<string, string> pars = new Dictionary<string, string>();
                foreach (var p in new ReflectionService().GetMethodParameters(method))
                {
                    pars[p.Key] = p.Key;
                }
                actions.Add(methodDeclaration + "{ return this.http.get('" + path + "/" + method.Name + "',{params:" + gen.GetParameters(pars) + "});}");
                //       new
                //    {
                //name = method.Name,
                //returns= method.ReturnType.Name,
                //args = ReflectionService.GetMethodParameters(method),
                //func = method.Name+"("+ ReflectionService.GetMethodParametersString(method) + "){ return this.http.get('" + path + "/"+method.Name+"',{params:"+ ReflectionService.GetMethodParametersBlock(method) + "});}",
                //   });
            }
            return actions;
        }
    }
}

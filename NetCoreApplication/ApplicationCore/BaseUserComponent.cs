﻿using Microsoft.AspNetCore.Http;

using ApplicationCore.Messaging;

using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.Domain
{
    public class BaseUserComponent: BaseComponent
    {
        public BaseUserComponent(RequestDelegate next) : base(next)
        {
        }

        public override void OnMessage(RequestMessage request, ResponseMessage response)
        {
            throw new NotImplementedException();
        }

        protected override void OnException(Exception ex)
        {
            throw new NotImplementedException();
        }
    }
}

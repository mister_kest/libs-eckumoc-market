﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace MvcMarketPlace.Data.Entities
{
    public class SepateSubdivision
    {
        public int ID { get; set; }

        [DisplayName("Магазин")]
        public int MarketID { get; set; }

        [DisplayName("Склад")]
        public int WarehouseID { get; set; }
        public virtual Warehouse Warehouse { get; set; }
        public virtual Market Market { get; set; }
    }
}
